from django.views.decorators.csrf import csrf_exempt
from django.http.response import HttpResponse
from django.utils.decorators import method_decorator
from core.views.base_view import BaseView
from zabia.chatbot import ChatBot


class MessageView(BaseView):
    def get(self, request, *args, **kwargs):
        if request.GET['hub.verify_token'] == 'VERIFY_TOKEN':
            return HttpResponse(request.GET['hub.challenge'])
        else:
            return HttpResponse('Invalid verify token')

    @method_decorator(csrf_exempt)
    def post(self, request, *args, **kwargs):
        try:
            chatbot = ChatBot()
            message_replied = chatbot.handle_request(request)
        except Exception as exp:
            print(str(exp))
            pass
        return HttpResponse({'a': 1}, content_type='application/json')