import json
from django.http import HttpResponse
from django.views import View
from django.views.generic import ListView, DetailView
from zabia.models.zabia_user import ZUser


class ZabiaUserListView(ListView):
    queryset = ZUser.objects.all()
    template_name = "user_list.html"


class ZabiaUserDetailsView(DetailView):
    model = ZUser
    template_name = "user_details.html"


class ZabiaUserListAjaxView(View):
    def get(self, request, *args, **kwargs):
        user_objects = ZUser.objects.all()
        response = {
            "draw":0,
            "recordsTotal":user_objects.count(),
            "recordsFiltered":user_objects.count(),
            "data":[
              # [
              #    "2",
              #    "johnny.salgado.marquez@gmail.com",
              #    "Johnny Salgado",
              #    "Zabia",
              #    "",
              #    ""
              # ]
            ]
            }

        for user in user_objects:
            response["data"] += [[
                str(user.pk),
                user.get_email(),
                user.full_name,
                "FACEBOOK",
                "",
                ""
            ]]
        return HttpResponse(json.dumps(response), content_type="application/json")