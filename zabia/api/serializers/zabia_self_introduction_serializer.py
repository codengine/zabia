from rest_framework import serializers
from core.api.serializers.base_model_serializer import BaseModelSerializer
from zabia.models.zabia_conversation import ZabiaConversation


class ZabiaSelftIntroConversationSerializer(BaseModelSerializer):
    text = serializers.SerializerMethodField()
    has_placeholder = serializers.SerializerMethodField()

    def get_text(self, obj):
        text_instance = obj.pick_conversation_text()
        if text_instance:
            return text_instance.as_json()

    def get_has_placeholder(self, obj):
        text_instance = obj.pick_conversation_text()
        if text_instance:
            return text_instance.has_placeholder()
        return False

    class Meta:
        model = ZabiaConversation
        fields = ('id', 'text', 'has_placeholder', 'parent_id', 'tag', 'presentation', 'image', 'order',
                  'conversation_type', 'user_response_type', 'user_response_content_type', 'bot_asking_mode')