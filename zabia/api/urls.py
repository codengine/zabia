from django.conf.urls import url

from zabia.api.viewsets.update_conversation_level_api_view import UpdateConversationLevelAPIView
from zabia.api.viewsets.zabia_introduction_api_viewset import ZabiaIntroductionAPIView


urlpatterns = [
    url(r'^zabia-self-introduction/$', ZabiaIntroductionAPIView.as_view(), name='zabia_self_introduction'),
    url(r'^update-conversation-level/$', UpdateConversationLevelAPIView.as_view(), name='update_conversation_level'),
]
