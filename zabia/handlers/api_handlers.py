from zabia.handlers.http_request_handler import HttpRequestHandler


class APIHandler(object):
    @classmethod
    def call_tips_api(cls, tags=[], page=0, count=3):
        request_param = {
            "user": "",
            "tags": ",".join(tags),
            "pageNumber": str(page),
            "recordNumber": str(count)
        }
        print(request_param)
        url = "https://southtech.pe/zabiatest/service/gettip.php"
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        return response
        
    @classmethod
    def call_restaurant_api(cls, lat, lng, tags=[], page=0, count=3):
        request_param = {
            "user": "",
            "latitude": lat,
            "longitude": lng,
            "distance": 10,
            "menu": "",
            "favorite": "",
            "page": 0,
            "records": 3
        }
        url = "https://southtech.pe/zabiatest/service/getrestaurant.php"
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        return response

    @classmethod
    def call_recipes_api(cls, user_id="", page=0, count=3):
        url = "https://southtech.pe/zabiatest/service/getrecipe.php"
        request_param = {
            "user": user_id,
            "page": str(page),
            "records": str(count)
            }
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        # print(response)
        return response

    @classmethod
    def call_recipe_details_by_id(cls, recipe_id):
        url = "https://southtech.pe/zabiatest/service/getrecipebyid.php"
        request_param = {
            "user": "",
            "recipe": str(recipe_id)
            }
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        # print(response)
        return response

    @classmethod
    def trigger_send_mail(cls, mail_data):
        url = "https://southtech.pe/zabiatest/service/sendemail.php"
        request_param = mail_data
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        # print(response)
        return response

    @classmethod
    def get_all_questions(cls, user_id, question_type=""):
        url = "http://southtech.pe/zabiatest/service/getuserquestion.php"
        request_param = {
            "user" : user_id,
            "questionType" : question_type
            }
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        # print(response)
        return response

    @classmethod
    def create_zabia_user(cls, email, name, password, source):
        url = "https://southtech.pe/zabiatest/service/userregister.php"
        request_param = {
            "email": email,
            "name": name,
            "password": password,
            "source": source
            }
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        return status_code, response

    @classmethod
    def save_question_response(cls, user_id, question_id, question_code, answer_type, answer_id, user_answer):
        url = "https://southtech.pe/zabiatest/service/saveuseranswer.php"
        request_param = {
            "user": str(user_id),
            "questionCode": question_code,
            "questionID": question_id,
            "answerType": answer_type,
            "userAnswerID": answer_id,
            "userAnswer": user_answer
            }
        print(request_param)
        status_code, response = HttpRequestHandler.perform_post_request(url, request_param)
        print(status_code)
        return status_code, response
