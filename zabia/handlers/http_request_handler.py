import requests


class HttpRequestHandler(object):

    @classmethod
    def perform_post_request(cls, url, data):
        try:
            headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) '
                                     'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
            response = requests.post(url=url, json=data, headers=headers)
            return response.status_code, response.json()
        except Exception as exp:
            print(str(exp))
            pass

    @classmethod
    def perform_api_request(cls, url, **kwargs):
        try:
            headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) '
                                     'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
            response = requests.get(url=url, headers=headers).json()
            return response
        except Exception as exp:
            pass
