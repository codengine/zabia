# Generated by Django 2.0.6 on 2018-07-13 15:02

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('zabia', '0002_auto_20180713_1352'),
    ]

    operations = [
        migrations.AddField(
            model_name='zabiaconversation',
            name='payload',
            field=models.CharField(blank=True, max_length=500, null=True),
        ),
    ]
