# Generated by Django 2.0.6 on 2018-07-13 06:01

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='AppInfo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('app_id', models.CharField(max_length=500)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ConversationLevel',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('app_id', models.CharField(max_length=500)),
                ('app_user_id', models.CharField(max_length=500)),
                ('current_level', models.IntegerField(default=1)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='HealthProfile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('diet_type', models.CharField(blank=True, max_length=255, null=True)),
                ('kitchen_type', models.CharField(blank=True, max_length=255, null=True)),
                ('feeling', models.CharField(blank=True, max_length=255, null=True)),
                ('lifestyle', models.CharField(blank=True, max_length=255, null=True)),
                ('nutritional_drink', models.CharField(blank=True, max_length=255, null=True)),
                ('food_habit', models.CharField(blank=True, max_length=255, null=True)),
                ('supplement', models.CharField(blank=True, max_length=255, null=True)),
                ('mood', models.CharField(blank=True, max_length=255, null=True)),
                ('special_care', models.CharField(blank=True, max_length=255, null=True)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Location',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('name', models.CharField(max_length=500)),
                ('lat', models.CharField(blank=True, max_length=20, null=True)),
                ('long', models.CharField(blank=True, max_length=20, null=True)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Reminder',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('topic', models.CharField(blank=True, max_length=500, null=True)),
                ('message', models.TextField(blank=True, null=True)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Scheduler',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('app_id', models.CharField(max_length=500)),
                ('app_user_id', models.CharField(max_length=500)),
                ('function', models.CharField(max_length=100)),
                ('occurance', models.CharField(max_length=100)),
                ('schedule_time', models.DateTimeField(blank=True, null=True)),
                ('topic', models.CharField(max_length=255)),
                ('context', models.CharField(max_length=255)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('reminder_info', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='zabia.Reminder')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='Tracker',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('topic', models.CharField(max_length=255)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='TrackerResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('app_id', models.CharField(max_length=500)),
                ('app_user_id', models.CharField(max_length=500)),
                ('user_response', models.CharField(max_length=255)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('tracker', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='zabia.Tracker')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ZabiaConversation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('content_source', models.CharField(default='FROM_CONV_TEXT', max_length=100)),
                ('tag', models.CharField(max_length=255)),
                ('topic', models.CharField(blank=True, max_length=500, null=True)),
                ('context', models.CharField(blank=True, max_length=255, null=True)),
                ('presentation', models.CharField(max_length=50)),
                ('image', models.TextField(blank=True, null=True)),
                ('image_type', models.CharField(blank=True, max_length=20, null=True)),
                ('order', models.IntegerField(default=0)),
                ('conversation_type', models.CharField(max_length=20)),
                ('user_response_type', models.CharField(max_length=20, null=True)),
                ('user_response_content_type', models.CharField(max_length=20, null=True)),
                ('bot_asking_mode', models.CharField(default='SAY', max_length=20)),
                ('post_action', models.CharField(blank=True, max_length=255, null=True)),
                ('post_action_intent', models.CharField(blank=True, max_length=100, null=True)),
                ('save_param', models.TextField(blank=True, null=True)),
                ('send_mail_params', models.TextField(blank=True, null=True)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('parent', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='zabia.ZabiaConversation')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ZabiaConversationText',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('text', models.TextField()),
                ('text_en', models.TextField(default='')),
                ('description', models.TextField(default='')),
                ('description_en', models.TextField(default='')),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ZUser',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('app_id', models.CharField(max_length=500)),
                ('app_user_id', models.CharField(max_length=500)),
                ('image', models.CharField(blank=True, max_length=500, null=True)),
                ('thumbnail', models.CharField(blank=True, max_length=500, null=True)),
                ('first_name', models.CharField(blank=True, max_length=255, null=True)),
                ('last_name', models.CharField(blank=True, max_length=255, null=True)),
                ('nick_name', models.CharField(blank=True, max_length=255, null=True)),
                ('full_name', models.CharField(blank=True, max_length=255, null=True)),
                ('age', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('gender', models.CharField(blank=True, max_length=20, null=True)),
                ('height', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('weight', models.DecimalField(blank=True, decimal_places=2, max_digits=20, null=True)),
                ('terms_accepted', models.BooleanField(default=False)),
                ('email_verified', models.BooleanField(default=False)),
                ('email_verification_code', models.CharField(blank=True, max_length=500, null=True)),
                ('email_verification_code_expiry', models.DateTimeField(blank=True, null=True)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('health_profile', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='zabia.HealthProfile')),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('location', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='zabia.Location')),
                ('user', models.OneToOneField(null=True, on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ZUserInputAwait',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('app_id', models.CharField(max_length=500)),
                ('app_user_id', models.CharField(max_length=500)),
                ('q_postback', models.CharField(max_length=255)),
                ('waiting_for_input', models.BooleanField(default=False)),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.CreateModel(
            name='ZUserResponse',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('date_created', models.DateTimeField()),
                ('last_updated', models.DateTimeField()),
                ('is_active', models.BooleanField(default=False)),
                ('is_deleted', models.BooleanField(default=False)),
                ('response_type', models.CharField(max_length=20)),
                ('response_text', models.TextField(default='')),
                ('conversation', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='zabia.ZabiaConversation')),
                ('created_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('last_updated_by', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='+', to=settings.AUTH_USER_MODEL)),
                ('zuser', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='zabia.ZUser')),
            ],
            options={
                'abstract': False,
            },
        ),
        migrations.AddField(
            model_name='zabiaconversation',
            name='texts',
            field=models.ManyToManyField(to='zabia.ZabiaConversationText'),
        ),
        migrations.AddField(
            model_name='scheduler',
            name='tracker_info',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='zabia.Tracker'),
        ),
        migrations.AddField(
            model_name='scheduler',
            name='tracker_question',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='zabia.ZabiaConversation'),
        ),
    ]
