from enum import Enum


class HPDietType(Enum):
    PALEO = "paleo"
    VEGAN = "vegan"
    VEGETARIAN = "vegetarian"
    PESCETARIAN = "pescetarian"
    LOWSODIO = "lowsodio"
    NONE = "none"


class HPKitchenType(Enum):
    ITALIAN = "Italian"
    PERUVIAN = "Peruvian"
    CHINESE = "Chinese"
    FUSION = "Fusion"
    FRENCH = "French"
    OTHER = "other"


class HPFeelingType(Enum):
    HAPPY = "Happy"
    SAD = "sad"
    NORMAL = "normal"
    DEPRESSED = "depressed"


class HPLifeStyle(Enum):
    SMOKES = "smokes"
    ALCOHOLIC = "alcoholic"


class HPNutritionalDrinks(Enum):
    SODAS = "sodas"
    CAFFEINE = "caffeine"
    ENERGIZERS = "energizers"


class HPFoodHabit(Enum):
    FISH = "fish"
    CHICKEN = "chicken"
    PIG = "pig"


class QuestionType(Enum):
    PERSONAL_INFO = "Datos personales"
    WOMENS_HEALTH = "Salud femenina"
    LIFESTYLE = "Estilo de vida"
    NUTRITION = "Nutrición"
    MOOD = "Estado de ánimo"
    DIET = "Dieta"









