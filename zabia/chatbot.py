from __future__ import print_function

from datetime import timedelta
import jwt
import smooch
from config.smooch import smooch_config
from engine.clock.Clock import Clock
from zabia.chatflow.mixins.basic_info_mixin import BasicInfoMixin
from zabia.chatflow.mixins.first_hello_message_mixin import FirstHelloMessageMixin
from zabia.chatflow.mixins.main_menu_mixin import ZabiaMainMenuMixin
from zabia.chatflow.mixins.new_user_handler_mixin import NewUserHandlerMixin
from zabia.chatflow.mixins.post_action_handler_mixin import PostActionHandlerMixin
from zabia.chatflow.mixins.postback_handler_mixin import PostbackHandlerMixin
from zabia.chatflow.mixins.say_greeting_mixin import SayGreetingMixin
from zabia.chatflow.mixins.smooch_no_payload_handler_mixin import SmoochNoPayloadHandlerMixin
from zabia.chatflow.mixins.smooch_payload_handler import SmoochPayloadHandler
from zabia.chatflow.mixins.terms_not_accepted_mixin import TermsNotAcceptedMixin
from zabia.chatflow.mixins.typing_activity_mixin import TypingActivityMixin
from zabia.chatflow.mixins.zabia_self_introduction_mixin import ZabiaSelfIntroductionMixin
from zabia.models.app_info import AppInfo
from zabia.models.zabia_user import ZUser
from zabia.utils.smooch_parser import SmoochParser


class ChatBot(TermsNotAcceptedMixin, FirstHelloMessageMixin, PostActionHandlerMixin, PostbackHandlerMixin, SmoochPayloadHandler,
              SmoochNoPayloadHandlerMixin, ZabiaMainMenuMixin, NewUserHandlerMixin, ZabiaSelfIntroductionMixin,
              BasicInfoMixin, TypingActivityMixin, SayGreetingMixin):

    def obtain_jwt_token(self):
        token_bytes = jwt.encode({'scope': 'app'}, smooch_config['SECRET'], algorithm='HS256', headers={'kid': smooch_config['KEY_ID']})
        token = token_bytes.decode('utf-8')
        return token

    def __init__(self):
        self.auth_token = self.obtain_jwt_token()
        smooch.configuration.api_key['Authorization'] = self.auth_token
        smooch.configuration.api_key_prefix['Authorization'] = 'Bearer'
        self.api_instance = smooch.ConversationApi()
        self.menu_api_instance = smooch.MenuApi()

    def get_api_instance(self):
        return self.api_instance

    def send_message(self, app_id, app_user_id, message_post_body):
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post_body)
        return api_response

    def send_main_menu(self, app_id, app_user_id, json_body):
        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
        
        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
        if not name:
            name = json_body['appUser']['givenName']

        options = ['Hello', 'Hi', 'Hey']

        from random import randint

        option = options[randint(0, len(options) - 1)]

        message_text = '%s! How can I help you please?' % option
        message_text += 'Below are your options. Select one of them or just ask me something'
        message_post_body = smooch.MessagePost(
            'appMaker', 'text', text=message_text)
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post_body)

        items = []
        # One message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='FORMA_ACTIVARSE')
        actions += [action]

        item = self.build_message_item(title='Formas de activarse',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/activarse.png',
                                       media_type='image/png', actions=actions)
        items += [item]

        # Next message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='YOGA')
        actions += [action]

        item = self.build_message_item(title='Yoga',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/contigo_mismo.jpeg',
                                       media_type='image/jpeg', actions=actions)
        items += [item]

        # Next message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='MEDITACION')
        actions += [action]

        item = self.build_message_item(title='Meditaci�n',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/meditacion1.jpeg',
                                       media_type='image/jpeg', actions=actions)
        items += [item]

        # Next message item
        actions = []
        action = self.build_action(text='Seleccionar', type='postback', payload='TIP_ACTIVARSE')
        actions += [action]

        item = self.build_message_item(title='Tips',
                                       media_url='https://www.southtech.pe/zabiatest/imagen/icono/tip_consejo.jpeg',
                                       media_type='image/jpeg', actions=actions)
        items += [item]

        message_post = smooch.MessagePost(role='appMaker', type='carousel',
                                          items=items)
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post)
        
        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        
        return api_response
        
    def greeting_message(self, offset):
        utc_now = Clock.timezone_utc_now()
        offset_int = int(offset)
        local_dt = utc_now + timedelta(hours=offset_int)
        hour = local_dt.hour
        if hour < 12:
            greeting = "Good morning"
        elif 12 <= hour < 18:
            greeting = "Good afternoon"
        else:
            greeting = "Good night"
        return greeting

    def handle_request(self, request):
        smooch_parser = SmoochParser(request)
        json_body = smooch_parser.get_json_body()
        app_id = smooch_parser.get_app_id()
        app_user_id = smooch_parser.get_app_user_id()

        AppInfo.update_app_info(app_id=app_id)

        # print(json_body)

        # from zabia.handlers.menu_handler import MenuHandler
        #
        # MenuHandler().add_persistent_menu()

        return self.handle_message(app_id, app_user_id, json_body)

    def handle_message(self, app_id, app_user_id, json_body):

        if json_body.get('trigger') == 'message:appUser':

            smooch_payload = json_body['messages'][0].get('payload')

            smooch_payload = smooch_payload if smooch_payload else ''

            # print(json_body)
            # print("Payload: " + smooch_payload)

            if not smooch_payload:
                # self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                # return
                self.handle_no_payload(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
            else:
                self.handle_smooch_payload(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

        elif json_body.get('trigger') == 'postback':
            # print(json_body)
            # print("Postback: " + json_body.get('trigger'))
            self.handle_postback(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

