from django.db import models
from core.models.base_entity import BaseEntity
from zabia.constants.enums import SchedulerFunction


class TrackerChart(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    function = models.CharField(max_length=255,
                                default=SchedulerFunction.TRACKER.value)  # SchedulerFunction.TRACKER.value
    chart_url = models.TextField()
    response_time = models.DateTimeField()

    @classmethod
    def exists(cls, app_id, app_user_id, response_time, function=SchedulerFunction.TRACKER.value):
        return cls.objects.filter(app_id=app_id,
                                  app_user_id=app_user_id,
                                  function=function,
                                  response_time=response_time).exists()

    @classmethod
    def add_tracker_chart(cls, app_id, app_user_id, chart_url, response_time, function=SchedulerFunction.TRACKER.value):
        if not cls.exists(app_id, app_user_id, response_time, function):
            instance = cls(app_id=app_id,
                           app_user_id=app_user_id,
                           function=function,
                           chart_url=chart_url,
                           response_time=response_time)
            instance.save()