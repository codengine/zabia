from django.db import models
from core.models.base_entity import BaseEntity


class HealthProfile(BaseEntity):
    diet_type = models.CharField(max_length=255, null=True, blank=True) # HPDietType.paleo.value
    kitchen_type = models.CharField(max_length=255, null=True, blank=True)
    feeling = models.CharField(max_length=255, null=True, blank=True)
    lifestyle = models.CharField(max_length=255, null=True, blank=True)
    nutritional_drink = models.CharField(max_length=255, null=True, blank=True)
    food_habit = models.CharField(max_length=255, null=True, blank=True)
    supplement = models.CharField(max_length=255, null=True, blank=True)
    mood = models.CharField(max_length=255, null=True, blank=True)
    special_care = models.CharField(max_length=255, null=True, blank=True)
