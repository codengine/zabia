from django.db import models
from core.models.base_entity import BaseEntity


class ZUserInputAwait(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    q_postback = models.CharField(max_length=255)
    waiting_for_input = models.BooleanField(default=False)

    @classmethod
    def get_current_waiting_postback(cls, app_id, app_user_id):
        z_user_await_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_user_await_objects.exists():
            z_user_await_object = z_user_await_objects.first()
            return z_user_await_object.q_postback
        return None

    @classmethod
    def check_if_waiting(cls, app_id, app_user_id):
        z_user_await_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_user_await_objects.exists():
            z_user_await_object = z_user_await_objects.first()
            return z_user_await_object.waiting_for_input
        return False

    @classmethod
    def disable_input_wait(cls, app_id, app_user_id):
        z_user_await_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_user_await_objects.exists():
            z_user_await_object = z_user_await_objects.first()
            z_user_await_object.waiting_for_input = False
            z_user_await_object.save()

    @classmethod
    def wait_for_input(cls, app_id, app_user_id, q_postback):
        z_user_await_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_user_await_objects.exists():
            z_user_await_object = z_user_await_objects.first()
            z_user_await_object.q_postback = q_postback
            z_user_await_object.waiting_for_input = True
            z_user_await_object.save()
        else:
            z_user_await_object = cls(app_id=app_id, app_user_id=app_user_id)
            z_user_await_object.q_postback = q_postback
            z_user_await_object.waiting_for_input = True
            z_user_await_object.save()

