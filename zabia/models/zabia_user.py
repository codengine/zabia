from decimal import Decimal
from django.db import models
from django.contrib.auth.models import User
from django.db.models import Q
from core.models.base_entity import BaseEntity
from engine.clock.Clock import Clock
from zabia.constants.enums import ConversationTag
from zabia.handlers.api_handlers import APIHandler
from zabia.models.health_profile import HealthProfile
from zabia.models.location import Location


class ZUser(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    user = models.OneToOneField(User, null=True, on_delete=models.CASCADE)
    zabia_user_id = models.CharField(max_length=255, null=True, blank=True)
    image = models.CharField(max_length=500, null=True, blank=True)
    thumbnail = models.CharField(max_length=500, null=True, blank=True)
    first_name = models.CharField(max_length=255, null=True, blank=True)
    last_name = models.CharField(max_length=255, null=True, blank=True)
    nick_name = models.CharField(max_length=255, null=True, blank=True)
    full_name = models.CharField(max_length=255, null=True, blank=True)
    age = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    gender = models.CharField(max_length=20, null=True, blank=True)
    height = models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    weight= models.DecimalField(decimal_places=2, max_digits=20, null=True, blank=True)
    location = models.ForeignKey(Location, null=True, blank=True, on_delete=models.CASCADE)
    location_confirmed = models.BooleanField(default=False)
    timezone = models.CharField(max_length=255, null=True, blank=True)
    terms_accepted = models.BooleanField(default=False)
    email_verified = models.BooleanField(default=False)
    email_verification_code = models.CharField(max_length=500, null=True, blank=True)
    email_verification_code_expiry = models.DateTimeField(null=True, blank=True)
    health_profile = models.ForeignKey(HealthProfile, null=True, blank=True, on_delete=models.CASCADE)

    @classmethod
    def get_zabia_user_id(cls, app_id, app_user_id):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            return z_user.zabia_user_id

    @property
    def timezone_offset(self):
        offset = Clock.timezone_to_offset(self.timezone)
        return offset

    def get_email(self):
        if self.user:
            return self.user.email
        return ""

    def render_age(self):
        if self.age:
            return str(int(self.age))
        return "-"

    def render_gender(self):
        if self.gender:
            return self.gender
        return "-"

    def render_height(self):
        if self.height:
            return str(self.height)
        return "-"

    def render_weight(self):
        if self.weight:
            return str(self.weight)
        return "-"

    @classmethod
    def check_if_location_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) &
                                     (Q(location__isnull=True) | Q(timezone__isnull=True) | Q(location_confirmed=False)))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_name_missing(cls,app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(full_name__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_email_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(zabia_user_id__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_age_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(age__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_gender_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(gender__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_image_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(image__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_weight_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(weight__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_height_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) & Q(height__isnull=True))
        if z_users.exists():
            return True
        return False

    @classmethod
    def check_if_basic_info_missing(cls, app_id, app_user_id):
        if not cls.objects.filter(app_id=app_id, app_user_id=app_user_id).exists():
            return True
        z_users = cls.objects.filter(Q(app_id=app_id) & Q(app_user_id=app_user_id) &
                                     (Q(full_name__isnull=True) | Q(age__isnull=True) |
                                     Q(zabia_user_id__isnull=True) |
                                      # Q(image__isnull=True) |
                                     Q(location_confirmed=False) |
                                     Q(weight__isnull=True) | Q(height__isnull=True) |
                                      Q(gender__isnull=True)))
        if z_users.exists():
            return True
        return False

    @classmethod
    def add_new_user(cls, app_id, app_user_id):
        z_user = cls()
        z_user.app_id = app_id
        z_user.app_user_id = app_user_id
        z_user.save()
        return z_user

    @classmethod
    def update_terms_agreed(cls, app_id, app_user_id, terms_accepted):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.terms_accepted = terms_accepted
            z_user.save()
            return z_user
        return None

    @classmethod
    def update_name(cls, app_id, app_user_id, name):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.full_name = name
            z_user.save()
            return True
        return False

    @classmethod
    def update_gender(cls, app_id, app_user_id, gender):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.gender = gender
            z_user.save()
            return True
        return False

    @classmethod
    def update_age(cls, app_id, app_user_id, age):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.age = Decimal(age)
            z_user.save()
            return True
        return False

    @classmethod
    def update_weight(cls, app_id, app_user_id, weight):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.weight = Decimal(weight)
            z_user.save()
            return True
        return False

    @classmethod
    def update_height(cls, app_id, app_user_id, height):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.height = Decimal(height)
            z_user.save()
            return True
        return False

    @classmethod
    def get_name(cls, app_id, app_user_id):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            return z_user.full_name
        return None

    @classmethod
    def get_gender(cls, app_id, app_user_id):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            return z_user.gender
        return None

    @classmethod
    def update_email_v1(cls, app_id, app_user_id, email):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            try:
                z_user = z_users.first()
                if not z_user.user:
                    if User.objects.filter(email=email).exists():
                        return """It seems that the email was being used by someone else.
                        Please enter a different email address"""
                    user_instance = User.objects.create_user(email, email, '1234')
                    z_user.user_id = user_instance.pk
                    z_user.save()
                else:
                    user_instance = z_user.user
                    user_instance.email = email
                    user_instance.save()
                    z_user.save()
                return True
            except Exception as exp:
                print(str(exp))
                return """Something wrong happened with your request. Please try using a different email address. """
        return False

    @classmethod
    def create_zabia_user(cls, app_id, app_user_id, email):
        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
        status_code, response = APIHandler.create_zabia_user(email=email,
                                                             name=name,
                                                             password='',
                                                             source='fbk')
        if status_code == 200:
            if response['data']:
                user_id = response['data'][0]['code']
                z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
                if z_users.exists():
                    z_user = z_users.first()
                    z_user.zabia_user_id = user_id
                    z_user.save()
            return True
        if status_code not in [200, 201]:
            return """It seems that the email was being used by someone else. Please enter a different email address"""
        else:
            return "Something wrong happened with your request. Please try using a different email address."

    @classmethod
    def update_image_url(cls, app_id, app_user_id, image_url):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.image = image_url
            z_user.save()
            return True
        return False

    @classmethod
    def get_user_lat_lng(cls, app_id, app_user_id):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            if z_user.location:
                return z_user.location.lat, z_user.location.long
        return None, None

    @classmethod
    def get_location_info(cls, app_id, app_user_id):
        location_info = ""
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            if z_user.location:
                location_info += "Address: %s\n" % z_user.location.name
            if z_user.timezone:
                location_info += "Timezone: %s\n" % z_user.timezone
        return location_info

    @classmethod
    def update_location(cls, app_id, app_user_id, name, lat, long, timezone):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            location = Location()
            location.name = name
            location.lat = lat
            location.long = long
            location.save()

            z_user.location_id = location.pk
            z_user.timezone = timezone
            z_user.save()
            return True
        return False

    @classmethod
    def save_location_confirmation(cls, app_id, app_user_id, location_confirmed):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            z_user.location_confirmed = location_confirmed
            z_user.save()

    @classmethod
    def is_location_confirmed(cls, app_id, app_user_id):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            return z_user.location_confirmed
        return False

    @classmethod
    def update_field(cls, app_id, app_user_id, field_name, field_value):
        z_users = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            if field_name == ConversationTag.EMAIL.value:
                return cls.create_zabia_user(app_id=app_id, app_user_id=app_user_id, email=field_value)
            elif field_name == ConversationTag.NAME.value:
                return cls.update_name(app_id=app_id, app_user_id=app_user_id, name=field_value)
            elif field_name == ConversationTag.GENDER.value:
                return cls.update_gender(app_id=app_id, app_user_id=app_user_id, gender=field_value)
            elif field_name == ConversationTag.AGE.value:
                return cls.update_age(app_id=app_id, app_user_id=app_user_id, age=field_value)
            elif field_name == ConversationTag.WEIGHT.value:
                return cls.update_weight(app_id=app_id, app_user_id=app_user_id, weight=field_value)
            elif field_name == ConversationTag.HEIGHT.value:
                return cls.update_height(app_id=app_id, app_user_id=app_user_id, height=field_value)
