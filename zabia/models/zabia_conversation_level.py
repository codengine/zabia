from django.db import models
from core.models.base_entity import BaseEntity
from zabia.constants.enums import ConversationLevelEnum


class ConversationLevel(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    current_level = models.IntegerField(default=ConversationLevelEnum.PROFILE_CREATED_BUT_NOT_AGREED_TERMS_AND_CONDITIONS.value)  # ConversationLevelEnum

