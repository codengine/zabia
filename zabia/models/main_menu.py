from django.db import models
from core.models.base_entity import BaseEntity


class SubMenu(BaseEntity):
    title = models.CharField(max_length=500)
    title_en = models.CharField(max_length=500, null=True, blank=True)
    tag = models.CharField(max_length=100)
    icon = models.ImageField(max_length=500, upload_to='mainmenu/', null=True)
    
    @classmethod
    def add_submenu(cls, title, tag, title_en=None, icon=None):
        instances = cls.objects.filter(title=title, tag=tag)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()
            
        instance.title = title
        if title_en:
            instance.title_en = title_en
        instance.tag = tag
        if icon:
            instance.icon = icon
        instance.save()        


class MainMenu(BaseEntity):
    title = models.CharField(max_length=500)
    title_en = models.CharField(max_length=500, null=True, blank=True)
    description = models.TextField()
    description_en = models.TextField(null=True, blank=True)
    tag = models.CharField(max_length=100) # MainMenu.EAT_HEALTH.value
    image = models.ImageField(max_length=500, upload_to='mainmenu/', null=True)
    order = models.IntegerField(default=0)
    childs = models.ManyToManyField(SubMenu)
    
    
    @classmethod
    def add_main_menu(cls, title, description, tag, image, title_en=None, description_en=None, order=0, childs=[]):
        instances = cls.objects.filter(title=title, tag=tag)
        if instances.exists():
            instance = instances.first()
        else:
            instance = cls()
        
        instance.title = title
        if title_en:
            instance.title_en = title_en
        instance.description = description
        if description_en:
            instance.description_en = description_en
        instance.tag = tag
        instance.image = image
        if order:
            instance.order = order
        instance.save()
        
        if childs:
            instance.childs.add(*childs)
        
    

