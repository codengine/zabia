from datetime import datetime, timedelta
from django.db import models
from celery.task.control import revoke
from core.models.base_entity import BaseEntity
from engine.clock.Clock import Clock
from zabia.constants.enums import SchedulerFunction, ConversationType, ConversationTag
from zabia.models.reminder import Reminder
from zabia.models.tracker import Tracker
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser


class TrackerProcessedResponse(BaseEntity):
    hash_value = models.CharField(max_length=2000)


class Scheduler(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    function = models.CharField(max_length=100) # e.g Tracker, Reminder
    occurance = models.CharField(max_length=100, null=True, blank=True) # ONCE_A_DAY, ONCE_A_WEEK, EVERY_OTHER_DAYS
    occurance_weekday = models.CharField(max_length=100, null=True, blank=True) # SATURDAY, SUNDAY, MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY
    schedule_until = models.DateTimeField(null=True, blank=True)
    schedule_time = models.TimeField(null=True, blank=True)
    topic = models.CharField(max_length=255) # e.g Mood, Tips
    context = models.CharField(max_length=255) # e.g Feel Good
    tracker_info = models.ForeignKey(Tracker, null=True, blank=True, on_delete=models.CASCADE)
    tracker_question = models.ForeignKey(ZabiaConversation, null=True, blank=True, on_delete=models.CASCADE)
    reminder_info = models.ForeignKey(Reminder, null=True, blank=True, on_delete=models.CASCADE)
    partner_email = models.CharField(max_length=255, null=True, blank=True)
    skip_partner_email = models.BooleanField(default=False)
    enabled = models.BooleanField(default=False)
    task_id = models.CharField(max_length=1000, null=True, blank=True)
    
    @classmethod
    def next_reschedule_time(cls, app_id, app_user_id, context, topic, function=SchedulerFunction.TRACKER.value):
    
        WEEK_DAYS = {
            "MONDAY": 0,
            "TUESDAY": 1,
            "WEDNESDAY": 2,
            "THURSDAY": 3,
            "FRIDAY": 4,
            "SATURDAY": 5,
            "SUNDAY": 6
        }

        zuser_object = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id).first()
        if zuser_object:
            user_timezone_offset = zuser_object.timezone_offset
        else:
            user_timezone_offset = None
    
        scheduler_objects = cls.objects.filter(app_id=app_id,
                                               app_user_id=app_user_id,
                                               function=function,
                                               context=context,
                                               topic=topic)
        if scheduler_objects.exists():
            scheduler_object = scheduler_objects.first()
            now_utc_time = Clock.timezone_utc_now()
            today_weekday = now_utc_time.weekday()
            next_reschedule_time = None
            # Check for occurance
            if scheduler_object.schedule_until and now_utc_time <= scheduler_object.schedule_until:
                if scheduler_object.occurance == "ONCE_A_DAY":
                    next_datetime = now_utc_time + timedelta(days=1)
                    next_reschedule_time = datetime.combine(next_datetime.date(), scheduler_object.schedule_time)
                    # return next_reschedule_time
                elif scheduler_object.occurance == "ONCE_A_WEEK":
                    occurance_weekday = scheduler_object.occurance_weekday
                    occurance_week_day_n = WEEK_DAYS.get(occurance_weekday)
                    if occurance_week_day_n - today_weekday < 0:
                        days_to_add = occurance_week_day_n - today_weekday + 7
                    else:
                        days_to_add = occurance_week_day_n - today_weekday
                    next_datetime = now_utc_time + timedelta(days=days_to_add)
                    next_reschedule_time = datetime.combine(next_datetime.date(), scheduler_object.schedule_time)
                    # return next_reschedule_time
                elif scheduler_object.occurance == "EVERY_OTHER_DAY":
                    occurance_weekday = scheduler_object.occurance_weekday
                    occurance_week_day_n = WEEK_DAYS.get(occurance_weekday)
                    next_datetime = now_utc_time + timedelta(days=1)
                    next_datetime_weekday = next_datetime.weekday()
                    if next_datetime_weekday == occurance_week_day_n:
                        next_datetime = now_utc_time + timedelta(days=2)
                    next_reschedule_time = datetime.combine(next_datetime.date(), scheduler_object.schedule_time)
                    # return next_reschedule_time
            if next_reschedule_time and user_timezone_offset:
                pass
            # dt = datetime.now().date()
            # return datetime(month=dt.month, day=dt.day, year=dt.year, hour=1, minute=40, second=0)
            return next_reschedule_time

    @classmethod
    def get_next_schedule_for_daily_subscription(cls):
        now_utc_time = Clock.timezone_utc_now()
        return now_utc_time + timedelta(days=1)

    @property
    def schedule_occurance_verbal(self):
        if self.occurance == "ONCE_A_DAY":
            return "Once a day"
        elif self.occurance == "ONCE_A_WEEK":
            return "Once in a week"
        elif self.occurance == "EVERY_OTHER_DAY":
            return "Every other day"

    @classmethod
    def get_tracker_summary(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            summary = ""
            summary += "Schedule Time: " + tracker_object.schedule_time.strftime("%H:%M") + "\n"
            summary += "Schedule Occurance: " + tracker_object.schedule_occurance_verbal + "\n"
            summary += "Schedule Till: " + tracker_object.schedule_until.strftime("%d/%m/%Y") + "\n"
            return summary
        return ""

    @classmethod
    def save_tracker_partner_email_skip(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            tracker_object.skip_partner_email = True
            tracker_object.save()

    @classmethod
    def enable_tracker(cls, app_id, app_user_id, context, topic, task_id=None):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            tracker_object.enabled = True
            if task_id:
                tracker_object.task_id = task_id
            tracker_object.save()

    @classmethod
    def get_task_id(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            return tracker_object.task_id

    @classmethod
    def cancel_celery_task(cls, app_id, app_user_id, context, topic):
        try:
            task_id = cls.get_task_id(app_id=app_id,
                                      app_user_id=app_user_id,
                                      context=context,
                                      topic=topic)
            revoke(task_id, terminate=True)
        except Exception as exp:
            print("Error on cancelling celery task")

    @classmethod
    def delete_tracker(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        tracker_objects.delete()

    @classmethod
    def save_tracker(cls, app_id, app_user_id, context, topic, field_name, field_value):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
        else:
            tracker_object = cls(app_id=app_id,
                                 app_user_id=app_user_id,
                                 function=SchedulerFunction.TRACKER.value,
                                 context=context,
                                 topic=topic)
        if field_name == ConversationTag.SCHEDULER_TIME.value:
            tracker_object.schedule_time = field_value
        elif field_name == ConversationTag.SCHEDULER_OCCURANCE_DAY.value:
            tracker_object.occurance_weekday = field_value
        elif field_name == ConversationTag.SCHEDULER_OCCURANCE.value:
            tracker_object.occurance = field_value
        elif field_name == ConversationTag.SCHEDULER_UNTIL.value:
            if field_value == "1_WEEK":
                now_utc_dt = Clock.timezone_utc_now()
                schedule_until_dt = Clock.add_days(now_utc_dt, 7)
                tracker_object.schedule_until = schedule_until_dt
            elif field_value == "1_MONTH":
                now_utc_dt = Clock.timezone_utc_now()
                schedule_until_dt = Clock.add_days(now_utc_dt, 30)
                tracker_object.schedule_until = schedule_until_dt
            elif field_value == "3_MONTH":
                now_utc_dt = Clock.timezone_utc_now()
                schedule_until_dt = Clock.add_days(now_utc_dt, 90)
                tracker_object.schedule_until = schedule_until_dt
        elif field_name == ConversationTag.TRACKER_PARTNER_EMAIL.value:
            tracker_object.partner_email = field_value
        tracker_object.save()

    @classmethod
    def reset_tracker(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id,
                                             context=context, topic=topic, function=SchedulerFunction.TRACKER.value)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            tracker_object.schedule_time = None
            tracker_object.schedule_until = None
            tracker_object.occurance = None
            tracker_object.partner_email = None
            tracker_object.skip_partner_email = False
            tracker_object.enabled = False
            tracker_object.save()

    @classmethod
    def check_if_tracker_enabled(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id,
                                             context=context, topic=topic, function=SchedulerFunction.TRACKER.value,
                                             enabled=True)
        return tracker_objects.exists()
        # return not cls.check_if_tracker_information_missing(app_id, app_user_id, context, topic)

    @classmethod
    def check_if_tracker_information_missing(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            partner_email_validated = False
            if tracker_object.skip_partner_email:
                partner_email_validated = True
            else:
                if tracker_object.partner_email:
                    partner_email_validated = True
            if any([not tracker_object.schedule_time, not tracker_object.schedule_until, not tracker_object.occurance,
                    not partner_email_validated]):
                return True
            return False
        else:
            return True

    @classmethod
    def get_tracker_summary_question(cls, app_id, app_user_id, context, topic):
        tracker_objects = ZabiaConversation.objects.filter(conversation_type=ConversationType.SCHEDULER_Q.value,
                                                           tag=ConversationTag.TRACKER_SUMMARY.value,
                                                           context=context,
                                                           topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            return tracker_object

    @classmethod
    def get_partner_email_question(cls, app_id, app_user_id, context, topic):
        tracker_objects = ZabiaConversation.objects.filter(conversation_type=ConversationType.SCHEDULER_Q.value,
                                                           tag=ConversationTag.TRACKER_PARTNER_EMAIL.value,
                                                           context=context,
                                                           topic=topic)
        if tracker_objects.exists():
            tracker_object = tracker_objects.first()
            return tracker_object

    @classmethod
    def get_next_tracker_question(cls, app_id, app_user_id, context, topic):
        tracker_objects = cls.objects.filter(app_id=app_id,
                                             app_user_id=app_user_id,
                                             function=SchedulerFunction.TRACKER.value,
                                             context=context,
                                             topic=topic)
        if not tracker_objects.exists():
            return ZabiaConversation.objects.filter(conversation_type=ConversationType.SCHEDULER_Q.value,
                                                    tag=ConversationTag.SCHEDULER_TIME.value,
                                                    context=context,
                                                    topic=topic).first()
        else:
            tracker_object = tracker_objects.first()
            if not tracker_object.schedule_time:
                return ZabiaConversation.objects.filter(conversation_type=ConversationType.SCHEDULER_Q.value,
                                                        tag=ConversationTag.SCHEDULER_TIME.value,
                                                        context=context,
                                                        topic=topic,
                                                        parent_id__isnull=True).first()
            elif not tracker_object.schedule_until:
                return ZabiaConversation.objects.filter(conversation_type=ConversationType.SCHEDULER_Q.value,
                                                        tag=ConversationTag.SCHEDULER_UNTIL.value,
                                                        context=context,
                                                        topic=topic,
                                                        parent_id__isnull=True).first()
            elif not tracker_object.occurance:
                return ZabiaConversation.objects.filter(conversation_type=ConversationType.SCHEDULER_Q.value,
                                                        tag=ConversationTag.SCHEDULER_OCCURANCE.value,
                                                        context=context,
                                                        topic=topic,
                                                        parent_id__isnull=True).first()
            elif not tracker_object.skip_partner_email and not tracker_object.partner_email:
                return ZabiaConversation.objects.filter(conversation_type=ConversationType.SCHEDULER_Q.value,
                                                        tag=ConversationTag.TRACKER_PARTNER_EMAIL.value,
                                                        context=context,
                                                        topic=topic).first()


