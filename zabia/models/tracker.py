from django.db import models
from core.models.base_entity import BaseEntity


class Tracker(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    context = models.CharField(max_length=500)
    topic = models.CharField(max_length=255)


class TrackerResponse(BaseEntity):
    tracker = models.ForeignKey(Tracker, on_delete=models.CASCADE)
    user_response = models.CharField(max_length=255)

