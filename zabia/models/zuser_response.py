from django.db import models
from core.models.base_entity import BaseEntity
from zabia.constants.enums import UserResponseType, SchedulerFunction
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser


class UserResponse(BaseEntity):
    response_type = models.CharField(max_length=20) # ResponseType.CHOICE_RESPONSE.value
    response_text = models.TextField(default="")
    activity = models.TextField(null=True, blank=True)


class ZUserResponse(BaseEntity):
    app_id = models.CharField(max_length=500)
    app_user_id = models.CharField(max_length=500)
    function = models.CharField(max_length=255, default=SchedulerFunction.TRACKER.value) # SchedulerFunction.TRACKER.value
    conversation = models.ForeignKey(ZabiaConversation, null=True, on_delete=models.CASCADE)
    context = models.CharField(max_length=255, null=True, blank=True) # Feel Good
    topic = models.CharField(max_length=255, null=True, blank=True) # Mood
    response = models.ManyToManyField(UserResponse)

    @classmethod
    def check_if_exists(cls, app_id, app_user_id, conversation_id, context, topic, function):
        return cls.objects.filter(app_id=app_id,
                                  app_user_id=app_user_id,
                                  conversation_id=conversation_id,
                                  context=context,
                                  topic=topic,
                                  function=function).exists()

    @classmethod
    def get_tracker_response_object(cls, app_id, app_user_id, context, topic):
        print(app_id)
        print(app_user_id)
        print(context)
        print(topic)
        tracker_response_objects = cls.objects.filter(app_id=app_id,
                                                      app_user_id=app_user_id,
                                                      function=SchedulerFunction.TRACKER.value,
                                                      context=context,
                                                      topic=topic)
        if tracker_response_objects.exists():
            tracker_response_object = tracker_response_objects.first()
            print(tracker_response_object.pk)
            return tracker_response_object

    @classmethod
    def check_if_exists2(cls, app_id, app_user_id, context, topic, function):
        return cls.objects.filter(app_id=app_id,
                                  app_user_id=app_user_id,
                                  context=context,
                                  topic=topic,
                                  function=function).exists()

    @classmethod
    def save_user_response(cls, app_id, app_user_id, conversation_id, context, topic,
                           response_type=UserResponseType.CHOICE_RESPONSE.value,
                           response_text=None, activity=None,
                           function=SchedulerFunction.TRACKER.value):
        if not response_text and not activity:
            return
        if not cls.check_if_exists2(app_id, app_user_id, context, topic, function=function):
            user_response_object = cls(app_id=app_id,
                                       app_user_id=app_user_id,
                                       context=context,
                                       topic=topic,
                                       function=function)
            user_response_object.save()
        else:
            user_response_object = cls.objects.filter(app_id=app_id,
                                                      app_user_id=app_user_id,
                                                      context=context,
                                                      topic=topic,
                                                      function=function).first()
        if response_text:
            response = UserResponse()
            response.response_type = response_type
            response.response_text = response_text
            response.save()
            user_response_object.response.add(response)
        else:
            response = user_response_object.response.order_by('id').first()
        return user_response_object

