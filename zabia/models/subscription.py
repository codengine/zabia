from django.db import models
from celery.task.control import revoke
from core.models.base_entity import BaseEntity
from zabia.models.zabia_user import ZUser


class Subscription(BaseEntity):
    user = models.ForeignKey(ZUser, on_delete=models.CASCADE)
    context = models.CharField(max_length=200)
    topic = models.CharField(max_length=200)
    status = models.BooleanField(default=False)
    task_id = models.CharField(max_length=500, null=True, blank=True)

    @classmethod
    def revoke_task(cls, task_id):
        try:
            revoke(task_id, terminate=True)
            return True
        except Exception as exp:
            return False

    @classmethod
    def get_task_id(cls, app_id, app_user_id, context, topic):
        user_object = cls.get_zuser(app_id, app_user_id)
        if not user_object:
            return None
        subscription_objects = cls.objects.filter(user_id=user_object.pk, context=context, topic=topic)
        if subscription_objects.exists():
            subscription_object = subscription_objects.first()
            return subscription_object.task_id

    @classmethod
    def update_task_id(cls, app_id, app_user_id, context, topic, task_id):
        user_object = cls.get_zuser(app_id, app_user_id)
        if not user_object:
            return False
        subscription_objects = cls.objects.filter(user_id=user_object.pk, context=context, topic=topic)
        if subscription_objects.exists():
            subscription_object = subscription_objects.first()
            subscription_object.task_id = task_id
            subscription_object.save()
            return subscription_object
        else:
            subscription_object = cls(user_id=user_object.pk, context=context, topic=topic, task_id=task_id)
            subscription_object.save()
            return subscription_object
    
    @classmethod
    def get_zuser(cls, app_id, app_user_id):
        user_objects = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if user_objects.exists():
            return user_objects.first()
    
    @classmethod
    def check_if_user_subscribed(cls, app_id, app_user_id, context, topic):
        user_object = cls.get_zuser(app_id, app_user_id)
        if not user_object:
            return False
        subscription_objects = cls.objects.filter(user_id=user_object.pk, context=context, topic=topic)
        if subscription_objects.exists():
            subscription_object = subscription_objects.first()
            return subscription_object.status
        return False
        
    @classmethod
    def change_upscription_status(cls,app_id, app_user_id, context, topic, status):
        user_object = cls.get_zuser(app_id, app_user_id)
        if not user_object:
            return False
        subscription_objects = cls.objects.filter(user_id=user_object.pk, context=context, topic=topic)
        if subscription_objects.exists():
            subscription_object = subscription_objects.first()
            subscription_object.status = status
            subscription_object.save()
            return subscription_object
        else:            
            subscription_object = cls(user_id=user_object.pk, context=context, topic=topic, status=status)
            subscription_object.save()
            return subscription_object
        
    @classmethod
    def enable_subscription(cls, app_id, app_user_id, context, topic):
        return cls.change_upscription_status(app_id, app_user_id, context, topic, True)
        
    @classmethod
    def disable_subscription(cls, app_id, app_user_id, context, topic):
        return cls.change_upscription_status(app_id, app_user_id, context, topic, False)
