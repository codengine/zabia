from django.db import models
from core.models.base_entity import BaseEntity


class Location(BaseEntity):
    name = models.CharField(max_length=500)
    lat = models.CharField(max_length=20, null=True, blank=True)
    long = models.CharField(max_length=20, null=True, blank=True)
