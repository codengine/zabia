from django.db import models
from core.models.base_entity import BaseEntity


class Reminder(BaseEntity):
    topic = models.CharField(max_length=500, null=True, blank=True)
    message = models.TextField(null=True, blank=True)
