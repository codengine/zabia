from django.db import models
from core.models.base_entity import BaseEntity


class QStack(BaseEntity):
    app_id = models.CharField(max_length=500, null=True, blank=True)
    app_user_id = models.CharField(max_length=500, null=True, blank=True)
    conversation_item_id = models.CharField(max_length=255, null=True, blank=True)

    @classmethod
    def push(cls, app_id, app_user_id, conversation_item_id):
        qstack_object = cls(app_id=app_id, app_user_id=app_user_id, conversation_item_id=conversation_item_id)
        qstack_object.save()

    @classmethod
    def top(cls, app_id, app_user_id):
        qstack_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if qstack_objects.exists():
            conversation_item_id = qstack_objects.first().conversation_item_id
            return conversation_item_id

    @classmethod
    def pop(cls, app_id, app_user_id):
        qstack_objects = cls.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if qstack_objects.exists():
            conversation_item_id = qstack_objects.first().conversation_item_id
            qstack_objects.first().delete()
            return conversation_item_id

    @classmethod
    def clear(cls, app_id, app_user_id):
        cls.objects.filter(app_id=app_id, app_user_id=app_user_id).delete()
