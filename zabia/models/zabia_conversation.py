from random import randint
import json
from django.db import models
from core.models.base_entity import BaseEntity
from zabia.constants.enums import BotAskingMode, ConversationTag, ConversationType, Presentation, ContentSource
from zabia.models.managers.objects_query_manager import ZabiaConversationQueryManager
from zabia.models.managers.zabia_basic_questions_querymanager import ZabiaBasicQuestionsModelManager
from zabia.models.managers.zabia_introduction_query_manager import ZabiaIntroductionModelManager


class ZabiaConversationText(BaseEntity):
    text = models.TextField()
    text_en = models.TextField(default='')
    description = models.TextField(default='')
    description_en = models.TextField(default='')

    def has_placeholder(self):
        contains_special_chars = lambda x: "[" in x and "]" in x
        return contains_special_chars(self.text) or contains_special_chars(self.text_en) or contains_special_chars(self.description) or contains_special_chars(self.description_en)

    def replace_placeholders(self, **kwargs):
        return self.text.replace("[Name]", kwargs.get('[Name]', '')).replace('[Gender]', kwargs.get('[Gender]', '')).replace('[Sex]', kwargs.get('[Sex]', '')).replace('[Date]', kwargs.get('[Date]', ''))

    def as_json(self):
        return {
            'text': self.text,
            'text_en': self.text_en,
            'description': self.description,
            'description_en': self.description_en
        }
    
    @classmethod
    def add(cls, text, text_en='', description='', description_en=''):
        if any([not text]):
            return
        text_instances = cls.objects.filter(text=text)
        if text_instances.exists():
            return text_instances.first()
        else:
            text_instance = cls()
            
        text_instance.text = text
        if text_en:
            text_instance.text_en = text_en
            
        if description:
            text_instance.description = description
            
        if description_en:
            text_instance.description_en = description_en
            
        text_instance.save()
        
        return text_instance


class ZabiaConversationTag(BaseEntity):
    name = models.CharField(max_length=255)


class ZabiaConversation(BaseEntity):
    parent = models.ForeignKey("self", null=True, blank=True, on_delete=models.CASCADE)
    texts = models.ManyToManyField(ZabiaConversationText)
    content_source = models.CharField(max_length=100, default=ContentSource.FROM_CONV_TEXT.value) # ContentSource.FROM_CONV_TEXT.value
    tag = models.CharField(max_length=255)  # ConversationTag.NAME.value
    topic = models.CharField(max_length=500, null=True, blank=True) # e.g Mood, Tips
    context = models.CharField(max_length=255, null=True, blank=True) # Feel Good
    payload = models.CharField(max_length=500, null=True, blank=True) # Feliz In case it is an option
    presentation = models.CharField(max_length=50) # Presentation.LINK.value
    image = models.TextField(null=True, blank=True)
    image_type = models.CharField(max_length=20, null=True, blank=True)
    order = models.IntegerField(default=0)
    conversation_type = models.CharField(max_length=20)  # QuestionType.FIRST_TIME_INTRODUCTION.value
    user_response_type = models.CharField(max_length=20, null=True) # UserResponseType.TEXT_RESPONSE.value
    user_response_content_type = models.CharField(max_length=20, null=True) # UserResponseContentType.TEXT.value
    bot_asking_mode = models.CharField(max_length=20, default=BotAskingMode.SAY.value) # BotAskingMode.SAY.value

    post_action = models.CharField(max_length=255, null=True, blank=True) # PostAction.SAVE.value
    post_action_intent = models.CharField(max_length=100, null=True, blank=True) # PostActionIntent.POSITIVE.value

    save_param = models.TextField(null=True, blank=True)

    send_mail_params = models.TextField(null=True, blank=True) # json

    required_profile = models.CharField(max_length=255, null=True, blank=True)

    objects = ZabiaConversationQueryManager()
    zintroductionmanager = ZabiaIntroductionModelManager()
    basicquestionsmanager = ZabiaBasicQuestionsModelManager()

    @classmethod
    def get_main_menu_by_context(cls, context):
        return cls.objects.filter(conversation_type=ConversationType.MAIN_MENU.value, parent_id__isnull=True,
                                  tag=ConversationTag.ZABIA_MAIN_MENU.value, context=context).first()

    @classmethod
    def get_child_menu(cls, context, topic, parent_menu_id):
        return cls.objects.filter(parent_id=parent_menu_id,context=context, topic=topic).first()

    def pick_conversation_text(self):
        try:
            texts = self.texts.all()
            text_instance_count = texts.count()
            text_instance = texts[randint(0, text_instance_count - 1)]
            return text_instance
        except Exception as exp:
            pass

    @classmethod
    def exists(cls, tag, conversation_type):
        return cls.objects.filter(tag=tag, conversation_type=conversation_type).exists()

    @classmethod
    def add(cls, texts, tag, conversation_type, presentation,
            bot_asking_mode, order=0, parent_id=None,
            image=None, image_type=None, user_response_type=None,
            user_response_content_type=None,
            post_action=None, post_action_intent=None, save_param=None,
            send_mail_params={},
            context = None,
            topic = None,
            payload=None,
            required_profile=None
            ):
        if any([not texts, not tag, not conversation_type, not presentation, not bot_asking_mode]):
            return None
        try:
            z_q = cls()
            z_q.tag = tag
            z_q.presentation = presentation
            if order:
                z_q.order = order
            z_q.conversation_type = conversation_type
            if context:
                z_q.context = context
            if topic:
                z_q.topic = topic
            if payload:
                z_q.payload = payload
            if image:
                z_q.image = image
            if image_type:
                z_q.image_type = image_type
            if user_response_type:
                z_q.user_response_type = user_response_type
            if user_response_content_type:
                z_q.user_response_content_type = user_response_content_type
            z_q.bot_asking_mode = bot_asking_mode
            if parent_id:
                z_q.parent_id = parent_id
            if post_action:
                z_q.post_action = post_action
            if post_action_intent:
                z_q.post_action_intent = post_action_intent
            if save_param:
                z_q.save_param = save_param
            if send_mail_params:
                z_q.send_mail_params = json.dumps(send_mail_params)
            if required_profile:
                z_q.required_profile = required_profile

            z_q.save()
            z_q.texts.add(*texts)
            return z_q
        except Exception as exp:
            pass

    @classmethod
    def add_zabia_self_introduction(cls, text_list=[], order=1):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                                         tag=ConversationTag.ZABIA_SELF_INTRO.value,
                                         conversation_type=ConversationType.ZABIA_INTRODUCTION.value,
                                         presentation=Presentation.INFO.value,
                                         bot_asking_mode=BotAskingMode.SAY.value,
                                         order=order)
        return instance

    @classmethod
    def add_zabia_basic_question(cls, text_list, tag, presentation, bot_asking_mode, order=0, parent_id=None,
                                 image=None, user_response_type=None, user_response_content_type=None,
                                 post_action=None, post_action_intent=None, save_param=None,
                                 send_mail_params={}):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                                         tag=tag,
                                         conversation_type=ConversationType.BASIC_INFO.value,
                                         presentation=presentation,
                                         bot_asking_mode=bot_asking_mode,
                                         order=order,
                                         image=image,
                                         parent_id=parent_id,
                                         user_response_type=user_response_type,
                                         user_response_content_type=user_response_content_type,
                                         post_action=post_action,
                                         post_action_intent=post_action_intent,
                                         save_param=save_param,
                                         send_mail_params=send_mail_params)
        return instance

    @classmethod
    def add_main_menu_item(cls, text_list, tag, presentation, bot_asking_mode, order=0, parent_id=None,
                                 image=None, image_type=None, user_response_type=None, user_response_content_type=None,
                                 post_action=None, post_action_intent=None, save_param=None, context=None,
                                 send_mail_params={}):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                                         tag=tag,
                                         conversation_type=ConversationType.MAIN_MENU.value,
                                         presentation=presentation,
                                         bot_asking_mode=bot_asking_mode,
                                         order=order,
                                         image=image,
                                         image_type=image_type,
                                         parent_id=parent_id,
                                         user_response_type=user_response_type,
                                         user_response_content_type=user_response_content_type,
                                         post_action=post_action,
                                         post_action_intent=post_action_intent,
                                         save_param=save_param,
                                         context=context,
                                         send_mail_params=send_mail_params)
        return instance

    @classmethod
    def add_flow_item(cls, text_list, tag, presentation, bot_asking_mode, order=0, parent_id=None,
                      image=None, image_type=None, user_response_type=None, user_response_content_type=None,
                      post_action=None, post_action_intent=None, save_param=None,
                      send_mail_params={}, context=None, topic=None, payload=None,
                      required_profile=None):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                                         tag=tag,
                                         conversation_type=ConversationType.ZABIA_FLOW.value,
                                         presentation=presentation,
                                         bot_asking_mode=bot_asking_mode,
                                         order=order,
                                         image=image,
                                         image_type=image_type,
                                         parent_id=parent_id,
                                         user_response_type=user_response_type,
                                         user_response_content_type=user_response_content_type,
                                         post_action=post_action,
                                         post_action_intent=post_action_intent,
                                         save_param=save_param,
                                         send_mail_params=send_mail_params,
                                         context=context,
                                         topic=topic,
                                         payload=payload,
                                         required_profile=required_profile)
        return instance

    @classmethod
    def add_scheduler_item(cls, text_list, tag, presentation, bot_asking_mode, order=0, parent_id=None,
                      image=None, image_type=None, user_response_type=None, user_response_content_type=None,
                      post_action=None, post_action_intent=None, save_param=None,
                      send_mail_params={}, context=None, topic=None, payload=None):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                                         tag=tag,
                                         conversation_type=ConversationType.SCHEDULER_Q.value,
                                         presentation=presentation,
                                         bot_asking_mode=bot_asking_mode,
                                         order=order,
                                         image=image,
                                         image_type=image_type,
                                         parent_id=parent_id,
                                         user_response_type=user_response_type,
                                         user_response_content_type=user_response_content_type,
                                         post_action=post_action,
                                         post_action_intent=post_action_intent,
                                         save_param=save_param,
                                         send_mail_params=send_mail_params,
                                         context=context,
                                         topic=topic,
                                         payload=payload)
        return instance
        
    @classmethod
    def add_tracker_question_item(cls, text_list, tag, presentation, bot_asking_mode, order=0, parent_id=None,
                      image=None, image_type=None, user_response_type=None, user_response_content_type=None,
                      post_action=None, post_action_intent=None, save_param=None,
                      send_mail_params={}, context=None, topic=None, payload=None):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                                         tag=tag,
                                         conversation_type=ConversationType.TRACKER_INPUT_Q.value,
                                         presentation=presentation,
                                         bot_asking_mode=bot_asking_mode,
                                         order=order,
                                         image=image,
                                         image_type=image_type,
                                         parent_id=parent_id,
                                         user_response_type=user_response_type,
                                         user_response_content_type=user_response_content_type,
                                         post_action=post_action,
                                         post_action_intent=post_action_intent,
                                         save_param=save_param,
                                         send_mail_params=send_mail_params,
                                         context=context,
                                         topic=topic,
                                         payload=payload)
        return instance
        
    @classmethod
    def add_subscription_item(cls, text_list, tag, presentation, bot_asking_mode, order=0, parent_id=None,
                      image=None, image_type=None, user_response_type=None, user_response_content_type=None,
                      post_action=None, post_action_intent=None, save_param=None,
                      send_mail_params={}, context=None, topic=None, payload=None):
        text_instances = []
        for text in text_list:
            text_instance = ZabiaConversationText.add(text=text)
            text_instances += [text_instance]

        instance = ZabiaConversation.add(texts=text_instances,
                                         tag=tag,
                                         conversation_type=ConversationType.SUBSCRIPTION_Q.value,
                                         presentation=presentation,
                                         bot_asking_mode=bot_asking_mode,
                                         order=order,
                                         image=image,
                                         image_type=image_type,
                                         parent_id=parent_id,
                                         user_response_type=user_response_type,
                                         user_response_content_type=user_response_content_type,
                                         post_action=post_action,
                                         post_action_intent=post_action_intent,
                                         save_param=save_param,
                                         send_mail_params=send_mail_params,
                                         context=context,
                                         topic=topic,
                                         payload=payload)
        return instance


