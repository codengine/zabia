from django.db import models


class ZabiaConversationQueryManager(models.Manager):

    def get_queryset(self):
        queryset = super(ZabiaConversationQueryManager, self).get_queryset().order_by('order')
        return queryset
