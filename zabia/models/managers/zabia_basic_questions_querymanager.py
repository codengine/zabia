from django.db import models

from zabia.constants.enums import ConversationTag, ConversationType


class ZabiaBasicQuestionsModelManager(models.Manager):

    def get_queryset(self):
        queryset = super(ZabiaBasicQuestionsModelManager, self).get_queryset()

        queryset = queryset.filter(conversation_type=ConversationType.BASIC_INFO.value,
                                   ).order_by('order')

        return queryset
