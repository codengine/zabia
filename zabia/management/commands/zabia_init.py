from django.core.management.base import BaseCommand

from zabia.management.utils.scheduler.tracker.tracker_mood import build_tracker_mood
from zabia.management.utils.zabia_introduction import zabia_introduction
from zabia.management.utils.basic_questions import init_basic_questions
from zabia.management.utils.chatflow import init_chatflow


class Command(BaseCommand):
    def handle(self, *args, **options):
        print("Initializing Zabia Introduction...")
        zabia_introduction()
        print("Zabia Introduction Initiaized")
        print("Initializing basic questions")
        init_basic_questions()
        print("basic questions initialized")
        print("Initializing chat flow")
        init_chatflow()
        print("Chatflow initialized")
        print("Initializing Trackers")
        build_tracker_mood()
        print("Mood Tracker Enabled")


        
        





