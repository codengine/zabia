import io
import boto3
import matplotlib

from engine.clock.Clock import Clock
from zabia.models.subscription import Subscription
from zabia.tasks import wake_up_tracker

matplotlib.use('agg')
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import uuid
from django.utils import timezone
from django.core.management.base import BaseCommand
from zabia.handlers.api_handlers import APIHandler
from zabia.models.scheduler import Scheduler
from zabia.models.zabia_conversation import ZabiaConversation, ZabiaConversationText
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_response import UserResponse, ZUserResponse
from zabia.plot.mood_chart import MoodChart
from zabia.tracker_kit import TrackerKit
from zabia.utils.aws_s3 import AWSS3
from celery.task.control import revoke


def get_celery_queue_items(queue_name):
    import base64
    import json

    # Get a configured instance of a celery app:
    from zabia.celery import app as celery_app

    with celery_app.pool.acquire(block=True) as conn:
        tasks = conn.default_channel.client.lrange(queue_name, 0, -1)
        decoded_tasks = []

    for task in tasks:
        j = json.loads(task)
        body = json.loads(base64.b64decode(j['body']))
        decoded_tasks.append(body)

    return decoded_tasks


class Command(BaseCommand):
    def handle(self, *args, **options):
        # items = get_celery_queue_items('zabia')
        # print(items)
        # return
        # offset = Clock.timezone_to_offset('America/Los_Angeles')
        # print(offset)
        # # Subscription.revoke_task(task_id='d4c499a5-ff1f-4d26-8123-93541a7d22ed')
        # # Subscription.revoke_task(task_id='64a469eb-8d82-4c6a-a2f0-26adda2246cf')
        # revoke('d4c499a5-ff1f-4d26-8123-93541a7d22ed', terminate=True)
        # revoke('64a469eb-8d82-4c6a-a2f0-26adda2246cf', terminate=True)
        # return
        # response = APIHandler.get_all_questions(user_id="76", question_type="Estilo de vida") #.call_restaurant_api(lat='-12.0463731', lng='-77.042754')
        # print(response)
        # # print(type(response['data'][0]['url_logo']))
        # return
        # mail_data = {
        #     "email": "codenginebd@gmail.com",
        #     "name": "Maria Garcia Smith",
        #     "subject": "This is a test email for Maria",
        #     "body": "<h1>It Worked!</h1> <br /> <p>Hello Maria, this is test email message to show you. Now "
        #             "we need to incorporate the html template here.<b>12 m 17-08-2018</b> </p>",
        #     "isHtml": 1
        #     }
        # response = APIHandler.trigger_send_mail(mail_data=mail_data)
        # print(response)
        # return
        wake_up_tracker(app_id='5b814999467817002244ca3f',
                        app_user_id='bed425a377addfe773cd563c',
                        topic='MOOD',
                        context='SENTIRSE BIEN')
        return
        #
        # app_id = 'test_1'
        # app_user_id = 'test_1'
        # context = 'text_context'
        # topic = 'test_topic'

        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=7585,
        #                                  response_text="Sad")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=7585,
        #                                  response_text="Fearfully")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=7585,
        #                                  response_text="Normal")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=7585,
        #                                  response_text="Happy")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=7585,
        #                                  response_text="Upset")

        # z_user_response = ZUserResponse.save_user_response(app_id=app_id,
        #                                                    app_user_id=app_user_id,
        #                                                    context=context,
        #                                                    topic=topic,
        #                                                    conversation_id=7585,
        #                                                    response_text="Happy")
        #
        # mood_chart = MoodChart()
        # file_memory = mood_chart.draw_in_memory(z_user_response)
        #
        # file_url = AWSS3.upload_file_from_memory(file_memory, 'test.png', '')
        # print(file_url)
        # return
        #
        # tracker_kit = TrackerKit()
        # tracker_kit.wake_up_tracker(app_id='5b5a118f35065c0021dba788',
        #                             app_user_id='9fdb08b27d73f57fd1ccd3e5',
        #                             context='SENTIRSE BIEN',
        #                             topic='MOOD')
        # return
        #
        # app_id = 'test_1'
        # app_user_id = 'test_1'
        # context = 'text_context'
        # topic = 'test_topic'
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=6343,
        #                                  response_text="Sad")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=6343,
        #                                  response_text="Fearfully")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=6343,
        #                                  response_text="Normal")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=6343,
        #                                  response_text="Happy")
        #
        # ZUserResponse.save_user_response(app_id=app_id,
        #                                  app_user_id=app_user_id,
        #                                  context=context,
        #                                  topic=topic,
        #                                  conversation_id=6343,
        #                                  response_text="Upset")
        #
        # z_user_response = ZUserResponse.save_user_response(app_id=app_id,
        #                                                    app_user_id=app_user_id,
        #                                                    context=context,
        #                                                    topic=topic,
        #                                                    conversation_id=6343,
        #                                                    response_text="Happy")
        #
        # mood_chart = MoodChart()
        # mood_chart.draw(z_user_response)










