from django.contrib.auth.models import User
from django.core.management.base import BaseCommand

from zabia.handlers.api_handlers import APIHandler
from zabia.models.scheduler import Scheduler
from zabia.models.zabia_conversation import ZabiaConversation, ZabiaConversationText
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait
from zabia.models.zuser_response import UserResponse, ZUserResponse
from zabia.plot.mood_chart import MoodChart


class Command(BaseCommand):
    def handle(self, *args, **options):
        ZUserInputAwait.objects.all().delete()
        User.objects.all().delete()
        ZUser.objects.all().delete()
        ZabiaConversation.objects.all().delete()
        ZabiaConversationText.objects.all().delete()
        Scheduler.objects.all().delete()
        UserResponse.objects.all().delete()
        ZUserResponse.objects.all().delete()

        # next_schedule_time = Scheduler.next_reschedule_time(app_id="5b47904d4ce27e0022a1b10c",
        #                                                     app_user_id="8f53db62d7ae382f4f23c54b",
        #                                                     context="SENTIRSE BIEN",
        #                                                     topic="MOOD",
        #                                                     )
        # print(next_schedule_time)
        # response = APIHandler.call_tips_api()
        # print(response)
        # chart = MoodChart()
        # chart.test_draw(z_user_response_object=None)
        # response = APIHandler.call_tips_api(tags=["diabetes"])
        # print(response)
        # response = APIHandler.call_restaurant_api()
        # print(response)
        # response = APIHandler.call_recipe_details_by_id(recipe_id=227)
        # print(response)
        # response = APIHandler.call_tips_api(tags=["diabetes"])
        # print(response)
        # !/usr/bin/env python
        # import datetime, pytz
        #
        # offset = datetime.datetime.now(pytz.timezone('Asia/Dhaka')).strftime('%z')
        # print(float(offset))









