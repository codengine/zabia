from zabia.constants.enums import ConversationTag, Presentation, BotAskingMode, PostAction, PostActionIntent, Context, \
    TrackerTopic, REMINDER_TOPIC
from zabia.constants.health_profile import QuestionType
from zabia.models.main_menu import MainMenu
from zabia.models.zabia_conversation import ZabiaConversation


def add_enable_subscription_item(context, topic, message):
    texts = [
        message
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.ENABLE_SUBSCRIPTION_Q.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "Yes"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.ENABLE_SUBSCRIPTION_Q.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.ENABLE_SUBSCRIPTION.value,
                                                       context=context,
                                                       topic=topic
                                                       )

    texts = [
        "Cancel"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.ENABLE_SUBSCRIPTION_Q.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.IGNORE_SUBSCRIPTION.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload=context
                                                       )


def add_disable_subscription_item(context, topic, message):
    texts = [
        message
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.DISABLE_SUBSCRIPTION_Q.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "Yes"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.DISABLE_SUBSCRIPTION_Q.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.DISABLE_SUBSCRIPTION.value,
                                                       context=context,
                                                       topic=topic
                                                       )

    texts = [
        "No"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.DISABLE_SUBSCRIPTION_Q.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.IGNORE_UNSUBSCRIPTION.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload=context
                                                       )
                                                       

def create_main_menu_item(text, image, image_type, order):
    texts = [
        text
    ]

    instance_ = ZabiaConversation.add_main_menu_item(text_list=texts, tag=ConversationTag.ZABIA_MAIN_MENU.value,
                                                     presentation=Presentation.CAROUSEL.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value,
                                                     order=order,
                                                     image=image,
                                                     image_type=image_type,
                                                     context=text)
    return instance_


def add_main_menu1():
    instance1 = create_main_menu_item(Context.COMER_SANO.value,
                                      'https://www.southtech.pe/zabiatest/imagen/icono/mandarinas.png',
                                      'image/png',
                                      order=1)
    texts = [
        "How I can help?",
        "How can I help please, [Name]?"
    ]

    instance1_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                  presentation=Presentation.ACTIONS.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                  parent_id=instance1.pk)
    if not instance1_1:
        return
    texts = [
        "Recipes"
    ]
    instance1_1_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                    parent_id=instance1_1.pk,
                                                    post_action=PostAction.SHOW_RECIPES.value,
                                                    context=Context.COMER_SANO.value,
                                                    topic=REMINDER_TOPIC.RECIPES.value,
                                                    required_profile=QuestionType.NUTRITION.value+","+QuestionType.DIET.value)
    add_enable_subscription_item(Context.COMER_SANO.value, REMINDER_TOPIC.RECIPES.value,
                                 "Do you want to subscribe for Daily Recipes?")
    add_disable_subscription_item(Context.COMER_SANO.value, REMINDER_TOPIC.RECIPES.value,
                                  "Do you want to unsubscribe for Daily Recipes?")

    texts = [
        "Restaurants"
    ]
    instance1_1_2 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                    parent_id=instance1_1.pk,
                                                    post_action=PostAction.SHOW_RESTAURANTS.value,
                                                    context=Context.COMER_SANO.value,
                                                    topic=REMINDER_TOPIC.RESTAURANTS.value)
    add_enable_subscription_item(Context.COMER_SANO.value, REMINDER_TOPIC.RESTAURANTS.value,
                                 "Do you want to subscribe for Daily nearby Restaurants?")
    add_disable_subscription_item(Context.COMER_SANO.value, REMINDER_TOPIC.TIPS.value,
                                  "Do you want to unsubscribe for Daily nearby Restaurants?")

    texts = [
        "Tips"
    ]
    instance1_1_3 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                    parent_id=instance1_1.pk,
                                                    post_action=PostAction.SHOW_TIPS.value,
                                                    context=Context.COMER_SANO.value,
                                                    topic=REMINDER_TOPIC.TIPS.value,
                                                    required_profile=QuestionType.LIFESTYLE.value)
    add_enable_subscription_item(Context.COMER_SANO.value, REMINDER_TOPIC.TIPS.value,
                                 "Do you want to subscribe for Daily Tips?")
    add_disable_subscription_item(Context.COMER_SANO.value, REMINDER_TOPIC.TIPS.value,
                                  "Do you want to unsubscribe for Daily Tips?")

    texts = [
        "Nutrients"
    ]
    instance1_1_4 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=4,
                                                    parent_id=instance1_1.pk,
                                                    required_profile=QuestionType.NUTRITION.value)

    texts = [
        "Facts"
    ]
    instance1_1_5 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=5,
                                                    parent_id=instance1_1.pk)

    texts = [
        "Shopping list"
    ]
    instance1_1_6 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=6,
                                                    parent_id=instance1_1.pk)


def add_main_menu2():
    instance2 = create_main_menu_item(Context.MANTENTE_ACTIVO.value,
                                      'https://www.southtech.pe/zabiatest/imagen/icono/mantenerseactivo.png',
                                      'image/png',
                                      order=2)

    texts = [
        "Cómo puedo ayudar?",
        "¿Cómo puedo ayudar por favor, [Name]?"
    ]

    instance2_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                  presentation=Presentation.ACTIONS.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                  parent_id=instance2.pk)
    if not instance2_1:
        return

    texts = [
        "Activame"
    ]
    instance2_1_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                    parent_id=instance2_1.pk)
    texts = [
        "Yoga"
    ]
    instance2_1_2 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                    parent_id=instance2_1.pk)
    texts = [
        "Tips"
    ]
    instance2_1_3 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                    parent_id=instance2_1.pk)
    texts = [
        "Lugares"
    ]
    instance2_1_4 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=4,
                                                    parent_id=instance2_1.pk)


def add_tracker_mood_option_item(text, context, topic, parent_instance, order=1):
    texts = [
        text
    ]
    instance = ZabiaConversation.add_flow_item(text_list=texts,
                                               tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                               presentation=Presentation.REPLY.value,
                                               bot_asking_mode=BotAskingMode.OPTION.value, order=order,
                                               parent_id=parent_instance.pk,
                                               post_action=PostAction.SAVE_FLOW.value,
                                               post_action_intent=PostActionIntent.INPUT_FROM_USER.value,
                                               context=context,
                                               topic=topic,
                                               payload=text
                                               )
    return instance


def add_tracker_say_item(text, parent_instance, context, topic, order=1):
    texts = [
        text
    ]
    instance = ZabiaConversation.add_flow_item(text_list=texts,
                                               tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                               presentation=Presentation.TEXT.value,
                                               bot_asking_mode=BotAskingMode.SAY.value, order=order,
                                               parent_id=parent_instance.pk,
                                               # post_action=PostAction.START_TRACKER.value,
                                               context=context,
                                               topic=topic,
                                               )
    return instance


def add_activity_item(text, parent_object, order, payload, context=Context.SENTIRSE_BIEN.value,
                      topic=TrackerTopic.MOOD.value,
                      post_action=PostAction.START_TRACKER.value):
    texts = [
        text
    ]
    activity_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                  tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                                  presentation=Presentation.REPLY.value,
                                                                  bot_asking_mode=BotAskingMode.OPTION.value,
                                                                  order=order,
                                                                  parent_id=parent_object.pk,
                                                                  post_action=post_action,
                                                                  context=context,
                                                                  topic=topic,
                                                                  payload=payload
                                                                  )
    return activity_object


def add_activity_question(question_text, parent_object, context, topic):
    texts = [
        question_text
    ]
    activity_parent_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                         tag=ConversationTag.TRACKER_QUESTION.value,
                                                                         presentation=Presentation.ACTIONS.value,
                                                                         bot_asking_mode=BotAskingMode.OPTION.value,
                                                                         parent_id=parent_object.pk,
                                                                         context=context,
                                                                         topic=topic)
    activity_object_1 = add_activity_item("Relax", activity_parent_object, 1, "Relax", context=context, topic=topic)

    activity_object_2 = add_activity_item("Work", activity_parent_object, 2, "Work", context=context, topic=topic)

    activity_object_3 = add_activity_item("Sport", activity_parent_object, 3, "Sport", context=context, topic=topic)

    activity_object_4 = add_activity_item("Meal", activity_parent_object, 4, "Meal", context=context, topic=topic)

    activity_object_5= add_activity_item("Shopping", activity_parent_object, 5, "Shopping", context=context, topic=topic)

    activity_object_6 = add_activity_item("None", activity_parent_object, 6, "None", context=context, topic=topic)

    return activity_parent_object


def add_main_menu3():
    context = Context.SENTIRSE_BIEN.value
    instance3 = create_main_menu_item(Context.SENTIRSE_BIEN.value,
                                      'https://www.southtech.pe/zabiatest/imagen/icono/sentirsebien.png',
                                      'image/png',
                                      order=3)

    texts = [
        "Please, choose an option below",
        "How can I help please, [Name]?"
    ]

    instance3_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                  presentation=Presentation.ACTIONS.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                  parent_id=instance3.pk)
    if not instance3_1:
        return

    texts = [
        "Tips"
    ]
    instance3_1_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                    parent_id=instance3_1.pk,
                                                    post_action=PostAction.SHOW_TIPS.value,
                                                    context=context,
                                                    topic=REMINDER_TOPIC.TIPS.value,
                                                    required_profile=QuestionType.LIFESTYLE.value)
    add_enable_subscription_item(context, REMINDER_TOPIC.TIPS.value, "Do you want to subscribe for Daily Tips?")
    add_disable_subscription_item(context, REMINDER_TOPIC.TIPS.value, "Do you want to unsubscribe for Daily Tips?")
    
    texts = [
        "Mood"
    ]
    instance3_1_2 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                    parent_id=instance3_1.pk,
                                                    required_profile=QuestionType.MOOD.value)

    texts = [
        "How do you feel today?"
    ]
    instance3_1_2_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                      tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                      presentation=Presentation.ACTIONS.value,
                                                      bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                      parent_id=instance3_1_2.pk)
    if not instance3_1_2_1:
        return

    instance3_1_2_1_1 = add_tracker_mood_option_item("Happy",
                                                     context=Context.SENTIRSE_BIEN.value,
                                                     topic=TrackerTopic.MOOD.value,
                                                     parent_instance=instance3_1_2_1,
                                                     order=1)
    instance3_1_2_1_1_1 = add_tracker_say_item("Being happy is a habit",
                                               parent_instance=instance3_1_2_1_1,
                                               context=Context.SENTIRSE_BIEN.value,
                                               topic=TrackerTopic.MOOD.value,
                                               order=1)
    add_activity_question("What are you doing?", instance3_1_2_1_1_1, context=Context.SENTIRSE_BIEN.value,
                          topic=TrackerTopic.MOOD.value)

    instance3_1_2_1_2 = add_tracker_mood_option_item("Sad",
                                                     context=Context.SENTIRSE_BIEN.value,
                                                     topic=TrackerTopic.MOOD.value,
                                                     parent_instance=instance3_1_2_1,
                                                     order=2)
    text = "There are negative moments in our lives that we will not be " \
           "able to control but we can choose how we do not feel about it."
    instance3_1_2_1_2_1 = add_tracker_say_item(text,
                                               parent_instance=instance3_1_2_1_2,
                                               context=Context.SENTIRSE_BIEN.value,
                                               topic=TrackerTopic.MOOD.value,
                                               order=1)
    add_activity_question("What did you do?", instance3_1_2_1_2_1, context=Context.SENTIRSE_BIEN.value,
                         topic=TrackerTopic.MOOD.value)


    instance3_1_2_1_3 = add_tracker_mood_option_item("Fearfully",
                                                     context=Context.SENTIRSE_BIEN.value,
                                                     topic=TrackerTopic.MOOD.value,
                                                     parent_instance=instance3_1_2_1,
                                                     order=3)
    text = "Fear is an unpleasant sensation caused by the perception of a real or imaginary danger "
    instance3_1_2_1_3_1 = add_tracker_say_item(text,
                                               parent_instance=instance3_1_2_1_3,
                                               context=Context.SENTIRSE_BIEN.value,
                                               topic=TrackerTopic.MOOD.value,
                                               order=1)
    add_activity_question("What did you do?", instance3_1_2_1_3_1, context=Context.SENTIRSE_BIEN.value,
                          topic=TrackerTopic.MOOD.value)

    instance3_1_2_1_4 = add_tracker_mood_option_item("Upset",
                                                     context=Context.SENTIRSE_BIEN.value,
                                                     topic=TrackerTopic.MOOD.value,
                                                     parent_instance=instance3_1_2_1,
                                                     order=4)
    text = "Anger is a negative feeling and the best thing is that we externalize it " \
           "in some way to prevent it from becoming an illness."
    instance3_1_2_1_4_1 = add_tracker_say_item(text,
                                               parent_instance=instance3_1_2_1_4,
                                               context=Context.SENTIRSE_BIEN.value,
                                               topic=TrackerTopic.MOOD.value,
                                               order=1)
    add_activity_question("What did you do?", instance3_1_2_1_4_1, context=Context.SENTIRSE_BIEN.value,
                          topic=TrackerTopic.MOOD.value)

    instance3_1_2_1_5 = add_tracker_mood_option_item("Normal",
                                                     context=Context.SENTIRSE_BIEN.value,
                                                     topic=TrackerTopic.MOOD.value,
                                                     parent_instance=instance3_1_2_1,
                                                     order=5)

    text = "Sounds good!"
    instance3_1_2_1_5_1 = add_tracker_say_item(text,
                                               parent_instance=instance3_1_2_1_5,
                                               context=Context.SENTIRSE_BIEN.value,
                                               topic=TrackerTopic.MOOD.value,
                                               order=1)

    add_activity_question("What were you doing?", instance3_1_2_1_5_1, context=Context.SENTIRSE_BIEN.value,
                          topic=TrackerTopic.MOOD.value)

    texts = [
        "Mindfullness"
    ]
    instance3_1_3 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                    parent_id=instance3_1.pk)

    texts = [
        "Meditation"
    ]
    instance3_1_4 = ZabiaConversation.add_flow_item(text_list=texts,
                                                    tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=4,
                                                    parent_id=instance3_1.pk)


def init_chatflow():
    add_main_menu1()
    add_main_menu2()
    add_main_menu3()

