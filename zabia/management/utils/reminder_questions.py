from zabia.constants.enums import ConversationTag, Presentation, BotAskingMode, PostAction
from zabia.models.zabia_conversation import ZabiaConversation


def add_reminder_summary(context, topic, message):
    texts = [
        message
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.REMINDER_SUMMARY.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "Yes"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_SUMMARY.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SCHEDULE_REMINDER.value,
                                                       context=context,
                                                       topic=topic
                                                       )

    texts = [
        "Cancel"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.TRACKER_SUMMARY.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.RETURN_TO_MENU.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload=context
                                                       )


def add_reminder_until_flow(context, topic, message):
    texts = [
        message
    ]
    instance2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.REMINDER_UNTIL.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                     context=context,
                                                     topic=topic
                                                     )

    # No Repeat, Daily, Every 1 hour, Monthly
    texts = [
        "1 Week"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_UNTIL.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance2.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="1_WEEK"
                                                       )
    texts = [
        "1 Month"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_UNTIL.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance2.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="1_MONTH"
                                                       )

    texts = [
        "3 Month"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_UNTIL.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                       parent_id=instance2.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="3_MONTH"
                                                       )


def add_reminder_frequency_flow(context, topic, message):
    texts = [
        message
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.REMINDER_OCCURANCE.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "Once a Day"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_OCCURANCE.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="ONCE_A_DAY"
                                                       )

    texts = [
        "Once a Week"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_OCCURANCE.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="ONCE_A_WEEK"
                                                       )
    texts = [
        "Every Other Day"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_OCCURANCE.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="EVERY_OTHER_DAY"
                                                       )


def add_reminder_time_flow(context, topic, message, time_question=None):

    convert_number_two_digit = lambda x: '0%s' % x if x >= 0 and x < 10 else '%s' % x

    texts = [
        message
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.REMINDER_TIME.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "08 AM"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_TIME.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_REMINDER_TIME.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="08:00"
                                                       )
    texts = [
        "12 AM"
    ]
    instance1_2= ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_TIME.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="12:00"
                                                       )
    texts = [
        "06 PM"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_TIME.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="18:00"
                                                       )
    texts = [
        "09 PM"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_TIME.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=4,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="21:00"
                                                       )
    texts = [
        "Other Time"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_TIME.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=5,
                                                       parent_id=instance1.pk,
                                                       context=context,
                                                       topic=topic
                                                       )
    texts = [
        time_question or "Please enter your time. e.g 12pm"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.REMINDER_TIME.value,
                                                       presentation=Presentation.TEXT.value,
                                                       bot_asking_mode=BotAskingMode.ASK.value, order=1,
                                                       parent_id=instance1_1.pk,
                                                       post_action=PostAction.SAVE_REMINDER.value,
                                                       context=context,
                                                       topic=topic
                                                       )


def add_reminder_question(text, context, topic):
    texts = [
        text
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.REMINDER_INITIAL.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value,
                                                     order=1,
                                                     context=context,
                                                     topic=topic)
    texts = [
        "Yes"
    ]
    instance1_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.REMINDER_INITIAL.value,
                                                  presentation=Presentation.REPLY.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                  parent_id=instance1.pk,
                                                  post_action=PostAction.RUN_REMINDER.value,
                                                  context=context,
                                                  topic=topic
                                                  )
    texts = [
        "No"
    ]
    instance1_2 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.REMINDER_INITIAL.value,
                                                  presentation=Presentation.REPLY.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                  parent_id=instance1.pk,
                                                  post_action=PostAction.RETURN_TO_MENU.value,
                                                  payload=context
                                                  )
    texts = [
        "Cancel"
    ]
    instance1_3 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.ZABIA_FLOW_ITEM.value,
                                                  presentation=Presentation.REPLY.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                  parent_id=instance1.pk,
                                                  post_action=PostAction.RETURN_TO_MAIN_MENU.value
                                                  )


def setup_reminder(context, topic, initial_question, time_message, time_question, frequency_message, until_message, summary_message):
    add_reminder_question(initial_question, context, topic)
    add_reminder_time_flow(context, topic, time_message, time_question=time_question)
    add_reminder_frequency_flow(context, topic, frequency_message)
    add_reminder_until_flow(context, topic, until_message)
    add_reminder_summary(context, topic, summary_message)
