from zabia.constants.enums import Context, REMINDER_TOPIC
from zabia.management.utils.reminder_questions import setup_reminder


def setup_tips_reminder():
    context = Context.COMER_SANO.value
    topic = REMINDER_TOPIC.TIPS.value
    initial_question = "Do you want to subscribe?"
    time_message = "At what time you want to get your tips"
    time_question = "Please enter your convenient time to get tips.(e.g 08:30 PM)"
    frequency_message = "How frequent do you want to schedule it?"
    until_message = "For how long do you want to schedule it?"
    summary_message = "You will get tips on the following schedule: "

    setup_reminder(context, topic, initial_question, time_message, time_question, frequency_message, until_message, summary_message)