from zabia.constants.enums import ConversationTag, Presentation, BotAskingMode, PostAction, Context, TrackerTopic
from zabia.management.utils.tracker_questions import add_mood_tracker_questions
from zabia.management.utils.utility import add_partner_email_question, add_tracker_summary, \
    add_tracker_modify_options, add_scheduler_time_flow, add_scheduler_occurance, add_schedule_until_flow, \
    change_delete_partner_email
from zabia.models.zabia_conversation import ZabiaConversation


def build_tracker_mood():
    context = Context.SENTIRSE_BIEN.value
    topic = TrackerTopic.MOOD.value

    texts = [
        "Do you want to track your mood? (Follow-up is recommended to understand the cause of your mood)"
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.TRACKER_INITIAL.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value,
                                                     order=1,
                                                     context=context,
                                                     topic=topic)
    texts = [
        "Yes"
    ]
    instance1_1 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.TRACKER_INITIAL.value,
                                                  presentation=Presentation.REPLY.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                  parent_id=instance1.pk,
                                                  post_action=PostAction.RUN_TRACKER.value,
                                                  context=context,
                                                  topic=topic
                                                  )
    texts = [
        "No"
    ]
    instance1_2 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.TRACKER_INITIAL.value,
                                                  presentation=Presentation.REPLY.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                  parent_id=instance1.pk,
                                                  post_action=PostAction.RETURN_TO_MENU.value,
                                                  payload=Context.SENTIRSE_BIEN.value
                                                  )
    texts = [
        "Cancel"
    ]
    instance1_3 = ZabiaConversation.add_flow_item(text_list=texts,
                                                  tag=ConversationTag.TRACKER_INITIAL.value,
                                                  presentation=Presentation.REPLY.value,
                                                  bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                  parent_id=instance1.pk,
                                                  post_action=PostAction.RETURN_TO_MAIN_MENU.value
                                                  )

    add_scheduler_time_flow(context=context,
                            topic=topic,
                            message="At what time would you like to record your mood?")

    add_scheduler_occurance(context=context, topic=topic)

    add_schedule_until_flow(context=context,
                            topic=topic)

    add_partner_email_question(context=context,
                               topic=topic,
                               text="Do you want to select a partner to be alerted if you fail to report?")

    add_tracker_summary(context=context, topic=topic)

    add_mood_tracker_questions(context=context, topic=topic)

    add_tracker_modify_options(context=context, topic=topic)

    # For modify tracker
    add_scheduler_time_flow(context=context,
                            topic=topic,
                            message="At what time would you like to record your mood?",
                            tag=ConversationTag.TRACKER_MODIFY_SCHEDULER_TIME.value)
    add_scheduler_occurance(context=context, topic=topic,
                            tag=ConversationTag.TRACKER_MODIFY_SCHEDULER_OCCURANCE.value,
                            text="How frequent do you want to track your mood?")
    add_schedule_until_flow(context=context,
                            topic=topic,
                            tag=ConversationTag.TRACKER_MODIFY_SCHEDULER_UNTIL.value,
                            text="For how long do you want to track your mood?")
    add_partner_email_question(context=context,
                               topic=topic,
                               text="Do you want to select a partner to be alerted if you fail to report?",
                               tag=ConversationTag.TRACKER_MODIFY_TRACKER_PARTNER_EMAIL.value)
    change_delete_partner_email(context=context,
                                topic=topic,
                                text="What do you want to do with your partners email address?")
