from zabia.constants.enums import ConversationTag, Presentation, BotAskingMode, UserResponseType, \
    UserResponseContentType, PostAction, PostActionIntent
from zabia.models.zabia_conversation import ZabiaConversation


def init_basic_questions():
    texts = [
        "[Name], is this your correct name?",
        "What do I call you? [Name], is it your name?"
    ]

    instance1 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.NAME.value,
                                                           presentation=Presentation.ACTIONS.value,
                                                           bot_asking_mode=BotAskingMode.OPTION.value, order=1)
    if not instance1:
        return
    texts = [
        "Yes, Correct!",
        "Yes"
    ]
    instance1_1 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.NAME.value,
                                                             presentation=Presentation.REPLY.value,
                                                             bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                             parent_id=instance1.pk,
                                                             post_action=PostAction.SAVE.value,
                                                             post_action_intent=PostActionIntent.POSITIVE,
                                                             save_param="[Name]")
    texts = [
        "Update my name"
    ]
    instance1_2 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.NAME.value,
                                                             presentation=Presentation.REPLY.value,
                                                             bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                             parent_id=instance1.pk
                                                             )

    texts = [
        "What is your name then?",
        "How do I call you then?",
        "How do I call you then, [Name]?",
        "Please enter your name"
    ]
    instance1_2_1 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.NAME.value,
                                                               presentation=Presentation.TEXT.value,
                                                               bot_asking_mode=BotAskingMode.ASK.value, order=1,
                                                               parent_id=instance1_2.pk,
                                                               user_response_type=UserResponseType.TEXT_RESPONSE.value,
                                                               user_response_content_type=UserResponseContentType.TEXT.value
                                                               )

    # texts = [
    #     "What is your name?",
    #     "How do I call you?"
    # ]
    # instance1 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.NAME.value,
    #                                                        presentation=Presentation.TEXT.value,
    #                                                        bot_asking_mode=BotAskingMode.ASK.value, order=2,
    #                                                        user_response_type=UserResponseType.TEXT_RESPONSE.value,
    #                                                        user_response_content_type=UserResponseContentType.TEXT.value
    #                                                        )

    # Question 2
    texts = [
        "[Name], I need your email address. Please enter your email address now",
        "To help you with tips and guidance from time to time, I need your email address. "
        "Please enter your email address now"
    ]
    instance2 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.EMAIL.value,
                                                           presentation=Presentation.TEXT.value,
                                                           bot_asking_mode=BotAskingMode.ASK.value, order=3,
                                                           user_response_type=UserResponseType.TEXT_RESPONSE.value,
                                                           user_response_content_type=UserResponseContentType.TEXT.value)
    if not instance2:
        return

    # Question 3
    texts = [
        "Please upload your profile picture now"
    ]
    instance3 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.PROFILE_PICTURE.value,
                                                           presentation=Presentation.TEXT.value,
                                                           bot_asking_mode=BotAskingMode.ASK.value, order=4,
                                                           user_response_type=UserResponseType.TEXT_RESPONSE.value,
                                                           user_response_content_type=UserResponseContentType.IMAGE.value)
    if not instance3:
        return

    # Question 4
    texts = [
        "How old are you?",
        "How old are you, [Name]?"
    ]
    instance4 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.AGE.value,
                                                           presentation=Presentation.TEXT.value,
                                                           bot_asking_mode=BotAskingMode.ASK.value, order=5,
                                                           user_response_type=UserResponseType.TEXT_RESPONSE.value,
                                                           user_response_content_type=UserResponseContentType.NUMBER.value)
    if not instance3:
        return

    # Question 5
    texts = [
        "[Name] seems like a [Gender] name. Is this correct?"
    ]

    instance5 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.GENDER.value,
                                                           presentation=Presentation.ACTIONS.value,
                                                           bot_asking_mode=BotAskingMode.OPTION.value, order=6
                                                           )
    if not instance5:
        return
    texts = [
        "Male"
    ]
    instance5_1 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.GENDER.value,
                                                             presentation=Presentation.REPLY.value,
                                                             bot_asking_mode=BotAskingMode.OPTION.value,
                                                             order=1,
                                                             parent_id=instance5.pk,
                                                             post_action=PostAction.SAVE.value,
                                                             post_action_intent=PostActionIntent.POSITIVE,
                                                             save_param="Male"
                                                             )
    texts = [
        "Female"
    ]
    instance5_2 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.GENDER.value,
                                                             presentation=Presentation.REPLY.value,
                                                             bot_asking_mode=BotAskingMode.OPTION.value,
                                                             order=2,
                                                             parent_id=instance5.pk,
                                                             post_action=PostAction.SAVE.value,
                                                             post_action_intent=PostActionIntent.POSITIVE,
                                                             save_param="Female"
                                                             )

    # Question 6
    texts = [
        "Enter you weight in KG. e.g 60",
        "[Name], please enter you weight in KG. e.g 53"
    ]
    instance6 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.WEIGHT.value,
                                                           presentation=Presentation.TEXT.value,
                                                           bot_asking_mode=BotAskingMode.ASK.value, order=7,
                                                           user_response_type=UserResponseType.TEXT_RESPONSE.value,
                                                           user_response_content_type=UserResponseContentType.NUMBER.value)
    if not instance6:
        return

    # Question 7
    texts = [
        "Enter you height in meter. e.g. 1.8",
        "[Name], please enter you height in meter. e.g 1.7"
    ]
    instance7 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.HEIGHT.value,
                                                           presentation=Presentation.TEXT.value,
                                                           bot_asking_mode=BotAskingMode.ASK.value, order=8,
                                                           user_response_type=UserResponseType.TEXT_RESPONSE.value,
                                                           user_response_content_type=UserResponseContentType.NUMBER.value)
    if not instance7:
        return

    # Question 8
    texts = [
        "We could not find your location. To further proceed "
        "we need your location information. Where are you located now? e.g New York",
        "We could not find your location. Please enter your location name. e.g Munich"
    ]
    instance8 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.LOCATION.value,
                                                           presentation=Presentation.TEXT.value,
                                                           bot_asking_mode=BotAskingMode.ASK.value, order=9,
                                                           user_response_type=UserResponseType.TEXT_RESPONSE.value,
                                                           user_response_content_type=UserResponseContentType.TEXT.value)
    if not instance8:
        return

    texts = [
        "We have found the following information for your location and timezone. Please confirm"
    ]

    instance9 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.LOCATION_ACTION.value,
                                                           presentation=Presentation.ACTIONS.value,
                                                           bot_asking_mode=BotAskingMode.OPTION.value, order=1)
    if not instance9:
        return
    texts = [
        "Confirm"
    ]
    instance9_1 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.LOCATION_ACTION.value,
                                                             presentation=Presentation.REPLY.value,
                                                             bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                             parent_id=instance9.pk,
                                                             post_action=PostAction.LOCATION_ACTION_CONFIRM.value)
    texts = [
        "Change"
    ]
    instance9_2 = ZabiaConversation.add_zabia_basic_question(text_list=texts, tag=ConversationTag.LOCATION_ACTION.value,
                                                             presentation=Presentation.REPLY.value,
                                                             bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                             parent_id=instance9.pk,
                                                             post_action=PostAction.LOCATION_ACTION_CHANGE.value)
