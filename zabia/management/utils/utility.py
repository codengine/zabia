from zabia.constants.enums import ConversationTag, Presentation, BotAskingMode, PostAction
from zabia.models.zabia_conversation import ZabiaConversation


def add_scheduler_time_flow(context, topic, message, tag=ConversationTag.SCHEDULER_TIME.value):

    convert_number_two_digit = lambda x: '0%s' % x if x >= 0 and x < 10 else '%s' % x

    texts = [
        message
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=tag,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "08 AM"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="08:00"
                                                       )
    texts = [
        "12 AM"
    ]
    instance1_2= ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="12:00"
                                                       )
    texts = [
        "06 PM"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="18:00"
                                                       )
    texts = [
        "09 PM"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=4,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="21:00"
                                                       )
    texts = [
        "Other Time"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=5,
                                                       parent_id=instance1.pk,
                                                       context=context,
                                                       topic=topic
                                                       )
    texts = [
        "Please enter your time you want the tracker to schedule this activity. e.g 12 pm"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.TEXT.value,
                                                       bot_asking_mode=BotAskingMode.ASK.value, order=1,
                                                       parent_id=instance1_1.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic
                                                       )


def add_once_a_week_option(context, topic, day_name, parent_instance, order,
                           post_action_intent="ONCE_A_WEEK",
                           tag=ConversationTag.SCHEDULER_OCCURANCE.value):
    texts = [
        day_name
    ]
    instance = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                    tag=tag,
                                                    presentation=Presentation.REPLY.value,
                                                    bot_asking_mode=BotAskingMode.OPTION.value, order=order,
                                                    parent_id=parent_instance.pk,
                                                    post_action=PostAction.SAVE_SCHEDULER.value,
                                                    post_action_intent=post_action_intent,
                                                    context=context,
                                                    topic=topic,
                                                    payload=day_name
                                                    )


def add_scheduler_occurance(context, topic, tag=ConversationTag.SCHEDULER_OCCURANCE.value,
                            text="How frequent do you want to track it?"):
    texts = [
        text
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=tag,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "Once a Day"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="ONCE_A_DAY"
                                                       )

    texts = [
        "Once a Week"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance1.pk,
                                                       context=context,
                                                       topic=topic,
                                                       payload="ONCE_A_WEEK"
                                                       )
    texts = [
        "Which day of the week you want to track it?"
    ]
    instance1_3_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                         tag=tag,
                                                         presentation=Presentation.ACTIONS.value,
                                                         bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                         parent_id=instance1_3.pk,
                                                         context=context,
                                                         topic=topic
                                                         )
    add_once_a_week_option(context, topic, "SATURDAY", instance1_3_1, 1, tag=tag)
    add_once_a_week_option(context, topic, "SUNDAY", instance1_3_1, 2, tag=tag)
    add_once_a_week_option(context, topic, "MONDAY", instance1_3_1, 3, tag=tag)
    add_once_a_week_option(context, topic, "TUESDAY", instance1_3_1, 4, tag=tag)
    add_once_a_week_option(context, topic, "WEDNESDAY", instance1_3_1, 5, tag=tag)
    add_once_a_week_option(context, topic, "THURSDAY", instance1_3_1, 6, tag=tag)
    add_once_a_week_option(context, topic, "FRIDAY", instance1_3_1, 7, tag=tag)

    texts = [
        "Every Other Day"
    ]
    instance1_4 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="EVERY_OTHER_DAY"
                                                       )
    texts = [
        "Which day you don't want to track it?"
    ]
    instance1_4_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                         tag=tag,
                                                         presentation=Presentation.ACTIONS.value,
                                                         bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                         parent_id=instance1_4.pk,
                                                         context=context,
                                                         topic=topic
                                                         )
    add_once_a_week_option(context, topic, "SATURDAY", instance1_4_1, 1, post_action_intent="EVERY_OTHER_DAY", tag=tag)
    add_once_a_week_option(context, topic, "SUNDAY", instance1_4_1, 2, post_action_intent="EVERY_OTHER_DAY", tag=tag)
    add_once_a_week_option(context, topic, "MONDAY", instance1_4_1, 3, post_action_intent="EVERY_OTHER_DAY", tag=tag)
    add_once_a_week_option(context, topic, "TUESDAY", instance1_4_1, 4, post_action_intent="EVERY_OTHER_DAY", tag=tag)
    add_once_a_week_option(context, topic, "WEDNESDAY", instance1_4_1, 5, post_action_intent="EVERY_OTHER_DAY", tag=tag)
    add_once_a_week_option(context, topic, "THURSDAY", instance1_4_1, 6, post_action_intent="EVERY_OTHER_DAY", tag=tag)
    add_once_a_week_option(context, topic, "FRIDAY", instance1_4_1, 7, post_action_intent="EVERY_OTHER_DAY", tag=tag)


def add_schedule_until_flow(context, topic, tag=ConversationTag.SCHEDULER_UNTIL.value,
                            text="For how long do you want to track it?"):
    texts = [
        text
    ]
    instance2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=tag,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                     context=context,
                                                     topic=topic
                                                     )

    # No Repeat, Daily, Every 1 hour, Monthly
    texts = [
        "1 Week"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance2.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="1_WEEK"
                                                       )
    texts = [
        "1 Month"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance2.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="1_MONTH"
                                                       )

    texts = [
        "3 Month"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                       parent_id=instance2.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="3_MONTH"
                                                       )
    texts = [
        "CONTINUOUSLY"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=4,
                                                       parent_id=instance2.pk,
                                                       post_action=PostAction.SAVE_SCHEDULER.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload="CONTINUOUSLY"
                                                       )


def change_delete_partner_email(context, topic, text,
                                tag=ConversationTag.TRACKER_MODIFY_TRACKER_PARTNER_EMAIL_CHANGE_DELETE.value):
    texts = [
        text
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=tag,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )
    texts = [
        "Change"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       context=context,
                                                       topic=topic
                                                       )
    texts = [
        "Please enter your partner's email address"
    ]
    instance1_1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                         tag=tag,
                                                         presentation=Presentation.TEXT.value,
                                                         bot_asking_mode=BotAskingMode.ASK.value, order=1,
                                                         parent_id=instance1_1.pk,
                                                         context=context,
                                                         topic=topic
                                                         )
    texts = [
        "DELETE"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       context=context,
                                                       topic=topic,
                                                       post_action=PostAction.TRACKER_MODIFY_PARTNERS_EMAIL_DELETE.value
                                                       )


def add_partner_email_question(context, topic, text,
                               tag=ConversationTag.TRACKER_PARTNER_EMAIL.value):
    texts = [
        text
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=tag,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "Yes Please"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       context=context,
                                                       topic=topic
                                                       )

    texts = [
        "Please enter your partner's email address"
    ]
    instance1_1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                         tag=tag,
                                                         presentation=Presentation.TEXT.value,
                                                         bot_asking_mode=BotAskingMode.ASK.value, order=1,
                                                         parent_id=instance1_1.pk,
                                                         context=context,
                                                         topic=topic,
                                                         post_action=PostAction.TRACKER_SUMMARY.value
                                                         )

    texts = [
        "No Need"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=tag,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance1.pk,
                                                       context=context,
                                                       topic=topic,
                                                       post_action=PostAction.TRACKER_SUMMARY.value
                                                       )


def add_tracker_modify_options(context, topic,
                               text="What do you want to change?",
                               tag=ConversationTag.MODIFY_TRACKER.value):
    texts = [
        text
    ]
    parent_instance = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                           tag=tag,
                                                           presentation=Presentation.ACTIONS.value,
                                                           bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                           context=context,
                                                           topic=topic
                                                           )
    tracker_options = [
        "schedule_time",
        "occurance",
        "until",
        "partner_email"
    ]

    for tracker_option in tracker_options:
        texts = [
            " ".join(option.lower().capitalize() for option in tracker_option.split("_"))
        ]
        instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                           tag=tag,
                                                           presentation=Presentation.REPLY.value,
                                                           bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                           parent_id=parent_instance.pk,
                                                           # post_action=PostAction.SAVE_SCHEDULER.value,
                                                           context=context,
                                                           topic=topic,
                                                           payload=tracker_option.upper()
                                                           )


def add_tracker_summary(context, topic):
    texts = [
        "The tracker with the following information will be created:"
    ]
    instance1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                     tag=ConversationTag.TRACKER_SUMMARY.value,
                                                     presentation=Presentation.ACTIONS.value,
                                                     bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                     context=context,
                                                     topic=topic
                                                     )

    texts = [
        "Confirm"
    ]
    instance1_1 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.TRACKER_SUMMARY.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.SCHEDULE_TRACKER.value,
                                                       context=context,
                                                       topic=topic
                                                       )

    texts = [
        "Change"
    ]
    instance1_2 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.MODIFY_TRACKER.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=2,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.LOAD_MODIFY_TRACKER_OPTIONS.value,
                                                       context=context,
                                                       topic=topic
                                                       )
    texts = [
        "Cancel"
    ]
    instance1_3 = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                       tag=ConversationTag.CANCEL_TRACKER.value,
                                                       presentation=Presentation.REPLY.value,
                                                       bot_asking_mode=BotAskingMode.OPTION.value, order=3,
                                                       parent_id=instance1.pk,
                                                       post_action=PostAction.RETURN_TO_MENU.value,
                                                       context=context,
                                                       topic=topic,
                                                       payload=context
                                                       )
