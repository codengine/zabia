from zabia.constants.enums import ConversationTag, Presentation, BotAskingMode, PostAction, PostActionIntent, Context, \
    TrackerTopic, UserResponseType, UserResponseContentType
from zabia.models.zabia_conversation import ZabiaConversation


def add_chart_item(measurement_name, parent_object, context, topic):
    texts = [
        "Ok, I will remember you measurement for %s on [Date]" % measurement_name
    ]
    activity_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                  tag=ConversationTag.TRACKER_QUESTION.value,
                                                                  presentation=Presentation.TEXT.value,
                                                                  bot_asking_mode=BotAskingMode.SAY.value,
                                                                  order=1,
                                                                  parent_id=parent_object.pk,
                                                                  post_action=PostAction.SHOW_MEASUREMENT_CHART.value,
                                                                  context=context,
                                                                  topic=topic
                                                                  )
    return activity_object
    

def add_tracker_question(measurement_name, context, topic, image, image_type): # measurement_name = weight, water intake etc
    texts = [
        "You need to enter your measurement for your %s tracker" % measurement_name
    ]
    main_tracker_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                      tag=ConversationTag.TRACKER_QUESTION.value,
                                                                      presentation=Presentation.CAROUSEL.value,
                                                                      bot_asking_mode=BotAskingMode.OPTION.value,
                                                                      order=1, image=image,
                                                                      image_type=image_type,
                                                                      context=context,
                                                                      topic=topic)
                                                                      
    texts = [
        "Please enter your measurement now(e.g 72.5 KG)"
    ]
    tracker_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                 tag=ConversationTag.TRACKER_QUESTION.value,
                                                                 presentation=Presentation.TEXT.value,
                                                                 bot_asking_mode=BotAskingMode.ASK.value,
                                                                 parent_id=main_tracker_object.pk,
                                                                 context=context,
                                                                 topic=topic,
                                                                 post_action=PostAction.SAVE_TRACKER_ANSWER.value,
                                                                 payload=measurement_name)
    add_chart_item(measurement_name, tracker_object)
