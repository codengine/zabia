from zabia.models.zabia_conversation import ZabiaConversation


def zabia_introduction():
    text = [
        'Thank you so much!',
        'Hi! Thanks very much!'
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=1)

    text = [
        'Let me introduce myself'
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=2)

    text = [
        'I am Zabia',
        'My Name is Zabia'
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=3)

    text = [
        'I am a chatbot'
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=4)

    text = [
        'I will be your guide from now on',
        'I will be your guide from time to time'
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=5)

    text = [
        'You can ask me questions, subscribe me for tips, foods, restaurants and many other services.',
        'I can provide you tips, guidance, foods and restaurants recommendations and many other services'
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=6)

    text = [
        'Before we move on, I need to know about you a bit. Kindly help me to know your basic information',
        'Before we move on, let me help to know you a bit more'
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=7)

    text = [
        "One more thing: Don’t use me in medical emergencies. I don’t "
        "provide medical advice, and I don’t support emergency calls"
    ]
    ZabiaConversation.add_zabia_self_introduction(text_list=text, order=8)
