from zabia.constants.enums import ConversationTag, Presentation, BotAskingMode, PostAction, PostActionIntent, Context, \
    TrackerTopic, UserResponseType, UserResponseContentType
from zabia.models.zabia_conversation import ZabiaConversation


def create_tracker_mood_option_item(mood, tracker_object, order, post_action_message=None,
                                    context=Context.SENTIRSE_BIEN.value,
                                    topic=TrackerTopic.MOOD.value):
    texts = [
        mood
    ]
    tracker_mood = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                               tag=ConversationTag.TRACKER_QUESTION.value,
                                                               presentation=Presentation.REPLY.value,
                                                               bot_asking_mode=BotAskingMode.OPTION.value,
                                                               order=order,
                                                               parent_id=tracker_object.pk,
                                                               post_action=PostAction.SAVE_TRACKER_ANSWER.value,
                                                               context=context,
                                                               topic=topic,
                                                               payload=mood
                                                               )
    return tracker_mood


def add_tracker_say_item(text_list, parent_object, context, topic):
    texts = text_list
    say_item = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                           tag=ConversationTag.TRACKER_QUESTION.value,
                                                           presentation=Presentation.TEXT.value,
                                                           bot_asking_mode=BotAskingMode.SAY.value, order=1,
                                                           parent_id=parent_object.pk,
                                                           context=context,
                                                           topic=topic
                                                           )
    return say_item


def add_activity_item(text, parent_object, order, payload, context=Context.SENTIRSE_BIEN.value,
                      topic=TrackerTopic.MOOD.value,
                      post_action=PostAction.SAVE_TRACKER_MOOD_ACTIVITY_ANSWER.value):
    texts = [
        text
    ]
    activity_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                  tag=ConversationTag.TRACKER_QUESTION.value,
                                                                  presentation=Presentation.REPLY.value,
                                                                  bot_asking_mode=BotAskingMode.OPTION.value,
                                                                  order=order,
                                                                  parent_id=parent_object.pk,
                                                                  post_action=post_action,
                                                                  context=context,
                                                                  topic=topic,
                                                                  payload=payload
                                                                  )
    return activity_object


def define_last_tracker_option_item(text, parent_object, order, payload, post_action, context, topic):
    texts = [
        text
    ]
    _instance = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                            tag=ConversationTag.LAST_TRACKER_OPTION.value,
                                                            presentation=Presentation.REPLY.value,
                                                            bot_asking_mode=BotAskingMode.OPTION.value,
                                                            order=order,
                                                            parent_id=parent_object.pk,
                                                            post_action=post_action,
                                                            context=context,
                                                            topic=topic,
                                                            payload=payload
                                                            )
    return _instance


def define_tracker_delete_option_item(text, parent_object, order, payload, post_action, context, topic):
    texts = [
        text
    ]
    _instance = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                            tag=ConversationTag.TRACKER_DELETE_OPTION.value,
                                                            presentation=Presentation.REPLY.value,
                                                            bot_asking_mode=BotAskingMode.OPTION.value,
                                                            order=order,
                                                            parent_id=parent_object.pk,
                                                            post_action=post_action,
                                                            context=context,
                                                            topic=topic,
                                                            payload=payload
                                                            )
    return _instance


def define_tracker_last_options(context, topic, exit_message):
    texts = [
        "How can I help you?"
    ]
    last_tracker_options_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                              tag=ConversationTag.LAST_TRACKER_OPTION.value,
                                                                              presentation=Presentation.ACTIONS.value,
                                                                              bot_asking_mode=BotAskingMode.OPTION.value,
                                                                              context=context,
                                                                              topic=topic)
    last_tracker_option_item1 = define_last_tracker_option_item(text="Subscribing biweekly content",
                                                                parent_object=last_tracker_options_object,
                                                                order=1,
                                                                payload=None,
                                                                post_action=PostAction.SUBSCRIBE_BI_WEEKLY_CONTENT.value,
                                                                context=context,
                                                                topic=topic)
    last_tracker_option_item2 = define_last_tracker_option_item(text="Continue",
                                                                parent_object=last_tracker_options_object,
                                                                order=2,
                                                                payload=None,
                                                                post_action=PostAction.CONTINUE.value,
                                                                context=context,
                                                                topic=topic)
    last_tracker_option_item3 = define_last_tracker_option_item(text="EXIT",
                                                                parent_object=last_tracker_options_object,
                                                                order=3,
                                                                payload=None,
                                                                post_action=PostAction.EXIT.value,
                                                                context=context,
                                                                topic=topic)
    texts = [
        exit_message
    ]
    exit_message_instance = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                        tag=ConversationTag.LAST_TRACKER_OPTION.value,
                                                                        presentation=Presentation.TEXT.value,
                                                                        bot_asking_mode=BotAskingMode.SAY.value,
                                                                        order=1,
                                                                        parent_id=last_tracker_option_item3.pk,
                                                                        context=context,
                                                                        topic=topic
                                                                        )


def tracker_delete_question(context, topic, delete_question):
    texts = [
        delete_question
    ]
    tracker_delete_question_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                                 tag=ConversationTag.DELETE_TRACKER_QUESTION.value,
                                                                                 presentation=Presentation.ACTIONS.value,
                                                                                 bot_asking_mode=BotAskingMode.OPTION.value,
                                                                                 context=context,
                                                                                 topic=topic)
    texts = [
        "Change"
    ]
    tracker_modify_option = ZabiaConversation.add_scheduler_item(text_list=texts,
                                                                 tag=ConversationTag.MODIFY_TRACKER.value,
                                                                 presentation=Presentation.REPLY.value,
                                                                 bot_asking_mode=BotAskingMode.OPTION.value, order=1,
                                                                 parent_id=tracker_delete_question_object.pk,
                                                                 post_action=PostAction.LOAD_MODIFY_TRACKER_OPTIONS.value,
                                                                 context=context,
                                                                 topic=topic
                                                                 )
    tracker_delete_option1 = define_tracker_delete_option_item(text="Delete",
                                                               parent_object=tracker_delete_question_object,
                                                               order=2,
                                                               payload=None,
                                                               post_action=PostAction.DELETE_TRACKER.value,
                                                               context=context,
                                                               topic=topic)
    tracker_delete_option2 = define_tracker_delete_option_item(text="Ignore",
                                                               parent_object=tracker_delete_question_object,
                                                               order=3,
                                                               payload=None,
                                                               post_action=PostAction.TRACKER_DELETE_IGNORE.value,
                                                               context=context,
                                                               topic=topic)


def add_chart_item(parent_object, context, topic):
    texts = [
        "Ok, I will remember your mood on [Date]"
    ]
    say_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                             tag=ConversationTag.TRACKER_INPUT_ACKNOWLEDGEMENT.value,
                                                             presentation=Presentation.TEXT.value,
                                                             bot_asking_mode=BotAskingMode.SAY.value,
                                                             order=1,
                                                             parent_id=parent_object.pk,
                                                             context=context,
                                                             topic=topic
                                                             )
    chart_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                               tag=ConversationTag.SHOW_MOOD_CHART.value,
                                                               presentation=Presentation.CAROUSEL.value,
                                                               bot_asking_mode=BotAskingMode.SAY.value,
                                                               order=1,
                                                               parent_id=say_object.pk,
                                                               context=context,
                                                               topic=topic
                                                               )
    return chart_object


def add_activity_question(question_text, parent_object, context, topic):
    texts = [
        question_text
    ]
    activity_parent_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                         tag=ConversationTag.TRACKER_QUESTION.value,
                                                                         presentation=Presentation.ACTIONS.value,
                                                                         bot_asking_mode=BotAskingMode.OPTION.value,
                                                                         parent_id=parent_object.pk,
                                                                         context=context,
                                                                         topic=topic)
    activity_object_1 = add_activity_item("Relax", activity_parent_object, 1, "Relax", context=context, topic=topic)
    add_chart_item(activity_object_1, context=context, topic=topic)

    activity_object_2 = add_activity_item("Work", activity_parent_object, 2, "Work", context=context, topic=topic)
    add_chart_item(activity_object_2, context=context, topic=topic)

    activity_object_3 = add_activity_item("Sport", activity_parent_object, 3, "Sport", context=context, topic=topic)
    add_chart_item(activity_object_3, context=context, topic=topic)

    activity_object_4 = add_activity_item("Meal", activity_parent_object, 4, "Meal", context=context, topic=topic)
    add_chart_item(activity_object_4, context=context, topic=topic)

    activity_object_5= add_activity_item("Shopping", activity_parent_object, 5, "Shopping", context=context, topic=topic)
    add_chart_item(activity_object_5, context=context, topic=topic)

    activity_object_6 = add_activity_item("None", activity_parent_object, 6, "None", context=context, topic=topic)
    add_chart_item(activity_object_6, context=context, topic=topic)

    activity_object_7 = add_activity_item("Cancel", activity_parent_object, 7, payload="Cancel", context=context, topic=topic,
                                          post_action=PostAction.RETURN_TO_MENU.value)
    add_chart_item(activity_object_7, context=context, topic=topic)
    return activity_parent_object


def add_mood_tracker_questions(context, topic):
    texts = [
        "You need to enter your mood for your mood tracker"
    ]
    main_tracker_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                      tag=ConversationTag.TRACKER_QUESTION.value,
                                                                      presentation=Presentation.CAROUSEL.value,
                                                                      bot_asking_mode=BotAskingMode.OPTION.value,
                                                                      order=1, image='https://www.southtech.pe/zabiatest/imagen/icono/sentirsebien.png',
                                                                      image_type='image/png',
                                                                      context=context,
                                                                      topic=topic)

    texts = [
        "How are you feeling right now?",
        "How are you feeling right now, [Name]?"
    ]
    tracker_object = ZabiaConversation.add_tracker_question_item(text_list=texts,
                                                                 tag=ConversationTag.TRACKER_QUESTION.value,
                                                                 presentation=Presentation.ACTIONS.value,
                                                                 bot_asking_mode=BotAskingMode.OPTION.value,
                                                                 parent_id=main_tracker_object.pk,
                                                                 context=context,
                                                                 topic=topic)

    tracker_mood_1 = create_tracker_mood_option_item(mood="Happy", tracker_object=tracker_object, order=1, context=context, topic=topic)
    text_list = ["Nice!", "Great!", "Great to hear!", "Great, [Name]!", "Great to hear, [Name]!"]
    say_item_1 = add_tracker_say_item(text_list, tracker_mood_1, context=context, topic=topic)
    add_activity_question("What are you doing?", say_item_1, context=context, topic=topic)

    tracker_mood_2 = create_tracker_mood_option_item(mood="Sad", tracker_object=tracker_object, order=2, context=context, topic=topic)
    text_list = ["No Worries, It happens sometimes!", "Got it", "Got it, [Name]", "So sorry to hear",
                 "If you have a lot of stress in your life, find ways to reduce it"]
    say_item_2 = add_tracker_say_item(text_list, tracker_mood_2, context=context, topic=topic)
    add_activity_question("What did you do?", say_item_2, context=context, topic=topic)

    tracker_mood_3 = create_tracker_mood_option_item(mood="Fearfully", tracker_object=tracker_object, order=3, context=context, topic=topic)
    text_list = ["Got it", "You can't be happy all the time"]
    say_item_3 = add_tracker_say_item(text_list, tracker_mood_3, context=context, topic=topic)
    add_activity_question("What did you do?", say_item_3, context=context, topic=topic)

    tracker_mood_4 = create_tracker_mood_option_item(mood="Upset", tracker_object=tracker_object, order=4, context=context, topic=topic)
    text_list = ["Got it", "Sorry to hear, [Name]"]
    say_item_4 = add_tracker_say_item(text_list, tracker_mood_4, context=context, topic=topic)
    add_activity_question("What did you do?", say_item_4, context=context, topic=topic)

    tracker_mood_5 = create_tracker_mood_option_item(mood="Normal", tracker_object=tracker_object, order=5, context=context, topic=topic)
    text_list = ["Sounds good!"]
    say_item_5 = add_tracker_say_item(text_list, tracker_mood_5, context=context, topic=topic)
    add_activity_question("What were you doing?", say_item_5, context=context, topic=topic)

    tracker_delete_question(context=context,
                            topic=topic,
                            delete_question="You have a mood tracker enabled. Do you want to delete your tracker?")


