from zabia.chatflow.router import ChatRouter
from zabia.constants.enums import ConversationLevelEnum
from zabia.models.zabia_user import ZUser


class NewUserHandlerMixin(object):
    def handle_new_user(self, app_id, app_user_id, json_body):
        z_user = ZUser.add_new_user(app_id=app_id, app_user_id=app_user_id)
        if z_user:
            ChatRouter.save_current_level(app_id=app_id, app_user_id=app_user_id, level=ConversationLevelEnum.PROFILE_CREATED_BUT_NOT_AGREED_TERMS_AND_CONDITIONS.value)
        return self.send_first_hello_message(app_id=app_id, app_user_id=app_user_id, json_body=json_body)