import smooch


class FirstHelloMessageMixin(object):
    def build_action(self, text, type, payload, uri=None):
        action = smooch.Action(type=type, text=text, payload=payload, uri=uri)
        return action

    def send_first_hello_message(self, app_id, app_user_id, json_body, skip_message=False,
                                 skip_typing_activity_trigger=False):
        if not skip_typing_activity_trigger:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

        if skip_message:
            actions = []
            action = smooch.Action(type='postback', text="I Agree with That", payload="AGREED_TERM_AND_POLICY")
            actions += [action]
            msg_text = "Sorry, without your consent, I'm afraid we cannot continue! "
            msg_text += "In case you want to agree click the button below."
            message_post = smooch.MessagePost(text=msg_text, role='appMaker', type='text', actions=actions)

            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post)

        else:
            items = []
            actions = []
            action = self.build_action(text='See terms of use', uri='https://www.championtutor.com/online/faq/',
                                       type='link', payload='TERMS_OF_USE')
            actions += [action]
            action = self.build_action(text='See privacy policy', uri='https://www.championtutor.com/online/faq/',
                                       type='link', payload='PRIVACY_POLICY')
            actions += [action]
            action = self.build_action(text='I am okay with that', type='postback', payload='AGREED_TERM_AND_POLICY')
            actions += [action]

            title = 'Hi! This is Zabia \n\n\n\n'
            media_url = 'https://www.southtech.pe/zabiatest/imagen/icono/sentirsebien.png'
            media_type = 'image/png'
            description = "I'm a health bot. See my terms and policy below. Is this okay with you?"

            message_item = smooch.MessageItem(title=title, description=description, media_url=media_url,
                                              media_type=media_type, size='large', actions=actions)

            items += [message_item]

            message_post = smooch.MessagePost(role='appMaker', type='carousel', items=items)

            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post)

        if not skip_typing_activity_trigger:
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        return api_response
