from decimal import Decimal
from random import randint
import smooch
from engine.clock.Clock import Clock
from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer
from zabia.chatflow.router import ChatRouter, QuestionRouter
from zabia.constants.enums import ConversationType, BotAskingMode, UserResponseType, UserResponseContentType, \
    ConversationTag
from zabia.handlers.api_handlers import APIHandler
from zabia.models.QStack import QStack
from zabia.models.scheduler import Scheduler
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait
from zabia.utils.location_util import LocationUtil
from zabia.utils.tzutil import TZUtil


class SmoochNoPayloadHandlerMixin(object):
    def handle_no_payload(self, app_id, app_user_id, json_body):
    
        if not ZUserInputAwait.check_if_waiting(app_id=app_id, app_user_id=app_user_id):
            QStack.clear(app_id, app_user_id)

        should_ask_health_questions = True if randint(0, 1) else False

        message_type = json_body['messages'][0]['type']

        new_user = ChatRouter.detect_if_new_user(app_user_id)

        if new_user:
            # Save the new user
            z_user = ZUser.add_new_user(app_id=app_id, app_user_id=app_user_id)
            return self.send_first_hello_message(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
        elif not ChatRouter.detect_if_terms_accepted(app_id=app_id, app_user_id=app_user_id):
            return self.handle_terms_not_accepted(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
        elif ZUserInputAwait.check_if_waiting(app_id=app_id, app_user_id=app_user_id):
            print("Waiting...")
            waiting_on_postback = ZUserInputAwait.get_current_waiting_postback(app_id=app_id, app_user_id=app_user_id)

            postback_split = waiting_on_postback.split(":")
            if len(postback_split) < 2:
                raise Exception("Invalid postback format found")
            conversation_type = postback_split[0]
            conversation_object_id = int(postback_split[1])

            if conversation_type == "PROFILE_Q":
                # "PROFILE_Q:question_id:question_type:question_code:answer_type"
                question_id = postback_split[1]
                question_type = postback_split[2]
                question_code = postback_split[3]
                answer_type = postback_split[4]

                answer_text = json_body['messages'][0]['text']
                zusers = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id)
                if zusers.exists():
                    zuser = zusers.first()
                    zabia_user_id = zuser.zabia_user_id
                    status_code, response = APIHandler.save_question_response(user_id=zabia_user_id,
                                                                              question_id=question_id,
                                                                              question_code=question_code,
                                                                              answer_type=answer_type,
                                                                              answer_id=0,
                                                                              user_answer="")

                    # Now fetch the next question and send
                    next_question = QuestionRouter.get_next_question(app_id=app_id,
                                                                     app_user_id=app_user_id,
                                                                     question_type=question_type)
                    if not next_question:
                        conversation_item_id = QStack.top(app_id=app_id, app_user_id=app_user_id)
                        if not conversation_item_id:
                            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            texts = [
                                'Thank you so much for your time.'
                            ]
                            text = texts[randint(0, len(texts) - 1)]
                            message_post_body = smooch.MessagePost(
                                'appMaker', 'text', text=text)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)

                            texts = [
                                'How can I help you please',
                                'What can I do for you please'
                            ]
                            text = texts[randint(0, len(texts) - 1)]
                            message_post_body = smooch.MessagePost(
                                'appMaker', 'text', text=text)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                            return
                        else:
                            conv_object = ZabiaConversation.objects.filter(pk=int(conversation_item_id)).first()
                            required_profiles = conv_object.required_profile.split(',')
                            for rprofile in required_profiles:
                                if zuser.gender == "Male" and rprofile == "Salud femenina":
                                    continue
                                if rprofile != question_type:
                                    next_question = QuestionRouter.get_next_question(app_id=app_id,
                                                                                     app_user_id=app_user_id,
                                                                                     question_type=rprofile)
                                    if next_question:
                                        break
                    if next_question:
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        message_post_body = SmoochRenderer.render_question_item(app_id=app_id,
                                                                                app_user_id=app_user_id,
                                                                                question_item=next_question)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        return
                    else:
                        conversation_item_id = QStack.top(app_id=app_id, app_user_id=app_user_id)
                        if conversation_item_id:
                            # Show options whether the user wants to continue with the last menu or not.
                            pass
                        else:
                            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            texts = [
                                'Thank you so much for your time.'
                            ]
                            text = texts[randint(0, len(texts) - 1)]
                            message_post_body = smooch.MessagePost(
                                'appMaker', 'text', text=text)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)

                            texts = [
                                'How can I help you please',
                                'What can I do for you please'
                            ]
                            text = texts[randint(0, len(texts) - 1)]
                            message_post_body = smooch.MessagePost(
                                'appMaker', 'text', text=text)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                        return

            else:
                QStack.clear(app_id=app_id, app_user_id=app_user_id)

            conversation_object = ZabiaConversation.objects.filter(pk=conversation_object_id).first()

            send_basic_questions = True

            if conversation_type == ConversationType.BASIC_INFO.value:
                if conversation_object.tag == ConversationTag.LOCATION.value and not ZUser.is_location_confirmed(app_id=app_id, app_user_id=app_user_id):
                    print("Location")
                    if conversation_object.bot_asking_mode == BotAskingMode.ASK.value:
                        if conversation_object.user_response_type == UserResponseType.TEXT_RESPONSE.value:
                            if conversation_object.user_response_content_type == UserResponseContentType.TEXT.value:
                                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                location_name = json_body['messages'][0]['text']
                                print("Location Name: %s" % location_name.lower())
                                location_util = LocationUtil()
                                address, lat, long = location_util.find_address(address_name=location_name.lower())
                                if address:
                                    timezone = TZUtil.find_timezone(lat=lat, long=long)
                                    print(timezone)
                                    print("Saving location for later")
                                    location_saved = ZUser.update_location(app_id=app_id,
                                                                           app_user_id=app_user_id,
                                                                           name=address,
                                                                           lat=lat,
                                                                           long=long,
                                                                           timezone=timezone)
                                    if location_saved:
                                        print("Location saved")
                                        send_basic_questions = False
                                        location_confirm_conversation_objects = ZabiaConversation.objects.filter(
                                            conversation_type=ConversationType.BASIC_INFO.value,
                                            tag=ConversationTag.LOCATION_ACTION.value,
                                            parent_id__isnull=True
                                        )
                                        if location_confirm_conversation_objects.exists():
                                            location_confirm_conversation_object = location_confirm_conversation_objects.first()
                                            message_post_body = SmoochRenderer.render_location_confirm_action(
                                                app_id=app_id,
                                                app_user_id=app_user_id,
                                                conversation_item=location_confirm_conversation_object)
                                            api_response = self.api_instance.post_message(
                                                app_id, app_user_id, message_post_body)
                                        ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)
                                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

                                    else:
                                        send_basic_questions = False
                                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                                        texts = [
                                            "Something wrong happened. Please enter your location again"
                                        ]
                                        text = texts[randint(0, len(texts) - 1)]
                                        message_post_body = smooch.MessagePost(
                                            'appMaker', 'text', text=text)
                                        api_response = self.api_instance.post_message(
                                            app_id, app_user_id, message_post_body)
                                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

                                else:
                                    send_basic_questions = False
                                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                                    texts = [
                                        "We could not detect your location %s. Please enter the correct location" % location_name
                                    ]
                                    text = texts[randint(0, len(texts) - 1)]
                                    message_post_body = smooch.MessagePost(
                                        'appMaker', 'text', text=text)
                                    api_response = self.api_instance.post_message(
                                        app_id, app_user_id, message_post_body)
                                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                else:
                    if conversation_object.bot_asking_mode == BotAskingMode.ASK.value:
                        if conversation_object.user_response_type == UserResponseType.TEXT_RESPONSE.value:
                            if conversation_object.user_response_content_type == UserResponseContentType.TEXT.value:
                                if message_type == 'text':
                                    message_text = json_body['messages'][0]['text']
                                    update_status = ZUser.update_field(app_id, app_user_id, field_name=conversation_object.tag, field_value=message_text)
                                    print(conversation_object.tag)
                                    print(message_text)
                                    print(type(update_status))
                                    if type(update_status) is not bool and update_status is not None:
                                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                                        texts = [
                                            update_status
                                        ]
                                        text = texts[randint(0, len(texts) - 1)]
                                        message_post_body = smooch.MessagePost(
                                            'appMaker', 'text', text=text)
                                        api_response = self.api_instance.post_message(
                                            app_id, app_user_id, message_post_body)
                                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

                                        send_basic_questions = False

                                    if type(update_status) is bool:
                                        ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)

                                else:
                                    pass
                            elif conversation_object.user_response_content_type == UserResponseContentType.IMAGE.value:
                                if message_type == 'image':
                                    image_url = json_body['messages'][0]['mediaUrl']
                                    ZUser.update_image_url(app_id, app_user_id, image_url=image_url)

                                    ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)
                            elif conversation_object.user_response_content_type == UserResponseContentType.NUMBER.value:
                                if message_type == 'text':
                                    message_text = json_body['messages'][0]['text']

                                    try:
                                        message_text = Decimal(message_text)

                                        ZUser.update_field(app_id, app_user_id, field_name=conversation_object.tag,
                                                           field_value=message_text)
                                        ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)
                                    except Exception as exp:
                                        print(str(exp))
                                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                                        texts = [
                                            'Hi %s, Please enter a numeric value. e.g 30' % name,
                                            'Hey %s, please enter a numeric value. e.g 23' % name
                                        ]
                                        text = texts[randint(0, len(texts) - 1)]
                                        message_post_body = smooch.MessagePost(
                                            'appMaker', 'text', text=text)
                                        api_response = self.api_instance.post_message(
                                            app_id, app_user_id, message_post_body)
                                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

                                        send_basic_questions = False

                                else:
                                    pass

                if send_basic_questions:
                    self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

            elif conversation_type == ConversationType.SCHEDULER_Q.value:
                print("No Payload Scheduler Q")
                print(conversation_object_id)

                def is_valid_email(email):
                    import re
                    match = re.search(r'[\w.-]+@[\w.-]+.\w+', email)
                    if match:
                        return True
                    return False

                conversation_object = ZabiaConversation.objects.filter(pk=conversation_object_id).first()
                if conversation_object:
                    if conversation_object.tag.endswith(ConversationTag.TRACKER_PARTNER_EMAIL.value):
                        message_text = json_body['messages'][0]['text']
                        is_valid = is_valid_email(message_text)
                        print(is_valid)
                        if is_valid:
                            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id,
                                                   context=conversation_object.context,
                                                   topic=conversation_object.topic,
                                                   field_name=ConversationTag.TRACKER_PARTNER_EMAIL.value,
                                                   field_value=message_text)
                            ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)
                            # Saved Now fetch the summary and send to the user
                            tracker_summary_object = Scheduler.get_tracker_summary_question(app_id=app_id,
                                                                                            app_user_id=app_user_id,
                                                                                            context=conversation_object.context,
                                                                                            topic=conversation_object.topic)
                            tracker_summary = Scheduler.get_tracker_summary(app_id=app_id,
                                                                            app_user_id=app_user_id,
                                                                            context=conversation_object.context,
                                                                            topic=conversation_object.topic)
                            message_post_body = SmoochRenderer.render_tracker_summary_item(app_id, app_user_id,
                                                                                           tracker_summary_object,
                                                                                           tracker_summary,
                                                                                           smooch_context=json_body)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        else:
                            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            texts = [
                                'Please enter a valid email address'
                            ]
                            text = texts[randint(0, len(texts) - 1)]
                            message_post_body = smooch.MessagePost(
                                'appMaker', 'text', text=text)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    elif conversation_object.tag == ConversationTag.SCHEDULER_TIME.value:
                        time_str = json_body['messages'][0]['text']
                        valid_time = Clock.is_valid_time(timestr=time_str)
                        if valid_time:
                            Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conversation_object.context,
                                                   topic=conversation_object.topic, field_name=conversation_object.tag,
                                                   field_value=valid_time)
                            if Scheduler.check_if_tracker_information_missing(app_id=app_id, app_user_id=app_user_id,
                                                                              context=conversation_object.context,
                                                                              topic=conversation_object.topic):
                                next_tracker_question = Scheduler.get_next_tracker_question(app_id=app_id,
                                                                                            app_user_id=app_user_id,
                                                                                            context=conversation_object.context,
                                                                                            topic=conversation_object.topic)
                                if not next_tracker_question:
                                    partner_email_question = Scheduler.get_partner_email_question(app_id=app_id,
                                                                                                  app_user_id=app_user_id,
                                                                                                  context=conversation_object.context,
                                                                                                  topic=conversation_object.topic)
                                    if partner_email_question:
                                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                        message_post_body = SmoochRenderer.render_smooch_content(
                                            partner_email_question,
                                            json_body,
                                            app_id=app_id,
                                            app_user_id=app_user_id)
                                        api_response = self.api_instance.post_message(
                                            app_id, app_user_id, message_post_body)
                                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                else:
                                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                    message_post_body = SmoochRenderer.render_smooch_content(
                                        next_tracker_question,
                                        json_body,
                                        app_id=app_id,
                                        app_user_id=app_user_id)
                                    api_response = self.api_instance.post_message(
                                        app_id, app_user_id, message_post_body)
                                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        else:
                            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            texts = [
                                'The time format you entered is invalid. Please enter a valid time. '
                                'e.g 12:00 am, or 23 or 23:30 or 10:20 pm etc'
                            ]
                            text = texts[randint(0, len(texts) - 1)]
                            message_post_body = smooch.MessagePost(
                                'appMaker', 'text', text=text)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif ZUser.check_if_basic_info_missing(app_id=app_id, app_user_id=app_user_id):
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
            texts = [
                'Hi %s, before we move on please complete your profile with some basic information.' % name,
                'Hey, before we move on please complete your profile with some basic information.'
            ]
            text = texts[randint(0, len(texts) - 1)]
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=text)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

        # elif ZUser.check_if_location_missing(app_id=app_id, app_user_id=app_user_id):
        #     # self.send_location_missing(app_id=app_id,app_user_id=app_user_id, json_body=json_body)
        #     pass
        elif should_ask_health_questions:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            timezone_offset = json_body['appUser']['clients'][0]['info']['timezone']
            timed_greeting = self.greeting_message(timezone_offset)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
            unanswered_q, question_list = QuestionRouter.get_all_questions(app_id=app_id, app_user_id=app_user_id)
            if unanswered_q:
                unanswered_q_profile = unanswered_q[0]
                next_question = QuestionRouter.get_next_question(app_id, app_user_id, question_type=unanswered_q_profile, question_list=question_list)
                if next_question:
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text',
                        text="%s %s! Lets answer few questions below before we move forward please" % (
                            timed_greeting, name))
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)
                    import time
                    time.sleep(3)
                    # print(next_question)
                    message_post_body = SmoochRenderer.render_question_item(app_id, app_user_id, next_question)
                    if message_post_body:
                        # print(message_post_body)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        print(api_response)
                else:
                    self.say_greeting_with_menus(app_id, app_user_id, json_body)
            else:
                self.say_greeting_with_menus(app_id, app_user_id, json_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        else:
            self.say_greeting_with_menus(app_id, app_user_id, json_body)
