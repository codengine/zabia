import smooch
from random import randint

from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer
from zabia.constants.enums import ConversationTag, BotAskingMode, ConversationType
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait
from zabia.utils.location_util import LocationUtil
from zabia.utils.tzutil import TZUtil


class BasicInfoMixin(object):

    def send_basic_info_question(self, basic_questions, app_id, app_user_id, json_body, replace_text=None, append_text=None):
        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

        if basic_questions.exists():

            count = basic_questions.count()

            basic_question_instance = basic_questions[randint(0, count - 1)]

            conversation_post_body = SmoochRenderer.render_smooch_content(basic_question_instance, json_body,
                                                                          app_id=app_id,
                                                                          app_user_id=app_user_id,
                                                                          replace_text=replace_text)

            # Now check and save if it should wait for user input
            if basic_question_instance.bot_asking_mode == BotAskingMode.ASK.value:
                postback_payload = '%s:%s' % (ConversationType.BASIC_INFO.value, basic_question_instance.pk)
                ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                               q_postback=postback_payload)

            if conversation_post_body:
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, conversation_post_body)

        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

    def handle_basic_info_missing(self, app_id, app_user_id, json_body):
        if ZUser.check_if_name_missing(app_id=app_id, app_user_id=app_user_id):
            # Find all Conversations by tag Name and prepare smooch request
            basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.NAME.value,
                                                                             parent_id__isnull=True).order_by('order')

            self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)
        elif ZUser.check_if_email_missing(app_id=app_id, app_user_id=app_user_id):
            basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.EMAIL.value,
                                                                             parent_id__isnull=True).order_by('order')

            self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)

        elif ZUser.check_if_gender_missing(app_id=app_id, app_user_id=app_user_id):
            basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.GENDER.value,
                                                                             parent_id__isnull=True).order_by('order')

            self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)

        elif ZUser.check_if_age_missing(app_id=app_id, app_user_id=app_user_id):
            basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.AGE.value,
                                                                             parent_id__isnull=True).order_by('order')

            self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)

        elif ZUser.check_if_weight_missing(app_id=app_id, app_user_id=app_user_id):
            basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.WEIGHT.value,
                                                                             parent_id__isnull=True).order_by('order')

            self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)

        elif ZUser.check_if_height_missing(app_id=app_id, app_user_id=app_user_id):
            basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.HEIGHT.value,
                                                                             parent_id__isnull=True).order_by('order')

            self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)

        elif ZUser.check_if_location_missing(app_id=app_id, app_user_id=app_user_id):

            # Guess the location using clients timezone offset
            timezone_offset = json_body['appUser']['clients'][0]['info']['timezone']
            print("Finding location using utc offset: %s" % timezone_offset)
            timezone_offset = int(timezone_offset)
            tz = TZUtil.guess_timezone(tz_offset=timezone_offset, common_only=True)
            if tz:
                print("Timezone found: %s" % tz)
                print("Finding address, lat and long using tz")
                location_util = LocationUtil()
                address, lat, long = location_util.find_address(address_name=tz.lower())
                if address and lat and long:
                    print("Address found. Saving location for later")
                    location_saved = ZUser.update_location(app_id=app_id,
                                                           app_user_id=app_user_id,
                                                           name=address,
                                                           lat=lat,
                                                           long=long,
                                                           timezone=tz)
                    if location_saved:
                        print("Location saved")
                        send_basic_questions = False
                        location_confirm_conversation_objects = ZabiaConversation.objects.filter(
                            conversation_type=ConversationType.BASIC_INFO.value,
                            tag=ConversationTag.LOCATION_ACTION.value,
                            parent_id__isnull=True
                        )
                        if location_confirm_conversation_objects.exists():
                            location_confirm_conversation_object = location_confirm_conversation_objects.first()
                            message_post_body = SmoochRenderer.render_location_confirm_action(
                                app_id=app_id,
                                app_user_id=app_user_id,
                                conversation_item=location_confirm_conversation_object)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

                    else:
                        send_basic_questions = False
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                        texts = [
                            "Something wrong happened. Please enter your location again"
                        ]
                        text = texts[randint(0, len(texts) - 1)]
                        message_post_body = smooch.MessagePost(
                            'appMaker', 'text', text=text)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                else:
                    print("No address found from timezone")

                    basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.LOCATION.value,
                                                                                     parent_id__isnull=True).order_by(
                        'order')

                    self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)
            else:
                print("No timezone found")

                basic_questions = ZabiaConversation.basicquestionsmanager.filter(tag=ConversationTag.LOCATION.value,
                                                                                 parent_id__isnull=True).order_by('order')

                self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body)

        else:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
            texts = [
                'Thank you %s, Your profile is now complete. Below are the options. Please choose one' % name,
                '%s, Thank you so much. Now your profile look good. You can choose option from below' % name
            ]
            text = texts[randint(0, len(texts) - 1)]
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=text)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

