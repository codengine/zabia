from random import randint
import smooch
from django.utils import timezone
from datetime import timedelta

from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer
from zabia.constants.enums import PostAction, ConversationTag, UserResponseType, ConversationType
from zabia.handlers.api_handlers import APIHandler
from zabia.models.scheduler import Scheduler
from zabia.models.subscription import Subscription
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_response import ZUserResponse
from zabia.tasks import wake_up_tracker, daily_tips


class PostActionHandlerMixin(object):
    def handle_post_action(self, conv_object, app_id, app_user_id, json_body):
        post_action = conv_object.post_action
        print(post_action)

        if post_action == PostAction.SCHEDULE_TRACKER.value:
            print("Scheduling the tracker")
            next_schedule_time = Scheduler.next_reschedule_time(app_id=app_id,
                                                                app_user_id=app_user_id,
                                                                context=conv_object.context,
                                                                topic=conv_object.topic,
                                                                )
            # next_schedule_time = timezone.now() + timedelta(minutes=5)
            if next_schedule_time:
                task_result = wake_up_tracker.apply_async((app_id, app_user_id, conv_object.context, conv_object.topic),
                                                          eta=next_schedule_time)
                Scheduler.cancel_celery_task(app_id=app_id,
                                             app_user_id=app_user_id,
                                             context=conv_object.context,
                                             topic=conv_object.topic)
                Scheduler.enable_tracker(app_id=app_id,
                                         app_user_id=app_user_id,
                                         context=conv_object.context,
                                         topic=conv_object.topic,
                                         task_id=task_result.id)
            print("Tracker has been scheduled")
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
            texts = [
                'What else I can do for you please?',
                'What else I can do for you %s?' % name
            ]
            text = texts[randint(0, len(texts) - 1)]
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=text)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

        elif post_action == PostAction.LOAD_MODIFY_TRACKER_OPTIONS.value:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            tracker_modify_options = ZabiaConversation.objects.filter(context=conv_object.context,
                                                                      topic=conv_object.topic,
                                                                      conversation_type=ConversationType.SCHEDULER_Q.value,
                                                                      tag=ConversationTag.MODIFY_TRACKER.value,
                                                                      parent_id__isnull=True)
            if tracker_modify_options.exists():
                tracker_modify_option = tracker_modify_options.first()
                message_post_body = SmoochRenderer.render_tracker_modify_options(app_id=app_id,
                                                                                 app_user_id=app_user_id,
                                                                                 conversation_item=tracker_modify_option)
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif post_action == PostAction.DELETE_TRACKER.value:
            Scheduler.delete_tracker(app_id=app_id,
                                     app_user_id=app_user_id,
                                     context=conv_object.context,
                                     topic=conv_object.topic)
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
            texts = [
                'Your tracker has been deleted'
            ]
            text = texts[randint(0, len(texts) - 1)]
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=text)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)

            main_menu_instance = ZabiaConversation.get_main_menu_by_context(context=conv_object.context)
            if main_menu_instance:
                self.handle_main_menu_options(app_id, app_user_id, json_body, main_menu_instance.pk)

            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif post_action == PostAction.TRACKER_DELETE_IGNORE.value:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            main_menu_instance = ZabiaConversation.get_main_menu_by_context(context=conv_object.context)
            if main_menu_instance:
                self.handle_main_menu_options(app_id, app_user_id, json_body, main_menu_instance.pk)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif post_action == PostAction.ENABLE_SUBSCRIPTION.value:
            print("Enabling subscription")
            Subscription.enable_subscription(app_id, app_user_id, conv_object.context, conv_object.topic)
            next_schedule_time = Scheduler.get_next_schedule_for_daily_subscription()
            if next_schedule_time:
                result = daily_tips.apply_async((app_id, app_user_id, conv_object.context,
                                                 conv_object.topic), eta=next_schedule_time)
                print(result)
                print(result.id)
                Subscription.update_task_id(app_id=app_id, app_user_id=app_user_id,
                                            context=conv_object.context,
                                            topic=conv_object.topic,
                                            task_id=result.id)
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                texts = [
                    'You are subscribed to daily tips. You will get your next tips tomorrow.'
                ]
                text = texts[randint(0, len(texts) - 1)]
                message_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)

                main_menu_instance = ZabiaConversation.get_main_menu_by_context(context=conv_object.context)
                if main_menu_instance:
                    self.handle_main_menu_options(app_id, app_user_id, json_body, main_menu_instance.pk)

                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            print("User subscribed")
        elif post_action == PostAction.DISABLE_SUBSCRIPTION.value:
            print("Disabling subscription")
            Subscription.disable_subscription(app_id, app_user_id, conv_object.context, conv_object.topic)
            task_id = Subscription.get_task_id(app_id=app_id, app_user_id=app_user_id,
                                               context=conv_object.context,
                                               topic=conv_object.topic)
            if task_id:
                Subscription.revoke_task(task_id=task_id)
            Subscription.update_task_id(app_id=app_id, app_user_id=app_user_id,
                                        context=conv_object.context,
                                        topic=conv_object.topic,
                                        task_id=None)
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
            texts = [
                'You have successfully un-subscribed to daily tips'
            ]
            text = texts[randint(0, len(texts) - 1)]
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=text)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)

            main_menu_instance = ZabiaConversation.get_main_menu_by_context(context=conv_object.context)
            if main_menu_instance:
                self.handle_main_menu_options(app_id, app_user_id, json_body, main_menu_instance.pk)

            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            print("User subscription disabled")
        elif post_action in [PostAction.IGNORE_SUBSCRIPTION.value, PostAction.IGNORE_UNSUBSCRIPTION.value]:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            main_menu_instance = ZabiaConversation.get_main_menu_by_context(context=conv_object.context)
            if main_menu_instance:
                self.handle_main_menu_options(app_id, app_user_id, json_body, main_menu_instance.pk)

            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        elif post_action in [PostAction.IGNORE_SUBSCRIPTION.value, PostAction.IGNORE_SUBSCRIPTION.IGNORE_UNSUBSCRIPTION.value]:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
            texts = [
                'What else I can do for you?',
                'What else I can do for you, %s?' % name,
            ]
            text = texts[randint(0, len(texts) - 1)]
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=text)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
        elif post_action == PostAction.SAVE_TRACKER_ANSWER.value:
            print("Save Tracker Answer!")
            print(conv_object.tag)
            if conv_object.tag == ConversationTag.TRACKER_QUESTION.value:
                ZUserResponse.save_user_response(app_id=app_id,
                                                 app_user_id=app_user_id,
                                                 conversation_id=conv_object.pk,
                                                 context=conv_object.context,
                                                 topic=conv_object.topic,
                                                 response_text=conv_object.payload
                                                 )

        elif post_action == PostAction.SAVE_TRACKER_MOOD_ACTIVITY_ANSWER.value:
            print("Tracker Activity Answer")
            if conv_object.tag == ConversationTag.TRACKER_QUESTION.value:
                ZUserResponse.save_user_response(app_id=app_id,
                                                 app_user_id=app_user_id,
                                                 conversation_id=conv_object.pk,
                                                 context=conv_object.context,
                                                 topic=conv_object.topic,
                                                 activity=conv_object.payload
                                                 )

        elif post_action == PostAction.SHOW_TIPS.value:
            print("Tips")
            user_subscribed = Subscription.check_if_user_subscribed(app_id, app_user_id, conv_object.context, conv_object.topic)
            # Call Tips API
            response = APIHandler.call_tips_api(tags=["diabetes"])
            print(response)
            if response["status"] == "1" and response["data"]:
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                response_data = response["data"][0]
                conversation_post_body = SmoochRenderer.render_tips_item(app_id, app_user_id, conv_object, response_data)
                if conversation_post_body:
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, conversation_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            if user_subscribed:
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text="You have an active subscription for daily tips")
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, conversation_post_body)
                unsubscribe_options = ZabiaConversation.objects.filter(tag=ConversationTag.DISABLE_SUBSCRIPTION_Q.value,
                                                                       context=conv_object.context,
                                                                       topic=conv_object.topic)
                if unsubscribe_options.exists():
                    unsubscribe_option = unsubscribe_options.first()
                    conversation_post_body = SmoochRenderer.render_subscription_item(app_id, app_user_id,
                                                                                     unsubscribe_option)
                    if conversation_post_body:
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, conversation_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)


            else:
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text="You don't have any active subscription")
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, conversation_post_body)
                subscribe_options = ZabiaConversation.objects.filter(tag=ConversationTag.ENABLE_SUBSCRIPTION_Q.value,
                                                                     context=conv_object.context,
                                                                     topic=conv_object.topic)
                if subscribe_options.exists():
                    subscribe_option = subscribe_options.first()
                    conversation_post_body = SmoochRenderer.render_subscription_item(app_id, app_user_id,
                                                                                     subscribe_option)
                    if conversation_post_body:
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, conversation_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        elif post_action == PostAction.SHOW_RECIPES.value:
            zabia_user_id = ZUser.get_zabia_user_id(app_id=app_id, app_user_id=app_user_id)
            response = APIHandler.call_recipes_api(user_id=zabia_user_id)
            if response["status"] == "1" and response["data"]:
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                response_data = response["data"][:3]
                conversation_post_body = SmoochRenderer.render_recipes(app_id, app_user_id, conv_object,
                                                                         response_data)
                if conversation_post_body:
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, conversation_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        elif post_action == PostAction.SHOW_RESTAURANTS.value:
            lat, long = ZUser.get_user_lat_lng(app_id=app_id, app_user_id=app_user_id)
            lat=lat
            long = long
            response = APIHandler.call_restaurant_api(lat=lat, lng=long)
            print(response)
            if response["status"] == "1":
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                if response["data"]:
                    response_data = response["data"]
                    message_post = SmoochRenderer.render_restaurant_item(app_id, app_user_id, conv_object,
                                                                             response_data)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post)
                else:
                    conversation_post_body = smooch.MessagePost(
                        'appMaker', 'text', text="Sorry! No nearby restaurants found for you.")
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, conversation_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        elif post_action == PostAction.TRACKER_SUMMARY.value:
            print("Here I am")
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            tracker_summary_object = Scheduler.get_tracker_summary_question(app_id=app_id,
                                                                            app_user_id=app_user_id,
                                                                            context=conv_object.context,
                                                                            topic=conv_object.topic)
            tracker_summary = Scheduler.get_tracker_summary(app_id=app_id,
                                                            app_user_id=app_user_id,
                                                            context=conv_object.context,
                                                            topic=conv_object.topic)
            message_post_body = SmoochRenderer.render_tracker_summary_item(app_id, app_user_id,
                                                                           tracker_summary_object,
                                                                           tracker_summary,
                                                                           smooch_context=json_body)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            Scheduler.save_tracker_partner_email_skip(app_id=app_id,
                                                      app_user_id=app_user_id,
                                                      context=conv_object.context,
                                                      topic=conv_object.topic)
        elif post_action == PostAction.SAVE_SCHEDULER.value:
            print("Save Scheduler Called!")

            if conv_object.tag.startswith('TRACKER_MODIFY_'):
                if conv_object.tag == ConversationTag.TRACKER_MODIFY_SCHEDULER_OCCURANCE.value:
                    if conv_object.post_action_intent and conv_object.payload:
                        Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                               topic=conv_object.topic, field_name=ConversationTag.SCHEDULER_OCCURANCE.value,
                                               field_value=conv_object.post_action_intent)
                        Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                               topic=conv_object.topic, field_name=ConversationTag.SCHEDULER_OCCURANCE_DAY.value,
                                               field_value=conv_object.payload)
                    else:
                        Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                               topic=conv_object.topic, field_name=conv_object.tag,
                                               field_value=conv_object.payload)
                else:
                    Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                           topic=conv_object.topic,
                                           field_name=conv_object.tag.replace('TRACKER_MODIFY_', ''),
                                           field_value=conv_object.payload)
            else:
                if conv_object.tag == ConversationTag.SCHEDULER_OCCURANCE.value:
                    if conv_object.post_action_intent and conv_object.payload:
                        Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                               topic=conv_object.topic, field_name=ConversationTag.SCHEDULER_OCCURANCE.value,
                                               field_value=conv_object.post_action_intent)
                        Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                               topic=conv_object.topic, field_name=ConversationTag.SCHEDULER_OCCURANCE_DAY.value,
                                               field_value=conv_object.payload)
                    else:
                        Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                               topic=conv_object.topic, field_name=conv_object.tag,
                                               field_value=conv_object.payload)
                else:
                    Scheduler.save_tracker(app_id=app_id, app_user_id=app_user_id, context=conv_object.context,
                                           topic=conv_object.topic, field_name=conv_object.tag,
                                           field_value=conv_object.payload)
            print("Saved!")
        elif post_action == PostAction.SAVE_FLOW.value:
            context = conv_object.context
            topic = conv_object.topic
            if context and topic:
                user_response = ZUserResponse(app_id=app_id, app_user_id=app_user_id)
                user_response.conversation_id = conv_object.pk
                user_response.response_type = UserResponseType.TEXT_RESPONSE.value
                user_response.context = context
                user_response.topic = topic
                user_response.response_text = conv_object.payload
                user_response.save()
        elif post_action == PostAction.START_TRACKER.value:
            # Now check if tracker is already enabled.
            if conv_object.context and conv_object.topic:
                tracker_enabled = Scheduler.check_if_tracker_enabled(app_id=app_id, app_user_id=app_user_id,
                                                                     context=conv_object.context,
                                                                     topic=conv_object.topic)
                print(tracker_enabled)
                if not tracker_enabled:
                    tracker_initial_conversation_objects = ZabiaConversation.objects. \
                        filter(tag=ConversationTag.TRACKER_INITIAL.value, context=conv_object.context,
                               topic=conv_object.topic)
                    if tracker_initial_conversation_objects.exists():
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        tracker_initial_conversation_object = tracker_initial_conversation_objects.first()
                        print("Tracker Initial Found!")
                        message_post_body = SmoochRenderer.render_smooch_content(tracker_initial_conversation_object,
                                                                                 json_body,
                                                                                 app_id=app_id,
                                                                                 app_user_id=app_user_id)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                else:
                    print("Tracker Enabled")
                    delete_tracker_questions = ZabiaConversation.objects.filter(conversation_type=ConversationType.TRACKER_INPUT_Q.value,
                                                                                tag=ConversationTag.DELETE_TRACKER_QUESTION.value, parent_id__isnull=True,
                                                                                context=conv_object.context,
                                                                                topic=conv_object.topic)
                    if delete_tracker_questions.exists():
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        delete_tracker_question = delete_tracker_questions.first()
                        message_post_body = SmoochRenderer.render_delete_tracker_questions(app_id=app_id,
                                                                                       app_user_id=app_user_id,
                                                                                       conversation_item=delete_tracker_question)
                        if message_post_body:
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

        elif post_action == PostAction.RUN_TRACKER.value:
            Scheduler.reset_tracker(app_id=app_id,
                                    app_user_id=app_user_id,
                                    context=conv_object.context,
                                    topic=conv_object.topic)
            if Scheduler.check_if_tracker_information_missing(app_id=app_id, app_user_id=app_user_id,
                                                              context=conv_object.context,
                                                              topic=conv_object.topic):
                next_tracker_question = Scheduler.get_next_tracker_question(app_id=app_id,
                                                                            app_user_id=app_user_id,
                                                                            context=conv_object.context,
                                                                            topic=conv_object.topic)
                if not next_tracker_question:
                    partner_email_question = Scheduler.get_partner_email_question(app_id=app_id,
                                                                                  app_user_id=app_user_id,
                                                                                  context=conv_object.context,
                                                                                  topic=conv_object.topic)
                    if partner_email_question:
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        message_post_body = SmoochRenderer.render_smooch_content(
                            partner_email_question,
                            json_body,
                            app_id=app_id,
                            app_user_id=app_user_id)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                else:
                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    message_post_body = SmoochRenderer.render_smooch_content(
                        next_tracker_question,
                        json_body,
                        app_id=app_id,
                        app_user_id=app_user_id)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)
                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            else:
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                tracker_summary_object = Scheduler.get_tracker_summary_question(app_id=app_id,
                                                                                app_user_id=app_user_id,
                                                                                context=conv_object.context,
                                                                                topic=conv_object.topic)
                tracker_summary = Scheduler.get_tracker_summary(app_id=app_id,
                                                                app_user_id=app_user_id,
                                                                context=conv_object.context,
                                                                topic=conv_object.topic)
                message_post_body = SmoochRenderer.render_tracker_summary_item(app_id, app_user_id,
                                                                               tracker_summary_object,
                                                                               tracker_summary,
                                                                               smooch_context=json_body)
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        elif post_action == PostAction.RETURN_TO_MAIN_MENU.value:
            # Parse the message using natural language processing
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
            texts = [
                'Can I help you with anything else please?',
                'Can I help you with anything else %s?' % name
            ]
            text = texts[randint(0, len(texts) - 1)]
            message_post_body = smooch.MessagePost(
                'appMaker', 'text', text=text)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
        elif post_action == PostAction.RETURN_TO_MENU.value:
            payload = conv_object.payload
            if payload:
                main_menu_objects = ZabiaConversation.objects.filter(tag=ConversationTag.ZABIA_MAIN_MENU.value)
                main_menu_objects = main_menu_objects.filter(texts__text=payload)
                if main_menu_objects.exists():
                    menu_object = main_menu_objects.first()
                    self.handle_main_menu_options(app_id, app_user_id, json_body, menu_object.pk)
        elif post_action == PostAction.SAVE.value:
            save_param = conv_object.save_param

            name = ''
            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
            if not name:
                name = json_body.get('appUser', {}).get('givenName', '')

            gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id)
            if not gender:
                clients = json_body.get('appUser', {}).get('clients', [])
                if clients:
                    gender = clients[0]['info']['gender']
                else:
                    gender = ''

            if '[Name]' in save_param:
                save_param = save_param.replace('[Name]', name)

            if '[Gender]' in save_param and gender:
                save_param = save_param.replace('[Gender]', name)

            if '[Sex]' in save_param and gender:
                save_param = save_param.replace('[Sex]', name)

            conversation_tag = conv_object.tag

            if save_param:
                pass