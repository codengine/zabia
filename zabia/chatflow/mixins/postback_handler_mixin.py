import smooch
from random import randint
from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer
from zabia.chatflow.router import ChatRouter
from zabia.constants.enums import ConversationType, BotAskingMode, PostAction, ConversationTag
from zabia.handlers.api_handlers import APIHandler
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait


class PostbackHandlerMixin(object):

    def send_custom_text_message(self, app_id, app_user_id, message_text):
        message_post_body = smooch.MessagePost(
            'appMaker', 'text', text=message_text)
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post_body)
        return api_response

    def handle_postback(self, app_id, app_user_id, json_body):

        ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)

        postback_payload = json_body['postbacks'][0]['action']['payload']

        print(postback_payload)

        if postback_payload == "AGREED_TERM_AND_POLICY":
            if not ChatRouter.detect_if_terms_accepted(app_id=app_id, app_user_id=app_user_id):
                ZUser.update_terms_agreed(app_id=app_id, app_user_id=app_user_id, terms_accepted=True)

                self.run_zabia_self_introduction(app_id=app_id, app_user_id=app_user_id, json_body=json_body)

                return self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
        elif postback_payload.startswith(ConversationType.MAIN_MENU.value+":"):
            postback_payload_split = postback_payload.split(':')
            parent_id = int(postback_payload_split[1])
            self.handle_main_menu_options(app_id, app_user_id, json_body, parent_id)
        elif postback_payload.startswith(ConversationType.MAIN_MENU_OPTION.value+":"):
            postback_payload_split = postback_payload.split(':')
            parent_id = int(postback_payload_split[1])
            self.handle_main_menu_options(app_id, app_user_id, json_body, parent_id)
        elif postback_payload.startswith(ConversationType.TRACKER_INPUT_Q.value):
            print("Tracker Q")
            postback_payload_split = postback_payload.split(':')
            parent_id = int(postback_payload_split[1])

            tracker_next_menu = ZabiaConversation.objects.filter(parent_id=parent_id)
            if tracker_next_menu.exists():
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                tracker_next_menu = tracker_next_menu.first()
                message_post_body = SmoochRenderer.render_tracker_question_item(app_id=app_id,
                                                                                app_user_id=app_user_id,
                                                                                conversation_item=tracker_next_menu)
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        elif postback_payload.startswith(ConversationType.ZABIA_FLOW.value):
            postback_payload_split = postback_payload.split(':')
            parent_id = int(postback_payload_split[1])

        elif postback_payload.startswith(ConversationType.PERSISTENT_MENU_ITEM.value+"_MAIN"):
            new_user = ChatRouter.detect_if_new_user(app_user_id)

            if new_user:
                # Save the new user
                z_user = ZUser.add_new_user(app_id=app_id, app_user_id=app_user_id)
                return self.send_first_hello_message(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
            elif not ChatRouter.detect_if_terms_accepted(app_id=app_id, app_user_id=app_user_id):
                return self.handle_terms_not_accepted(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
            elif ZUser.check_if_basic_info_missing(app_id=app_id, app_user_id=app_user_id):
                self.handle_basic_info_missing(app_id, app_user_id, json_body)
            else:
                self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
        elif postback_payload.startswith('RECIPE_DETAIL'):
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            print("Recipe Details")
            postback_payload_split = postback_payload.split(':')
            detail_section = postback_payload_split[1]
            recipe_id = postback_payload_split[2]
            print(recipe_id)
            api_response = APIHandler.call_recipe_details_by_id(recipe_id=recipe_id)
            if api_response['status'] == '1' and api_response['data']:
                name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                texts = [
                    'Please find the %s details below for recipe %s' % (postback_payload_split[1].lower(), api_response['data'][0]['name'])
                ]
                text = texts[randint(0, len(texts) - 1)]
                message_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                _api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)

                message_post_body = SmoochRenderer.render_recipe_detail(app_id=app_id,
                                                                        app_user_id=app_user_id,
                                                                        api_response=api_response['data'][0],
                                                                        details_section=detail_section)
                _api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

