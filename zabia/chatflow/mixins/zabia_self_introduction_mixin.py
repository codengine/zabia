import time
from random import randint
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer


class ZabiaSelfIntroductionMixin(object):
    def run_zabia_self_introduction(self, app_id, app_user_id, json_body):
        return
        zabia_intro_conversation_objects = ZabiaConversation.zintroductionmanager.all()
        for conversation_object in zabia_intro_conversation_objects:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            conversation_post_body = SmoochRenderer.render_smooch_content(conversation_object, json_body)
            if conversation_post_body:
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, conversation_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            time.sleep(randint(2, 6))

