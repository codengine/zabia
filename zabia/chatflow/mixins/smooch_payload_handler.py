import hashlib
import json
import copy
import smooch
from random import randint
from datetime import datetime
from datetime import timedelta
from django.utils import timezone
from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer
from zabia.chatflow.router import QuestionRouter
from zabia.constants.enums import ConversationType, BotAskingMode, PostAction, ConversationTag, UserResponseType, \
    Context, TrackerTopic
from zabia.handlers.api_handlers import APIHandler
from zabia.models.QStack import QStack
from zabia.models.scheduler import Scheduler, TrackerProcessedResponse
from zabia.models.subscription import Subscription
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait
from zabia.models.zuser_response import ZUserResponse
from zabia.tasks import wake_up_tracker, daily_tips


class SmoochPayloadHandler(object):

    def handle_smooch_payload(self, app_id, app_user_id, json_body):

        ZUserInputAwait.disable_input_wait(app_id=app_id, app_user_id=app_user_id)

        postback_payload = json_body['messages'][0].get('payload')
        postback_split = postback_payload.split(":")
        if len(postback_split) < 2:
            raise Exception("Invalid payload format found")
        conversation_type = postback_split[0]
        conversation_object_id = int(postback_split[1])

        if conversation_type == 'PROFILE_Q':
            # PROFILE_Q:question_id:question_type:question_code:answer_type:answer_id:description_english:description_spanish
            zusers = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id)
            if zusers.exists():
                zuser = zusers.first()
                zabia_user_id = zuser.zabia_user_id
                question_id = int(postback_split[1])
                question_type = postback_split[2]
                question_code = postback_split[3]
                answer_type = postback_split[4]
                answer_id = postback_split[5]
                description_english = postback_split[6]
                description_spanish = postback_split[7]

                status_code, response = APIHandler.save_question_response(user_id=zabia_user_id, question_id=question_id,
                                                                          question_code=question_code, answer_type=answer_type,
                                                                          answer_id=answer_id,
                                                                          user_answer="")
                # Now fetch the next question and send
                next_question = QuestionRouter.get_next_question(app_id=app_id,
                                                                 app_user_id=app_user_id,
                                                                 question_type=question_type)
                if not next_question:
                    conversation_item_id = QStack.top(app_id=app_id, app_user_id=app_user_id)
                    if not conversation_item_id:
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        texts = [
                                'Thank you so much for your time.'
                            ]
                        text = texts[randint(0, len(texts) - 1)]
                        message_post_body = smooch.MessagePost(
                            'appMaker', 'text', text=text)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                            
                        texts = [
                                'How can I help you please',
                                'What can I do for you please'
                            ]
                        text = texts[randint(0, len(texts) - 1)]
                        message_post_body = smooch.MessagePost(
                            'appMaker', 'text', text=text)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                        return
                    else:
                        conv_object = ZabiaConversation.objects.filter(pk=int(conversation_item_id)).first()
                        required_profiles = conv_object.required_profile.split(',')
                        for rprofile in required_profiles:
                            if zuser.gender == "Male" and rprofile == "Salud femenina":
                                continue
                            if rprofile != question_type:
                                next_question = QuestionRouter.get_next_question(app_id=app_id,
                                                                                 app_user_id=app_user_id,
                                                                                 question_type=rprofile)
                                if next_question:
                                    break
                if next_question:
                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    message_post_body = SmoochRenderer.render_question_item(app_id=app_id,
                                                                            app_user_id=app_user_id,
                                                                            question_item=next_question)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)
                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    return
                else:
                    conversation_item_id = QStack.top(app_id=app_id, app_user_id=app_user_id)
                    if conversation_item_id:
                        # Show options whether the user wants to continue with the last menu or not.
                        print("Finding main menu options using context and topic")
                        conversation_object = ZabiaConversation.objects.filter(pk=int(conversation_item_id)).first()
                        if conversation_object:
                            texts = [
                                'Thank you so much for your time. Now you are good to go'
                            ]
                            text = texts[randint(0, len(texts) - 1)]
                            message_post_body = smooch.MessagePost(
                                'appMaker', 'text', text=text)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)

                            main_menu_instance = ZabiaConversation.get_main_menu_by_context(context=conversation_object.context)
                            child_menu_instance = ZabiaConversation.get_child_menu(context=conversation_object.context,
                                                                                   topic=conversation_object.topic,
                                                                                   parent_menu_id=main_menu_instance.pk)
                            print(child_menu_instance)
                            if child_menu_instance:
                                message_post_body = SmoochRenderer.render_main_menu_options(app_id, app_user_id, child_menu_instance)
                                api_response = self.api_instance.post_message(
                                    app_id, app_user_id, message_post_body)
                    else:
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        texts = [
                            'Thank you so much for your time.'
                        ]
                        text = texts[randint(0, len(texts) - 1)]
                        message_post_body = smooch.MessagePost(
                            'appMaker', 'text', text=text)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)

                        texts = [
                            'How can I help you please',
                            'What can I do for you please'
                        ]
                        text = texts[randint(0, len(texts) - 1)]
                        message_post_body = smooch.MessagePost(
                            'appMaker', 'text', text=text)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
                    return
        else:
            QStack.clear(app_id=app_id, app_user_id=app_user_id)

        if conversation_object_id != -1 and conversation_type != 'PROFILE_Q':
            conversation_object = ZabiaConversation.objects.filter(pk=conversation_object_id).first()
            print(postback_payload)
            print(conversation_object.conversation_type)
            print(conversation_object.tag)
            print(conversation_object.pk)
        else:
            conversation_object = None

        if not conversation_object and conversation_type:
            if conversation_type == "TRACKER_MODIFY_MORE_YES":
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                tracker_modify_options = ZabiaConversation.objects.filter(context = Context.SENTIRSE_BIEN.value,
                                                                          topic = TrackerTopic.MOOD.value,
                                                                          conversation_type=ConversationType.SCHEDULER_Q.value,
                                                                          tag=ConversationTag.MODIFY_TRACKER.value,
                                                                          parent_id__isnull=True)
                if tracker_modify_options.exists():
                    tracker_modify_option = tracker_modify_options.first()
                    message_post_body = SmoochRenderer.render_tracker_modify_options(app_id=app_id,
                                                                                     app_user_id=app_user_id,
                                                                                     conversation_item=tracker_modify_option)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            elif conversation_type == "TRACKER_MODIFY_MORE_NO":
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                tracker_summary_object = Scheduler.get_tracker_summary_question(app_id=app_id,
                                                                                app_user_id=app_user_id,
                                                                                context=Context.SENTIRSE_BIEN.value,
                                                                                topic=TrackerTopic.MOOD.value)
                tracker_summary = Scheduler.get_tracker_summary(app_id=app_id,
                                                                app_user_id=app_user_id,
                                                                context=Context.SENTIRSE_BIEN.value,
                                                                topic=TrackerTopic.MOOD.value)
                message_post_body = SmoochRenderer.render_tracker_summary_item(app_id, app_user_id,
                                                                               tracker_summary_object,
                                                                               tracker_summary,
                                                                               smooch_context=json_body)
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        else:
            if conversation_object and conversation_object.required_profile:
                print("Required profile")
                print("Checking if profile data has been collected already")
                profiles = conversation_object.required_profile.split(',')
                question = None
                for profile in profiles:
                    question = QuestionRouter.get_next_question(app_id=app_id,
                                                                app_user_id=app_user_id,
                                                                question_type=profile)
                    if question:
                        break
                if question:
                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    QStack.push(app_id=app_id, app_user_id=app_user_id, conversation_item_id=conversation_object.pk)

                    timezone_offset = json_body['appUser']['clients'][0]['info']['timezone']
                    timed_greeting = self.greeting_message(timezone_offset)
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                    message_post_body = smooch.MessagePost(
                        'appMaker', 'text',
                        text="%s %s! Lets answer few questions below before we move forward please" % (
                            timed_greeting, name))
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)

                    message_post_body = SmoochRenderer.render_question_item(app_id=app_id,
                                                                            app_user_id=app_user_id,
                                                                            question_item=question)
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, message_post_body)
                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    return

            if conversation_object and conversation_object.conversation_type == ConversationType.BASIC_INFO.value:
                if conversation_object.bot_asking_mode == BotAskingMode.OPTION.value:
                    next_conversation_objects = ZabiaConversation.basicquestionsmanager. \
                        filter(parent_id=conversation_object_id)
                    if next_conversation_objects.exists():
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        next_conversation_object = next_conversation_objects.first()
                        conversation_post_body = SmoochRenderer.render_smooch_content(next_conversation_object,
                                                                                      json_body, app_id, app_user_id)

                        if conversation_post_body:
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, conversation_post_body)
                            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            if next_conversation_object.bot_asking_mode == BotAskingMode.SAY.value:
                                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                for conversation_object in next_conversation_objects[1:]:
                                    conversation_post_body = SmoochRenderer.render_smooch_content(conversation_object,
                                                                                                  json_body, app_id,
                                                                                                  app_user_id)
                                    if conversation_post_body:
                                        api_response = self.api_instance.post_message(
                                            app_id, app_user_id, conversation_post_body)

                            # Now check and save if it should wait for user input
                            if next_conversation_object.bot_asking_mode == BotAskingMode.ASK.value:
                                postback_payload = '%s:%s' % (next_conversation_object.conversation_type, next_conversation_object.pk)
                                ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                                               q_postback=postback_payload)

                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    else:
                        post_action = conversation_object.post_action
                        if post_action == PostAction.LOCATION_ACTION_CONFIRM.value:
                            ZUser.save_location_confirmation(app_id=app_id,
                                                             app_user_id=app_user_id,
                                                             location_confirmed=True)
                            print("Location Confirmed")
                            print("Now saving basic profile to the API")

                            print("Saving done")
                            return self.handle_basic_info_missing(app_id, app_user_id, json_body)
                        elif post_action == PostAction.LOCATION_ACTION_CHANGE.value:
                            basic_questions = ZabiaConversation.basicquestionsmanager.filter(
                                tag=ConversationTag.LOCATION.value,
                                parent_id__isnull=True).order_by('order')

                            self.send_basic_info_question(basic_questions, app_id, app_user_id, json_body,
                                                          replace_text='Where you are located then?')
                        elif post_action == PostAction.SAVE.value:
                            save_param = conversation_object.save_param

                            name = ''
                            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                            if not name:
                                name = json_body.get('appUser', {}).get('givenName', '')

                            gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id)
                            if not gender:
                                clients = json_body.get('appUser', {}).get('clients', [])
                                if clients:
                                    gender = clients[0]['info']['gender']
                                else:
                                    gender = ''

                            if '[Name]' in save_param:
                                save_param = save_param.replace('[Name]', name)

                            if '[Gender]' in save_param and gender:
                                save_param = save_param.replace('[Gender]', name)

                            if '[Sex]' in save_param and gender:
                                save_param = save_param.replace('[Sex]', name)

                            conversation_tag = conversation_object.tag

                            if save_param:
                                if conversation_tag == ConversationTag.NAME.value:
                                    ZUser.update_name(app_id=app_id, app_user_id=app_user_id, name=save_param)
                                elif conversation_tag == ConversationTag.GENDER.value:
                                    ZUser.update_gender(app_id=app_id, app_user_id=app_user_id, gender=save_param)

                            return self.handle_basic_info_missing(app_id, app_user_id, json_body)

            elif conversation_object and conversation_object.conversation_type == ConversationType.ZABIA_FLOW.value:

                self.handle_post_action(conversation_object, app_id, app_user_id, json_body)

                print("Inside Zabia Flow")
                next_conversation_objects = ZabiaConversation.objects. \
                    filter(parent_id=conversation_object_id).order_by('order')
                if next_conversation_objects.exists():
                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    next_conversation_object = next_conversation_objects.first()
                    conversation_post_body = SmoochRenderer.render_smooch_content(next_conversation_object,
                                                                                  json_body, app_id, app_user_id)

                    if conversation_post_body:
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, conversation_post_body)
                        self.handle_post_action(next_conversation_object, app_id, app_user_id, json_body)

                        for c_object in next_conversation_objects[1:]:
                            if c_object.bot_asking_mode == BotAskingMode.SAY.value:
                                next_conversation_object = c_object
                                conversation_post_body = SmoochRenderer.render_smooch_content(c_object,
                                                                                              json_body, app_id,
                                                                                              app_user_id)
                                if conversation_post_body:
                                    api_response = self.api_instance.post_message(
                                        app_id, app_user_id, conversation_post_body)
                                    self.handle_post_action(c_object, app_id, app_user_id, json_body)

                        next_conversation_objects = ZabiaConversation.objects. \
                            filter(parent_id=next_conversation_object.pk).order_by('order')
                        if next_conversation_objects.exists():
                            next_conversation_object = next_conversation_objects.first()
                            conversation_post_body = SmoochRenderer.render_smooch_content(next_conversation_object,
                                                                                          json_body, app_id, app_user_id)
                            if conversation_post_body:
                                api_response = self.api_instance.post_message(
                                    app_id, app_user_id, conversation_post_body)

                        # Now check and save if it should wait for user input
                        if next_conversation_object.bot_asking_mode == BotAskingMode.ASK.value:
                            postback_payload = '%s:%s' % (next_conversation_object.conversation_type, next_conversation_object.pk)
                            ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                                           q_postback=postback_payload)

                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

            elif conversation_object and conversation_object.conversation_type == ConversationType.SCHEDULER_Q.value:
                print("Scheduler Q Entered")
                print(conversation_object.tag)
                show_post_action=True
                self.handle_post_action(conversation_object, app_id, app_user_id, json_body)
                if conversation_object.tag == ConversationTag.TRACKER_PARTNER_EMAIL.value:
                    next_conversation_objects = ZabiaConversation.objects. \
                        filter(parent_id=conversation_object.pk).order_by('order')
                    if next_conversation_objects.exists():
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        next_conversation_object = next_conversation_objects.first()
                        conversation_post_body = SmoochRenderer.render_smooch_content(next_conversation_object,
                                                                                      json_body, app_id, app_user_id)

                        if conversation_post_body:
                            show_post_action = False
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, conversation_post_body)

                            if next_conversation_object.bot_asking_mode == BotAskingMode.ASK.value:
                                postback_payload = '%s:%s' % (
                                    next_conversation_object.conversation_type, next_conversation_object.pk)
                                ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                                               q_postback=postback_payload)

                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    else:
                        show_post_action = False
                    if show_post_action:
                        self.handle_post_action(conversation_object, app_id, app_user_id, json_body)
                elif conversation_object.tag == ConversationTag.MODIFY_TRACKER.value:
                    if conversation_object.payload == "SCHEDULE_TIME":
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        conversation_objects = ZabiaConversation.objects.filter(tag=ConversationTag.TRACKER_MODIFY_SCHEDULER_TIME.value,
                                                                                parent_id__isnull=True)
                        if conversation_objects.exists():
                            conversation_object = conversation_objects.first()
                            message_post_body = SmoochRenderer.render_options(app_id=app_id,
                                                                              app_user_id=app_user_id,
                                                                              conversation_item=conversation_object)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    elif conversation_object.payload == "OCCURANCE":
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        conversation_objects = ZabiaConversation.objects.filter(tag=ConversationTag.TRACKER_MODIFY_SCHEDULER_OCCURANCE.value,
                                                                                parent_id__isnull=True)
                        if conversation_objects.exists():
                            conversation_object = conversation_objects.first()
                            message_post_body = SmoochRenderer.render_options(app_id=app_id,
                                                                              app_user_id=app_user_id,
                                                                              conversation_item=conversation_object)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    elif conversation_object.payload == "UNTIL":
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        conversation_objects = ZabiaConversation.objects.filter(tag=ConversationTag.TRACKER_MODIFY_SCHEDULER_UNTIL.value,
                                                                                parent_id__isnull=True)
                        if conversation_objects.exists():
                            conversation_object = conversation_objects.first()
                            message_post_body = SmoochRenderer.render_options(app_id=app_id,
                                                                              app_user_id=app_user_id,
                                                                              conversation_item=conversation_object)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    elif conversation_object.payload == "PARTNER_EMAIL":
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        conversation_objects = ZabiaConversation.objects.filter(tag=ConversationTag.TRACKER_MODIFY_TRACKER_PARTNER_EMAIL.value,
                                                                                parent_id__isnull=True)
                        if conversation_objects.exists():
                            conversation_object = conversation_objects.first()
                            message_post_body = SmoochRenderer.render_options(app_id=app_id,
                                                                              app_user_id=app_user_id,
                                                                              conversation_item=conversation_object)
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

                elif conversation_object.tag.startswith('TRACKER_MODIFY_'):
                    next_conversation_objects = ZabiaConversation.objects. \
                        filter(parent_id=conversation_object.pk).order_by('order')
                    if next_conversation_objects.exists():
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        next_conversation_object = next_conversation_objects.first()
                        conversation_post_body = SmoochRenderer.render_smooch_content(next_conversation_object,
                                                                                      json_body, app_id, app_user_id)

                        if conversation_post_body:
                            show_post_action = False
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, conversation_post_body)

                            if next_conversation_object.bot_asking_mode == BotAskingMode.ASK.value:
                                postback_payload = '%s:%s' % (
                                    next_conversation_object.conversation_type, next_conversation_object.pk)
                                ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                                               q_postback=postback_payload)

                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        self.handle_post_action(conversation_object, app_id, app_user_id, json_body)
                    else:
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        message_post_body = SmoochRenderer.render_update_more_tracker_options_confirm(app_id=app_id,
                                                                                                      app_user_id=app_user_id)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                elif conversation_object.tag == ConversationTag.TRACKER_SUMMARY.value:
                    print("Save the Tracker now and schedule")

                    # self.handle_post_action(conversation_object, app_id, app_user_id, json_body)
                elif conversation_object.tag == ConversationTag.ENABLE_SUBSCRIPTION_Q.value:
                    print("Enable subscription queue entered")

                elif conversation_object.tag == ConversationTag.DISABLE_SUBSCRIPTION_Q.value:
                    print("Disable subscription queue entered")
                elif conversation_object.tag == ConversationTag.SCHEDULER_TIME.value:
                    # self.handle_post_action(conversation_object, app_id, app_user_id, json_body)
                    child_conversation_objects = ZabiaConversation.objects.filter(parent_id=conversation_object.pk)
                    if child_conversation_objects.exists():
                        print("Scheduler Time Child conversation object found!")
                        print(json_body)
                        print("Done!")
                        child_conversation_object = child_conversation_objects.first()

                        if child_conversation_object.bot_asking_mode == BotAskingMode.ASK.value:
                            postback_payload = '%s:%s' % (
                                child_conversation_object.conversation_type, child_conversation_object.pk)
                            ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id,
                                                           q_postback=postback_payload)

                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        message_post_body = SmoochRenderer.render_smooch_content(
                            child_conversation_object,
                            json_body,
                            app_id=app_id,
                            app_user_id=app_user_id)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    else:
                        if Scheduler.check_if_tracker_information_missing(app_id=app_id, app_user_id=app_user_id,
                                                                          context=conversation_object.context,
                                                                          topic=conversation_object.topic):
                            next_tracker_question = Scheduler.get_next_tracker_question(app_id=app_id,
                                                                                        app_user_id=app_user_id,
                                                                                        context=conversation_object.context,
                                                                                        topic=conversation_object.topic)
                            if not next_tracker_question:
                                partner_email_question = Scheduler.get_partner_email_question(app_id=app_id,
                                                                                              app_user_id=app_user_id,
                                                                                              context=conversation_object.context,
                                                                                              topic=conversation_object.topic)
                                if partner_email_question:
                                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                    message_post_body = SmoochRenderer.render_smooch_content(
                                        partner_email_question,
                                        json_body,
                                        app_id=app_id,
                                        app_user_id=app_user_id)
                                    api_response = self.api_instance.post_message(
                                        app_id, app_user_id, message_post_body)
                                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            else:
                                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                message_post_body = SmoochRenderer.render_smooch_content(
                                    next_tracker_question,
                                    json_body,
                                    app_id=app_id,
                                    app_user_id=app_user_id)
                                api_response = self.api_instance.post_message(
                                    app_id, app_user_id, message_post_body)
                                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                else:
                    child_conversation_objects = ZabiaConversation.objects.filter(parent_id=conversation_object.pk)
                    if child_conversation_objects.exists():
                        print("Child conversation object found!")
                        print(json_body)
                        print("Done!")
                        child_conversation_object = child_conversation_objects.first()
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        message_post_body = SmoochRenderer.render_smooch_content(
                            child_conversation_object,
                            json_body,
                            app_id=app_id,
                            app_user_id=app_user_id)
                        api_response = self.api_instance.post_message(
                            app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    else:
                        print("Child conversation object not found!")
                        if Scheduler.check_if_tracker_information_missing(app_id=app_id, app_user_id=app_user_id,
                                                                          context=conversation_object.context,
                                                                          topic=conversation_object.topic):
                            next_tracker_question = Scheduler.get_next_tracker_question(app_id=app_id,
                                                                                        app_user_id=app_user_id,
                                                                                        context=conversation_object.context,
                                                                                        topic=conversation_object.topic)
                            if not next_tracker_question:
                                partner_email_question = Scheduler.get_partner_email_question(app_id=app_id,
                                                                                              app_user_id=app_user_id,
                                                                                              context=conversation_object.context,
                                                                                              topic=conversation_object.topic)
                                if partner_email_question:
                                    self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                    message_post_body = SmoochRenderer.render_smooch_content(
                                        partner_email_question,
                                        json_body,
                                        app_id=app_id,
                                        app_user_id=app_user_id)
                                    api_response = self.api_instance.post_message(
                                        app_id, app_user_id, message_post_body)
                                    self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                            else:
                                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                message_post_body = SmoochRenderer.render_smooch_content(
                                    next_tracker_question,
                                    json_body,
                                    app_id=app_id,
                                    app_user_id=app_user_id)
                                api_response = self.api_instance.post_message(
                                    app_id, app_user_id, message_post_body)
                                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

            elif conversation_object and conversation_object.conversation_type == ConversationType.TRACKER_INPUT_Q.value:
                jsn = copy.deepcopy(json_body)
                del jsn['appUser']
                hash_value = hashlib.md5(json.dumps(jsn).encode('utf-8')).hexdigest()
                print(hash_value)
                tracker_process_response_objects = TrackerProcessedResponse.objects.filter(hash_value=hash_value)
                if not tracker_process_response_objects.exists():

                    tracker_process_response_object = TrackerProcessedResponse(hash_value=hash_value)
                    tracker_process_response_object.save()

                    self.handle_post_action(conversation_object, app_id, app_user_id, json_body)

                    child_conversation_objects = ZabiaConversation.objects.filter(parent_id=conversation_object.pk)
                    if child_conversation_objects.exists():
                        print("Child conversation object found!")
                        child_conversation_object = child_conversation_objects.first()
                        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        message_post_body = SmoochRenderer.render_smooch_content(
                            child_conversation_object,
                            json_body,
                            app_id=app_id,
                            app_user_id=app_user_id)
                        if message_post_body:
                            api_response = self.api_instance.post_message(
                                app_id, app_user_id, message_post_body)
                        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                        print("Another: " + child_conversation_object.bot_asking_mode)
                        while child_conversation_object.bot_asking_mode == BotAskingMode.SAY.value:
                            child_conversation_objects = ZabiaConversation.objects.filter(parent_id=child_conversation_object.pk)
                            if child_conversation_objects.exists():
                                print("Child conversation object found!")
                                child_conversation_object = child_conversation_objects.first()
                                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                                message_post_body = SmoochRenderer.render_smooch_content(
                                    child_conversation_object,
                                    json_body,
                                    app_id=app_id,
                                    app_user_id=app_user_id)
                                if message_post_body:
                                    api_response = self.api_instance.post_message(
                                        app_id, app_user_id, message_post_body)
                                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                    else:
                        print("Child Not Found!")

            elif ZUser.check_if_basic_info_missing(app_id=app_id, app_user_id=app_user_id):
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                texts = [
                    'Hi %s, before we move on please complete your profile with some basic information.' % name,
                    'Hey, before we move on please complete your profile with some basic information.'
                ]
                text = texts[randint(0, len(texts) - 1)]
                message_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                self.handle_basic_info_missing(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
            else:
                # Parse the message using natural language processing
                self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
                name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                texts = [
                    'How can I help you please?',
                    'Hi, how can I help you?',
                    'What can I do for you please?',
                    'Hello %s, how can I help you please?' % name,
                    'Hey %s, what can I do for you please?' % name
                ]
                text = texts[randint(0, len(texts) - 1)]
                message_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, message_post_body)
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)
