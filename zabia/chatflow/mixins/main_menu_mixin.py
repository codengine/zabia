from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer
from zabia.constants.enums import Presentation
from zabia.models.zabia_conversation import ZabiaConversation


class ZabiaMainMenuMixin(object):

    def handle_main_menu_options(self, app_id, app_user_id, json_body, parent_id):
        while True:
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            text_conversations = ZabiaConversation.objects.filter(parent_id=parent_id,
                                                                  presentation=Presentation.TEXT.value).order_by('order')
            if text_conversations.exists():
                text_conversation = text_conversations.first()
                conversation_post_body = SmoochRenderer.render_main_menu_options(app_id=app_id,
                                                                                 app_user_id=app_user_id,
                                                                                 conversation_item=text_conversation,
                                                                                 smooch_context=json_body)
                if conversation_post_body:
                    api_response = self.api_instance.post_message(
                        app_id, app_user_id, conversation_post_body)
                parent_id = text_conversation.pk
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
            else:
                self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
                break
        zabia_conversations = ZabiaConversation.objects.filter(parent_id=parent_id,
                                         presentation=Presentation.ACTIONS.value).order_by('order')
        if zabia_conversations.exists():
            zabia_conversation = zabia_conversations.first()
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
            conversation_post_body = SmoochRenderer.render_main_menu_options(app_id=app_id,
                                                                             app_user_id=app_user_id,
                                                                             conversation_item=zabia_conversation,
                                                                             smooch_context=json_body)
            if conversation_post_body:
                api_response = self.api_instance.post_message(
                    app_id, app_user_id, conversation_post_body)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

    def handle_main_menu(self, app_id, app_user_id, json_body):
        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
        conversation_post_body = SmoochRenderer.render_main_menu(app_id=app_id, app_user_id=app_user_id, smooch_context=json_body)
        if conversation_post_body:
            api_response = self.api_instance.post_message(
                app_id, app_user_id, conversation_post_body)
        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
