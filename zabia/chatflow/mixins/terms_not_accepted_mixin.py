class TermsNotAcceptedMixin(object):
    def handle_terms_not_accepted(self, app_id, app_user_id, json_body):
        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

        self.send_first_hello_message(app_id=app_id, app_user_id=app_user_id, json_body=json_body, skip_message=True,
                                      skip_typing_activity_trigger=True)

        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)