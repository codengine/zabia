import smooch
from smooch.rest import ApiException


class TypingActivityMixin(object):
    def trigger_start_typing_activity(self, app_id, app_user_id):
        typing_activity_trigger_body = smooch.TypingActivityTrigger(role="appMaker", type="typing:start")
        try:
            self.api_instance.trigger_typing_activity(app_id, app_user_id, typing_activity_trigger_body)
        except ApiException as e:
            print("Exception when calling ConversationApi->trigger_typing_activity: %s\n" % e)

    def trigger_stop_typing_activity(self, app_id, app_user_id):
        typing_activity_trigger_body = smooch.TypingActivityTrigger(role="appMaker", type="typing:stop")
        try:
            self.api_instance.trigger_typing_activity(app_id, app_user_id, typing_activity_trigger_body)
        except ApiException as e:
            print("Exception when calling ConversationApi->trigger_typing_activity: %s\n" % e)
