from random import randint
import smooch
from zabia.models.zabia_user import ZUser


class SayGreetingMixin(object):
    def say_greeting_with_menus(self, app_id, app_user_id, json_body):
        timezone_offset = json_body['appUser']['clients'][0]['info']['timezone']
        timed_greeting = self.greeting_message(timezone_offset)

        greetings = ["HI", "HELLO", "THERE", "HEY", "GREETINGS", "GREETING"]
        menu_keywords = ["MENU", "MAIN MENU"]
        help_keywords = ["HELP"]
        message_text = json_body['messages'][0]['text']

        message_text_split = [word.upper() for word in message_text.split()]

        greeting_found = set(greetings) & set(message_text_split)

        self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)
        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
        if greeting_found:
            texts = [
                'Hello, %s! How can I help please?' % timed_greeting,
                'Hello,! How can I help please?',
                '%s! What can I do for you please?' % timed_greeting,
                'What can I do for you please?',
                'Hi %s, %s, How can I help you?' % (name, timed_greeting),
                'Hi %s, How can I help you?' % name,
                '%s %s! What can I do for you?' % (timed_greeting, name),
                '%s! What can I do for you?' % name,
                'Hello %s, %s! How can I help?' % (name, timed_greeting),
                'Hello %ss! How can I help?' % name,
                'Hi, %s! Here are your options' % timed_greeting,
                'Hi! Here are your options'
            ]
        elif (set(menu_keywords) & set(message_text_split)):
            texts = [
                'Here is your menu. Please choose an option from the menu',
                '%s, Here is your menu. Please choose an option from the menu' % timed_greeting
            ]
        elif (set(help_keywords) & set(message_text_split)):
            texts = [
                'How can I help you?',
                '%s, How can I help you?' % timed_greeting,
                '%s %s, How can I help you?' % (name, timed_greeting),
                'How can I help %s?' % name,
                'How can I help please?',
                '%s! How can I help please?' % timed_greeting
            ]
        else:
            texts = [
                'Greeting %s!, How can I help?' % name,
                '%s, %s!, How can I help?' % (timed_greeting, name),
                'How can I help %s?' % name,
                '%s! How can I help %s?' % (timed_greeting, name),
                'How can I help you please?',
                '%s, How can I help you please?' % timed_greeting,
                'Hi, how can I help you?',
                'Hi %s, how can I help you?' % timed_greeting,
                'What can I do for you please?',
                '%s! What can I do for you please?' % timed_greeting,
                'Hello %s, how can I help you please?' % name,
                'Hello %s, %s! How can I help you please?' % (name, timed_greeting),
                'Hey %s, what can I do for you please?' % name,
                'Hey %s %s! What can I do for you please?' % (timed_greeting, name)
            ]
        text = texts[randint(0, len(texts) - 1)]
        message_post_body = smooch.MessagePost(
            'appMaker', 'text', text=text)
        api_response = self.api_instance.post_message(
            app_id, app_user_id, message_post_body)
        self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)
        self.handle_main_menu(app_id=app_id, app_user_id=app_user_id, json_body=json_body)