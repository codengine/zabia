from zabia.handlers.api_handlers import APIHandler
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zabia_conversation_level import ConversationLevel
from zabia.constants.enums import ConversationLevelEnum


class ChatRouter(object):

    def __init__(self, json_body):
        self.json_body = json_body

    @classmethod
    def detect_if_new_user(self, app_user_id):
        return not ZUser.objects.filter(app_user_id=app_user_id).exists()

    @classmethod
    def detect_if_terms_accepted(cls, app_id, app_user_id):
        z_user = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id).first()
        if z_user:
            return z_user.terms_accepted
        return None

    @classmethod
    def detect_if_basic_info_missing(cls, app_id, app_user_id):
        return ZUser.check_if_basic_info_missing(app_id=app_id, app_user_id=app_user_id)

    @classmethod
    def save_current_level(cls, app_id, app_user_id, level):
        level_objects = ConversationLevel.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if level_objects.exists():
            level_object = level_objects.first()
        else:
            level_object = cls()
        level_object.current_level = level
        level_object.save()
        return level_object

    @classmethod
    def get_next_level(cls, app_id, app_user_id):
        level_objects = ConversationLevel.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if level_objects.exists():
            level_object = level_objects.first()
            return level_object.current_level
        return ConversationLevelEnum.PROFILE_CREATED_BUT_NOT_AGREED_TERMS_AND_CONDITIONS.value


class QuestionRouter(object):
    """
    'question_type' =
    Datos personales:Personal Information
    Salud femenina:Women's Haealth
    Estilo de vida:Lifestyle
    Nutrición: Nutrition
    Estado de ánimo: Mood
    Dieta: Diet
    """

    @classmethod
    def get_next_question(cls, app_id, app_user_id, question_type="", question_list=[]):
        z_users = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            user_id = z_user.zabia_user_id
            if not question_list:
                response = APIHandler.get_all_questions(user_id=user_id,
                                                        question_type=question_type)
                questions = response['data']
            else:
                questions = question_list
            unanswered_questions = [qitem for qitem in questions if not qitem['user_answer']
                                    and (qitem['question_type'] == question_type if question_type else True)]
            if unanswered_questions:
                return unanswered_questions[0]

    @classmethod
    def get_all_questions(cls, app_id, app_user_id):
        z_users = ZUser.objects.filter(app_id=app_id, app_user_id=app_user_id)
        if z_users.exists():
            z_user = z_users.first()
            user_id = z_user.zabia_user_id
            response = APIHandler.get_all_questions(user_id=user_id,
                                                    question_type="")
            questions = response['data']
            unanswered_question_types = [qitem['question_type'] for qitem in questions if not qitem['user_answer'] and qitem['question_type'] != 'Datos personales']
            uqtypes = []
            if z_user.gender == "Male":
                for uqt in unanswered_question_types:
                    if uqt == "Salud femenina":
                        continue
                    else:
                        uqtypes += [uqt]
            unanswered_question_types = uqtypes

            return list(set(unanswered_question_types)), response['data']








