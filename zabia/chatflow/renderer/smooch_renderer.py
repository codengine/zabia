import uuid
import smooch
from django.utils import timezone
from engine.clock.Clock import Clock
from zabia.constants.enums import BotAskingMode, ConversationTag, ConversationType, Presentation, ContentSource
from zabia.models.tracker_chart import TrackerChart
from zabia.models.zabia_conversation import ZabiaConversation
from zabia.models.zabia_user import ZUser
from zabia.models.zuser_input_await import ZUserInputAwait
from zabia.models.zuser_response import ZUserResponse
from zabia.plot.mood_chart import MoodChart
from zabia.utils.aws_s3 import AWSS3


class SmoochRenderer(object):

    @classmethod
    def render_smooch_content(cls, conversation_item, smooch_context={}, app_id=None, app_user_id=None,
                              replace_text=None, append_text=None):
        if conversation_item.conversation_type == ConversationType.ZABIA_INTRODUCTION.value:
            return cls.render_self_introduction_conversation_item(conversation_item, smooch_context)
        elif conversation_item.conversation_type == ConversationType.BASIC_INFO.value:
            return cls.render_basic_info_conversation_item(conversation_item, smooch_context, app_id, app_user_id,
                                                           replace_text=replace_text, append_text=append_text)
        elif conversation_item.conversation_type == ConversationType.ZABIA_FLOW.value:
            return cls.render_zabia_flow_item(app_id, app_user_id, conversation_item, smooch_context)
        elif conversation_item.conversation_type == ConversationType.SCHEDULER_Q.value:
            return cls.render_scheduler_item(app_id, app_user_id, conversation_item, smooch_context)
        elif conversation_item.conversation_type == ConversationType.TRACKER_INPUT_Q.value:
            return cls.render_tracker_question_item(app_id, app_user_id, conversation_item, smooch_context)

    @classmethod
    def render_update_more_tracker_options_confirm(cls, app_id, app_user_id):
        actions = []
        action = smooch.Action(type='reply', text="Yes",
                               payload='TRACKER_MODIFY_MORE_YES:-1')
        actions += [action]
        action = smooch.Action(type='reply', text="No",
                               payload='TRACKER_MODIFY_MORE_NO:-1')
        actions += [action]
        message_post = smooch.MessagePost(role='appMaker', type='text',
                                          text="Do you want to modify any other tracker option?",
                                          actions=actions)
        return message_post

    @classmethod
    def render_options(cls, app_id, app_user_id, conversation_item, append_text=None):
        if conversation_item.presentation == Presentation.ACTIONS.value:
            # Now pick actions.
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                if append_text:
                    text += "\n" + append_text
                actions = []
                child_items = ZabiaConversation.objects.filter(parent_id=conversation_item.pk).order_by('order')
                for child in child_items:
                    actions += [cls.render_subscription_item(app_id, app_user_id, child)]
                message_post = smooch.MessagePost(role='appMaker', type='text', text=text,
                                                  actions=actions)
                return message_post
        elif conversation_item.presentation == Presentation.REPLY.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None

            if text:
                action = smooch.Action(type='reply', text=text,
                                       payload='%s:%s' % (conversation_item.tag, conversation_item.pk))
                return action

    @classmethod
    def render_delete_tracker_questions(cls, app_id, app_user_id, conversation_item):
        return cls.render_options(app_id, app_user_id, conversation_item)

    @classmethod
    def render_subscription_item(cls, app_id, app_user_id, conversation_item):
        return cls.render_options(app_id, app_user_id, conversation_item)

    @classmethod
    def render_tracker_modify_options(cls, app_id, app_user_id, conversation_item):
        return cls.render_options(app_id, app_user_id, conversation_item)

    @classmethod
    def render_location_confirm_action(cls, app_id, app_user_id, conversation_item):
        # Prepare the location summary
        location_info = ZUser.get_location_info(app_id=app_id,
                                                app_user_id=app_user_id)
        return cls.render_options(app_id, app_user_id, conversation_item, append_text=location_info)

    @classmethod
    def render_question_item(cls, app_id, app_user_id, question_item):
        """
        {
         'question_description':'Fecha de nacimiento',
         'question_code':'FECNAC',
         'question_description_english':'Date of birth',
         'answers':[

         ],
         'id_question':'22',
         'answer_type':'D',
         'user_answer':[
            {
               'id_user_answer':'1308',
               'id_answer':None,
               'user_answer_complement':None,
               'user_answer':'1971-07-28'
            }
         ],
         'question_type':'Datos personales',
         'question_description_alternative':''
        }
        """

        question_id = question_item['id_question']
        question_code = question_item['question_code']
        question_description_spanish = question_item['question_description']
        question_description_english = question_item['question_description_english']

        question_description = question_description_english
        if not question_description:
            question_description = question_description_spanish

        question_type = question_item['question_type']

        answer_type = question_item.get('answer_type')
        if answer_type == "M" or answer_type == "U":
            answers = question_item.get('answers', [])
            if not answers:
                return None
            actions = []
            for answer in answers:
                answer_id = answer['id_answer']
                description_english = answer['description_english']
                description_spanish = answer['description']

                action = smooch.Action(type='reply', text=description_english or description_spanish,
                                       payload='PROFILE_Q:%s:%s:%s:%s:%s:%s:%s' % (question_id, question_type, question_code,
                                                                                answer_type, answer_id,
                                                                                description_english,
                                                                                description_spanish))
                actions += [action]
            message_post = smooch.MessagePost(role='appMaker', type='text', text=question_description_english or question_description_spanish,
                                              actions=actions)
            return message_post
        else:
            postback = "PROFILE_Q:%s:%s:%s:%s" % (question_id, question_type, question_code, answer_type)
            ZUserInputAwait.wait_for_input(app_id=app_id, app_user_id=app_user_id, q_postback=postback)
            message_post = smooch.MessagePost('appMaker', 'text', text=question_description_english or question_description_spanish)
            return message_post

    @classmethod
    def render_restaurant_item(cls, app_id, app_user_id, conversation_item, api_response):
        """
        :param app_id:
        :param app_user_id:
        :param conversation_item:
        :param api_response:
        [
            {
             "restaurantID":"535",
             "name":"Sof\u00e1 Caf\u00e9 | Barranco",
             "address":"Av. San Mart\u00edn 480, esquina con Calle Centenario Lima",
             "latitude":"-12.147981",
             "longitude":"-76.964819",
             "url_logo":null,
             "url_marker":"https:\/\/www.southtech.pe\/zabiatest\/imagen\/comercio\/variada.gif",
             "distance":4.01,
             "schedule":"",
             "reservation":"",
             "description":"",
             "like":"0",
             "favorite":"",
             "cuisine_type":[

             ]
            },
            {
             "restaurantID":"260",
             "name":"La Casa de la Parihuela",
             "address":"Psje Arturo Suarez 459 Zona B S.J.M",
             "latitude":"-12.166231",
             "longitude":"-76.974121",
             "url_logo":null,
             "url_marker":"https:\/\/www.southtech.pe\/zabiatest\/imagen\/comercio\/pescados.gif",
             "distance":4.89,
             "schedule":"",
             "reservation":"",
             "description":"",
             "like":"0",
             "favorite":"",
             "cuisine_type":[
                {
                   "nombre":"Fusi\u00f3n"
                }
             ]
            }
        ]
        :return:
        """

        items = []
        for restaurant_item in api_response:
            actions = []

            action = smooch.Action(text='Order Food', type='link',
                                   uri='http://www.myzabia.com/botreservation', payload='RESTAURANT_ORDER_FOOD')
            actions += [action]

            action = smooch.Action(text='Reserve', type='link',
                                   uri='http://www.myzabia.com/botreservation', payload='RESTAURANT_RESERVE')
            actions += [action]

            description='No details found'
            if restaurant_item['description']:
                description = restaurant_item['description']

            media_url, media_type=None, None

            if restaurant_item.get('url_logo'):
                media_url = restaurant_item['url_logo']

            if restaurant_item.get('media_type', None):
                media_type = restaurant_item['media_type']

            message_item = smooch.MessageItem(title=restaurant_item['name'], description=description,
                                              media_url=media_url, media_type=media_type,
                                              actions=actions)
            items += [message_item]

        message_post = smooch.MessagePost(role='appMaker', type='carousel',
                                          items=items)
        return message_post

    @classmethod
    def render_recipe_ingredient_item(cls, app_id, app_user_id, api_response):
        """
        {
           'id':'6745',
           'name':'palta',
           'description':'2 paltas fuertes',
           'quantity':'2.00 unidad'
        }
        """

        actions = []
        action = smooch.Action(text='Like', type='share')
        actions += [action]
        # action = smooch.Action(text='Share', type='share')
        # actions += [action]
        message_item = smooch.MessageItem(title=api_response['name'],
                                          description='Quantity: %s. \n%s' % (api_response['quantity'],
                                                                              api_response['description']),
                                          actions=actions)
        return message_item

    @classmethod
    def render_recipe_detail(cls, app_id, app_user_id, api_response, details_section):
        if details_section == 'INGREDIENTS':
            items = []
            ingredients = ""
            for ingredient_item in api_response['ingredients']:
                ingredients += "%s, %s\n" % (ingredient_item['quantity'], ingredient_item['name'])
                # recipe_rendered = cls.render_recipe_ingredient_item(app_id, app_user_id, ingredient_item)
                # items += [recipe_rendered]

            conversation_post_body = smooch.MessagePost(role='appMaker', type='text', text=ingredients)
            return conversation_post_body
        elif details_section == 'PREPARATION':
            text = api_response['instructions']
            if text:
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                return conversation_post_body

    @classmethod
    def render_recipes(cls, app_id, app_user_id, conversation_item, recipe_items):
        items = []
        for recipe in recipe_items:
            recipe_rendered = cls.render_recipes_item(app_id, app_user_id, conversation_item, recipe)
            items += [recipe_rendered]

        conversation_post_body = smooch.MessagePost(role='appMaker', type='carousel', items=items)
        return conversation_post_body

    @classmethod
    def render_recipes_item(cls, app_id, app_user_id, conversation_item, api_response):
        def detect_content_type(url):
            if url.endswith(".png"):
                return "image/png"
            elif url.endswith(".jpeg"):
                return "image/jpeg"
            elif url.endswith(".jpg"):
                return "image/jpg"
        """
        {  
         "id":"227",
         "name":" Papas Asadas al horno",
         "image_url":"https:\/\/www.southtech.pe\/zabiatest\/imagen\/receta\/a49a0841acee748bc2b18d02ad86cd94.jpg",
         "instructions":"Sancochar las papas con cascara por 10 minutos, hasta que est\u00e9n ligeramente cocidas.\r\nTrozarlas en 4 partes, ponerlas sobre una fuente para el horno, rocear con aceite de oliva extra virgen y espolvorear con especias y sal gruesa.\r\n\r\nPoner al horno precalentado por 20 minutos.\r\nServir a gusto.",
         "favorite":"0",
         "servings":"8",
         "like":"0",
         "credit":null,
         "time":"35m.",
         "cuisine":[  
            {  
               "name":"african"
            },
            {  
               "name":"american"
            },
            {  
               "name":"british"
            },
            {  
               "name":"cajun"
            },
            {  
               "name":"eastern european"
            },
            {  
               "name":"Fusi\u00f3n"
            },
            {  
               "name":"german"
            },
            {  
               "name":"latin american"
            },
            {  
               "name":"mexican"
            },
            {  
               "name":"middle eastern"
            },
            {  
               "name":"Org\u00e1nica"
            },
            {  
               "name":"Peruana contempor\u00e1nea"
            },
            {  
               "name":"Sandwichs y ensaladas"
            },
            {  
               "name":"southern"
            },
            {  
               "name":"Tapas y piqueos"
            }
         ],
         "dishtype":[  
            {  
               "name":"antipasti"
            },
            {  
               "name":"appetizer"
            },
            {  
               "name":"salad"
            },
            {  
               "name":"side dish"
            },
            {  
               "name":"snack"
            },
            {  
               "name":"starter"
            }
         ],
         "benefit":[  
            {  
               "name":"Weight loss"
            }
         ],
         "nutritional":"34 kcal",
         "fat":"0.54 gr.",
         "price":"S\/. 4",
         "country":"Peru",
         "region":"Lima",
         "difficulty":"Bajo",
         "dish_type":"",
         "diet_type":"",
         "ingredients":[  
            {  
               "id":"4110",
               "name":"Papa Blanca",
               "quantity":"1.00 kilogramo",
               "description":"Papa Blanca trozada"
            },
            {  
               "id":"4111",
               "name":"Romero",
               "quantity":"0.50 cucharadita",
               "description":"Romero"
            },
            {  
               "id":"4113",
               "name":"sal gruesa",
               "quantity":"0.50 cucharadita",
               "description":"Sal Gruesa"
            },
            {  
               "id":"4114",
               "name":"Aceite de Oliva Exra Virgen",
               "quantity":"2.00 cucharada",
               "description":"Aceite de Oliva extra virgen"
            }
         ]
        }
        """
        media_url = api_response.get("image_url")
        if not media_url:
            return
        media_type = detect_content_type(media_url)
        if not media_type:
            return

        recipe_id = api_response['id']

        description = api_response['instructions'][:128] #"Recipe: %s" % api_response["name"] + "\n"

        actions = []
        action = smooch.Action(text='Ingredients', type='postback',
                               payload='RECIPE_DETAIL:INGREDIENTS:%s' % (recipe_id, ))
        actions += [action]
        action = smooch.Action(text='Preparation', type='postback',
                               payload='RECIPE_DETAIL:PREPARATION:%s' % (recipe_id,))
        actions += [action]
        message_item = smooch.MessageItem(title='Recipe: ' + api_response['name'],
                                          description=description,
                                          media_url=media_url,
                                          media_type=media_type,
                                          actions=actions)
        return message_item

    @classmethod
    def render_tips_item(cls, app_id, app_user_id, conversation_item, api_response):

        def detect_content_type(url):
            if url.endswith(".png"):
                return "image/png"
            elif url.endswith(".jpeg"):
                return "image/jpeg"
            elif url.endswith(".jpg"):
                return "image/jpg"

        """
        :param api_response:
        {
         'image_url':'https://www.southtech.pe/zabiatest/imagen/tip/605.png',
         'detail':'',
         'title':'Post Genetico_Migraña',
         'tipID':'605',
         'video_url':''
        }
        :return:
        """
        now_time = timezone.now()
        response_time = timezone.datetime(year=now_time.year, month=now_time.month,
                                          day=now_time.day, hour=now_time.hour, minute=now_time.minute, second=0)

        media_url = api_response.get("image_url")
        if not media_url:
            media_url = api_response.get("video_url")
        if not media_url:
            return
        media_type = detect_content_type(media_url)
        title = api_response.get("title")
        if not title:
            return
        description = api_response.get("detail", "")
        if not description:
            description = "Your daily tips on %s" % response_time.strftime(
                                              "%A, %B %d, %Y")

        description += "\n %s" % title

        items = []
        actions = []
        action = smooch.Action(text='Share', type='share',
                               payload='%s' % ("ShareTips",))
        actions += [action]
        message_item = smooch.MessageItem(title="Tips for Today",
                                          description=description,
                                          media_url=media_url,
                                          media_type=media_type,
                                          actions=actions)
        items += [message_item]
        conversation_post_body = smooch.MessagePost(role='appMaker', type='carousel', items=items)
        return conversation_post_body

    @classmethod
    def render_tracker_reminder_menu(cls, app_id, app_user_id, context, topic, smooch_context={}):
        conversation_objects = ZabiaConversation.objects.filter(context=context,
                                                                topic=topic,
                                                                tag=ConversationTag.TRACKER_QUESTION.value,
                                                                presentation=Presentation.CAROUSEL.value,
                                                                parent_id__isnull=True).order_by('order')[:3]
        items = []
        for conversation_item in conversation_objects:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    timenow = Clock.timezone_utc_now().strftime("%A, %B %d, %Y %H:%M%P")
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender,
                        '[Date]': timenow
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                action = smooch.Action(text='Enter Mood', type='postback',
                                       payload='%s:%s' % (conversation_item.conversation_type, conversation_item.pk))
                actions += [action]
                message_item = smooch.MessageItem(title="Reminder", description=text.lower().capitalize(),
                                                  media_url=conversation_item.image,
                                                  media_type=conversation_item.image_type,
                                                  actions=actions)
                items += [message_item]
        if items:
            conversation_post_body = smooch.MessagePost(role='appMaker', type='carousel', items=items)
            return conversation_post_body

    @classmethod
    def render_tracker_question_item(cls, app_id, app_user_id, conversation_item, smooch_context={}):
        if conversation_item.tag == ConversationTag.SHOW_MOOD_CHART.value:
            # Save the chart and get file path
            print(conversation_item.pk)
            zuser_response_object = ZUserResponse.get_tracker_response_object(app_id=app_id,
                                                                              app_user_id=app_user_id,
                                                                              context=conversation_item.context,
                                                                              topic=conversation_item.topic)
            if zuser_response_object:
                print("Inside")
                print(zuser_response_object.pk)
                file_name = str(uuid.uuid4()) + '.png'
                mood_chart = MoodChart()
                file_memory = mood_chart.draw_in_memory(zuser_response_object)
                chart_url = AWSS3.upload_file_from_memory(file_memory, file_name, '')
            else:
                chart_url = "https://s3.amazonaws.com/zabiamoodchart/no_mood_data.png"

            print(chart_url)
            now_time = timezone.now()
            response_time = timezone.datetime(year=now_time.year, month=now_time.month,
                                              day=now_time.day, hour=now_time.hour, minute=now_time.minute, second=0)
            if not TrackerChart.exists(app_id, app_user_id, response_time):
                TrackerChart.add_tracker_chart(app_id, app_user_id, chart_url, response_time)
            else:
                return
            items = []
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    timenow = Clock.timezone_utc_now().strftime("%A, %B %d, %Y %H:%M%P")
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender,
                        '[Date]': timenow
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                action = smooch.Action(text='Share', type='share',
                                       payload='%s:%s' % ("ShareMoodChart", conversation_item.pk))
                actions += [action]
                message_item = smooch.MessageItem(title="Mood Chart", description="Your mood chart on %s" % response_time.strftime("%A, %B %d, %Y %H:%M%P"),
                                                  media_url=chart_url,
                                                  media_type="image/png",
                                                  actions=actions)
                items += [message_item]
            if items:
                conversation_post_body = smooch.MessagePost(role='appMaker', type='carousel', items=items)
                return conversation_post_body
        elif conversation_item.tag == ConversationTag.TRACKER_INPUT_ACKNOWLEDGEMENT.value:
            now_time = timezone.now()
            response_time = timezone.datetime(year=now_time.year, month=now_time.month,
                                              day=now_time.day, hour=now_time.hour, minute=now_time.minute, second=0)
            if TrackerChart.exists(app_id, app_user_id, response_time):
                return
        elif conversation_item.presentation == Presentation.TEXT.value or conversation_item.presentation == Presentation.INFO.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    timenow = Clock.timezone_utc_now().strftime("%A, %B %d, %Y %H:%M%P")
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender,
                        '[Date]': timenow
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                return conversation_post_body
        elif conversation_item.presentation == Presentation.ACTIONS.value:
            # Now pick actions.
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    timenow = Clock.timezone_utc_now().strftime("%A, %B %d, %Y %H:%M%P")
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender,
                        '[Date]': timenow
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                child_items = ZabiaConversation.objects.filter(parent_id=conversation_item.pk).order_by('order')
                for child in child_items:
                    actions += [cls.render_zabia_flow_item(app_id, app_user_id, child)]
                message_post = smooch.MessagePost(role='appMaker', type='text', text=text,
                                                  actions=actions)
                return message_post
        elif conversation_item.presentation == Presentation.REPLY.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    timenow = Clock.timezone_utc_now().strftime("%A, %B %d, %Y %H:%M%P")
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender,
                        '[Date]': timenow
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None

            if text:
                action = smooch.Action(type='reply', text=text,
                                       payload='%s:%s' % (conversation_item.tag, conversation_item.pk))
                return action

    @classmethod
    def render_tracker_summary_item(cls, app_id, app_user_id, conversation_item, summary, smooch_context={}):
        if conversation_item.presentation == Presentation.ACTIONS.value:
            # Now pick actions.
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                child_items = ZabiaConversation.objects.filter(parent_id=conversation_item.pk).order_by('order')
                for child in child_items:
                    actions += [cls.render_zabia_flow_item(app_id, app_user_id, child)]
                text = text + "\n" + summary
                message_post = smooch.MessagePost(role='appMaker', type='text', text=text,
                                                  actions=actions)
                return message_post
        elif conversation_item.presentation == Presentation.REPLY.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None

            if text:
                print("Tag: " + conversation_item.tag)
                action = smooch.Action(type='reply', text=text,
                                       payload='%s:%s' % (conversation_item.tag, conversation_item.pk))
                return action

    @classmethod
    def render_scheduler_item(cls, app_id, app_user_id, conversation_item, smooch_context={}):
        if conversation_item.presentation == Presentation.TEXT.value or conversation_item.presentation == Presentation.INFO.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                return conversation_post_body
        elif conversation_item.presentation == Presentation.ACTIONS.value:
            # Now pick actions.
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                child_items = ZabiaConversation.objects.filter(parent_id=conversation_item.pk).order_by('order')
                for child in child_items:
                    actions += [cls.render_zabia_flow_item(app_id, app_user_id, child)]
                message_post = smooch.MessagePost(role='appMaker', type='text', text=text,
                                                  actions=actions)
                return message_post
        elif conversation_item.presentation == Presentation.REPLY.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None

            if text:
                print("Tag: " + conversation_item.tag)
                action = smooch.Action(type='reply', text=text,
                                       payload='%s:%s' % (conversation_item.tag, conversation_item.pk))
                return action
    
    @classmethod
    def render_self_introduction_conversation_item(cls, conversation_item, smooch_context={}):
        text_instance = conversation_item.pick_conversation_text()
        if text_instance:
            text = text_instance.text
        else:
            text = None
        if text:
            conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
            return conversation_post_body

    @classmethod
    def render_zabia_flow_item(cls, app_id, app_user_id, conversation_item, smooch_context={}):
        if conversation_item.presentation == Presentation.TEXT.value or conversation_item.presentation == Presentation.INFO.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                return conversation_post_body
        elif conversation_item.presentation == Presentation.ACTIONS.value:
            # Now pick actions.
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                child_items = ZabiaConversation.objects.filter(parent_id=conversation_item.pk).order_by('order')
                for child in child_items:
                    actions += [cls.render_zabia_flow_item(app_id, app_user_id, child)]
                message_post = smooch.MessagePost(role='appMaker', type='text', text=text,
                                                  actions=actions)
                return message_post
        elif conversation_item.presentation == Presentation.REPLY.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None

            if text:
                print("Tag: " + conversation_item.tag)
                action = smooch.Action(type='reply', text=text,
                                       payload='%s:%s' % (conversation_item.tag, conversation_item.pk))
                return action

    @classmethod
    def render_main_menu_options(cls, app_id, app_user_id, conversation_item, smooch_context={}):
        if conversation_item.presentation == Presentation.TEXT.value or conversation_item.presentation == Presentation.INFO.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                return conversation_post_body
        elif conversation_item.presentation == Presentation.ACTIONS.value:
            # Now pick actions.
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                child_items = ZabiaConversation.objects.filter(parent_id=conversation_item.pk).order_by('order')
                for child in child_items:
                    actions += [cls.render_main_menu_options(app_id, app_user_id, child)]
                message_post = smooch.MessagePost(role='appMaker', type='text', text=text,
                                                  actions=actions)
                return message_post
        elif conversation_item.presentation == Presentation.REPLY.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None

            if text:
                action = smooch.Action(type='reply', text=text,
                                       payload='%s:%s' % (conversation_item.tag, conversation_item.pk))
                return action

    @classmethod
    def render_main_menu(cls, app_id, app_user_id, smooch_context={}):
        conversation_objects = ZabiaConversation.objects.filter(tag=ConversationTag.ZABIA_MAIN_MENU.value,
                                                                parent_id__isnull=True).order_by('order')[:3]
        items = []
        for conversation_item in conversation_objects:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id) or ''
                    gender = ZUser.get_gender(app_id=app_id, app_user_id=app_user_id) or ''
                    placeholders = {
                        '[Name]': name,
                        'Gender': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None
            if text:
                actions = []
                action = smooch.Action(text='Select', type='postback', payload='%s:%s' % (ConversationType.MAIN_MENU.value, conversation_item.pk))
                actions += [action]
                message_item = smooch.MessageItem(title=text.lower().capitalize(), media_url=conversation_item.image,
                                                  media_type=conversation_item.image_type,
                                                  actions=actions)
                items += [message_item]
        if items:
            conversation_post_body = smooch.MessagePost(role='appMaker', type='carousel', items=items)
            return conversation_post_body
            
    @classmethod
    def render_basic_info_conversation_item(cls, conversation_item, smooch_context={}, app_id=None, app_user_id=None,
                                            replace_text=None, append_text=None):
        if conversation_item.presentation == Presentation.TEXT.value:
            text_instance = conversation_item.pick_conversation_text()
            if replace_text:
                text = replace_text
            else:
                if text_instance:
                    if text_instance.has_placeholder():
                        name = ''
                        if app_id and app_user_id:
                            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                        if not name:
                            name = smooch_context.get('appUser', {}).get('givenName', '')
                        clients = smooch_context.get('appUser', {}).get('clients', [])
                        if clients:
                            gender = clients[0]['info']['gender']
                        else:
                            gender = ''
                        placeholders = {
                            '[Name]': name,
                            '[Gender]': gender
                        }
                        text = text_instance.replace_placeholders(**placeholders)
                    else:
                        text = text_instance.text
                else:
                    text = None
            if text:
                conversation_post_body = smooch.MessagePost(
                    'appMaker', 'text', text=text)
                return conversation_post_body
        elif conversation_item.presentation == Presentation.ACTIONS.value:
            # Now pick actions.
            text_instance = conversation_item.pick_conversation_text()
            if replace_text:
                text = replace_text
            else:
                if text_instance:
                    if text_instance.has_placeholder():
                        name = ''
                        if app_id and app_user_id:
                            name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                        if not name:
                            name = smooch_context.get('appUser', {}).get('givenName', '')
                        clients = smooch_context.get('appUser', {}).get('clients', [])
                        if clients:
                            gender = clients[0]['info']['gender']
                        else:
                            gender = ''
                        placeholders = {
                            '[Name]': name,
                            '[Gender]': gender
                        }
                        text = text_instance.replace_placeholders(**placeholders)
                    else:
                        text = text_instance.text
                else:
                    text = None
            if text:
                actions = []
                child_items = ZabiaConversation.objects.filter(parent_id=conversation_item.pk).order_by('order')[:3]
                for child in child_items:
                    actions += [cls.render_smooch_content(child)]
                message_post = smooch.MessagePost(role='appMaker', type='text', text=text,
                                                  actions=actions)
                return message_post
        elif conversation_item.presentation == Presentation.REPLY.value:
            text_instance = conversation_item.pick_conversation_text()
            if text_instance:
                if text_instance.has_placeholder():
                    name = ''
                    if app_id and app_user_id:
                        name = ZUser.get_name(app_id=app_id, app_user_id=app_user_id)
                    if not name:
                        name = smooch_context.get('appUser', {}).get('givenName', '')
                    clients = smooch_context.get('appUser', {}).get('clients', [])
                    if clients:
                        gender = clients[0]['info']['gender']
                    else:
                        gender = ''
                    placeholders = {
                        '[Name]': name,
                        '[Gender]': gender
                    }
                    text = text_instance.replace_placeholders(**placeholders)
                else:
                    text = text_instance.text
            else:
                text = None

            if text:
                action = smooch.Action(type='reply', text=text,
                                       payload='%s:%s' % (ConversationType.BASIC_INFO.value, conversation_item.pk))
                return action



