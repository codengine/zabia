# Create your tasks here
from __future__ import absolute_import, unicode_literals
from celery import shared_task
from datetime import datetime, timedelta
from django.utils import timezone

from zabia.models.scheduler import Scheduler
from zabia.models.subscription import Subscription
from zabia.tracker_kit import TrackerKit

tracker_kit = TrackerKit()


@shared_task
def add(x, y):
    return x + y


@shared_task
def wake_up_tracker(app_id, app_user_id, context, topic):
    tracker_kit.wake_up_tracker(app_id=app_id,
                                app_user_id=app_user_id,
                                context=context,
                                topic=topic)
    next_schedule_time = Scheduler.next_reschedule_time(app_id=app_id,
                                                        app_user_id=app_user_id,
                                                        context=context,
                                                        topic=topic,
                                                        )
    # next_schedule_time = timezone.now() + timedelta(minutes=2)
    if next_schedule_time:
        wake_up_tracker.apply_async((app_id, app_user_id, context, topic), eta=next_schedule_time)


@shared_task
def daily_tips(app_id, app_user_id, context, topic):
    tracker_kit.popup_daily_tips(app_id=app_id,
                                 app_user_id=app_user_id,
                                 context=context,
                                 topic=topic)
    next_schedule_time = Scheduler.get_next_schedule_for_daily_subscription()
    if next_schedule_time:
        result = daily_tips.apply_async((app_id, app_user_id, context, topic), eta=next_schedule_time)
        task_id = result.id
        Subscription.update_task_id(app_id=app_id, app_user_id=app_user_id,
                                    context=context,
                                    topic=topic,
                                    task_id=task_id)


@shared_task
def check_failed_tracker_response(app_id, app_user_id, context, topic):
    pass

