import io
import matplotlib
matplotlib.use('agg')
from datetime import datetime, timedelta
import matplotlib.pyplot as plt
import uuid
from django.utils import timezone


class MoodChart(object):

    def draw(self, z_user_response_object):
        response_objects = z_user_response_object.response.all()
        x_points, y_points, x_axis, y_axis, y_labels = self.prepare_mood_chart_data(response_objects)
        chart_title = "Mood Tracker Chart"
        random_file_name = z_user_response_object.app_user_id + "-" + str(uuid.uuid4()) + ".png"
        save_path = "media/charts/mood/" + random_file_name
        self.draw_line_chart(chart_title, x_points, y_points, x_axis, y_axis, y_labels, save_path)
        return save_path

    def draw_in_memory(self, z_user_response_object):
        response_objects = z_user_response_object.response.all()
        x_points, y_points, x_axis, y_axis, y_labels = self.prepare_mood_chart_data(response_objects)
        chart_title = "Mood Tracker Chart"
        random_file_name = z_user_response_object.app_user_id + "-" + str(uuid.uuid4()) + ".png"
        file_in_memory = self.draw_line_chart(chart_title, x_points, y_points, x_axis, y_axis, y_labels)
        return file_in_memory

    def prepare_mood_chart_data(self, response_objects):
        """
            response_objects are objects of type UserResponse
            returns [], [] # First one is a x points and second one is y points
        """
        now_time = timezone.now()
        mood_names = ["", "Upset", "Fearfully", "Sad", "Normal", "Happy"]
        mood_numbers = [10,15,20,25,30]
        
        y_labels = mood_names
        
        mood_name_mapping = {
            "Upset": 5,
            "Fearfully": 10,
            "Sad": 15,
            "Normal": 20,
            "Happy": 25
        }

        if response_objects.exists():
            start_date = response_objects.order_by("-id").first().date_created
            end_date = response_objects.order_by("id").first().date_created
        else:
            start_date = now_time
            end_date = now_time

        start_date = start_date - timedelta(5)
        end_date = end_date + timedelta(5)
        
        x_axis = []
        days_difference = (end_date - start_date).days
        for i in range(1, 6):
            x_axis += [now_time - timedelta(days=i)]
        x_axis = x_axis[::-1]
        x_axis += [now_time]
        for i in range(1, days_difference + 1):
            x_axis += [now_time + timedelta(days=i)]
        for i in range(6):
            x_axis += [now_time + timedelta(days=days_difference + i)]
            
        x_axis = [d.date() for d in x_axis]

        y_axis = [i for i in range(0, 35, 5)]

        x_points = []
        y_points = []
        for response_object in response_objects[:6]:
            x_points += [response_object.date_created.date()]
            y_points +=[mood_name_mapping.get(response_object.response_text)]
        # print(y_points)
        return x_points, y_points, x_axis, y_axis, y_labels

    def draw_line_chart(self, chart_title, x_data=[], y_data=[], x_axis=[], y_axis=[], y_labels=[], save_path=None):
        fig, ax = plt.subplots()
        # print(y_labels)
        ax.set_yticks(y_axis)
        # Set ticks labels for x-axis
        ax.set_yticklabels(y_labels)

        label = 'Mood Chart'
        if not x_data and not y_data:
            label = "No Data Found"
        ax.plot_date(x_data, y_data, linestyle='--', label=label)

        ax.set_xlim(x_axis[0], x_axis[len(x_axis) - 1])
        ax.set_ylim(y_axis[0], y_axis[len(y_axis) - 1])
        fig.autofmt_xdate()
        plt.title(chart_title)
        ax.legend()
        # plt.show()
        if save_path:
            fig.savefig(save_path)
        else:
            img_data = io.BytesIO()
            plt.savefig(img_data, format='png')
            img_data.seek(0)
            return img_data
        return save_path