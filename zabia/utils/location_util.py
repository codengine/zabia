import time
import requests


class LocationUtil(object):
    def __init__(self):
        # https://developers.google.com/maps/documentation/geocoding/get-api-key
        self.TIME_TO_SLEEP = 10
        self.GOOGLE_MAPS_API_URL = 'http://maps.googleapis.com/maps/api/geocode/json'
        self.API_KEY = None
        
    def try_geocode(self, address_name, url, key=None):
        try:
             params = {
                 # 'key': 'AIzaSyAQwat1KRa11ENNxsJC9dIUxYJoSY8n9jo',
                 'address': address_name
             }
             if key:
                 params['key'] = key
             req = requests.get(self.GOOGLE_MAPS_API_URL, params=params)
             response = req.json()
             return response
        except Exception as exp:
            return None

    def find_address(self, address_name):
        try:
            response = None
            geocoded = False
            failed_try_count = 10
            while geocoded is not True and failed_try_count > 0:
                response = self.try_geocode(address_name, self.GOOGLE_MAPS_API_URL, self.API_KEY)
                if not response:
                    return None, None, None
                if response['status'] == 'OVER_QUERY_LIMIT':
                    time.sleep(self.TIME_TO_SLEEP)
                    failed_try_count = failed_try_count - 1
                    continue
                elif response['status'] != 'OK':
                    return None, None, None
                elif response['status'] == 'OK':
                    geocoded = True
                    break
            result = response['results'][0]
            return result['formatted_address'], result['geometry']['location']['lat'], result['geometry']['location']['lng']
        except Exception as exp:
            print(str(exp))
            return None, None, None


if __name__ == "__main__":
    location_util = LocationUtil()
    address, lat, long = location_util.find_address(address_name="Dhaka")
    print(address, str(lat), str(long))
