import datetime as dt
from random import randint
import pytz
from timezonefinder import TimezoneFinder


class TZUtil(object):

    @classmethod
    def find_timezone(cls, lat, long):
        try:
            tz_finder = TimezoneFinder()
            TimezoneFinder.using_numba()
            return tz_finder.certain_timezone_at(lng=long, lat=lat)
        except Exception:
            pass

    @classmethod
    def guess_timezone(cls, tz_offset, common_only=True):
        results = cls.possible_timezones(tz_offset=tz_offset, common_only=common_only)
        tz = None
        lindex = 0
        rindex = len(results) - 1
        print(results)
        if results:
            tz = results[randint(lindex, rindex)]
        return tz

    @classmethod
    def possible_timezones(cls, tz_offset, common_only=True):
        # pick one of the timezone collections
        timezones = pytz.common_timezones if common_only else pytz.all_timezones

        # convert the float hours offset to a timedelta
        offset_days, offset_seconds = 0, int(tz_offset * 3600)
        if offset_seconds < 0:
            offset_days = -1
            offset_seconds += 24 * 3600
        desired_delta = dt.timedelta(offset_days, offset_seconds)

        # Loop through the timezones and find any with matching offsets
        null_delta = dt.timedelta(0, 0)
        results = []
        for tz_name in timezones:
            tz = pytz.timezone(tz_name)
            non_dst_offset = getattr(tz, '_transition_info', [[null_delta]])[-1]
            if desired_delta == non_dst_offset[0]:
                results.append(tz_name)
        return results


if __name__ == "__main__":
    # tz = TZUtil.find_timezone(lat=23.810332, long=90.4125181)
    tz = TZUtil.guess_timezone(tz_offset=6, common_only=True)
    print(tz)
    # print(pytz.all_timezones)
