import os
import boto
from django.conf import settings
from django.db.models import Q
from django.db.models.aggregates import Count
from boto.s3.key import Key
from engine.clock import Clock


class AWSS3(object):
    def __init__(self):
        pass

    @classmethod
    def connect_to_s3(cls):
        s3_access_key = settings.S3_ACCESS_KEY
        s3_secret_key = settings.S3_SECRET_KEY
        s3_bucket_name = settings.S3_BUCKET_NAME

        conn = boto.connect_s3(s3_access_key, s3_secret_key)
        bucket = conn.get_bucket(s3_bucket_name)
        return conn, bucket

    @classmethod
    def read_resources(cls):
        try:
            conn, bucket = cls.connect_to_s3()
            bucket_entries = bucket.list()
            return bucket_entries
        except Exception as exp:
            return None

    @classmethod
    def upload_file_from_memory(cls, memory_file, file_name, content_type='image/png'):
        conn, bucket = cls.connect_to_s3()
        file_size = memory_file.getbuffer().nbytes
        k = Key(bucket)
        k.key = file_name
        if content_type:
            k.set_metadata('Content-Type', content_type)
        print(file_size)
        sent = k.set_contents_from_string(memory_file.read(), policy='public-read')
        if sent == file_size:
            return 'https://s3.amazonaws.com/zabiamoodchart/%s' % file_name