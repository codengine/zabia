import jwt
import smooch
import random
from config.smooch import smooch_config
from zabia.chatflow.mixins.typing_activity_mixin import TypingActivityMixin
from zabia.chatflow.renderer.smooch_renderer import SmoochRenderer
from zabia.models.scheduler import Scheduler


class TrackerKit(TypingActivityMixin):
    def obtain_jwt_token(self):
        token_bytes = jwt.encode({'scope': 'app'}, smooch_config['SECRET'], algorithm='HS256', headers={'kid': smooch_config['KEY_ID']})
        token = token_bytes.decode('utf-8')
        return token

    def __init__(self):
        self.auth_token = self.obtain_jwt_token()
        smooch.configuration.api_key['Authorization'] = self.auth_token
        smooch.configuration.api_key_prefix['Authorization'] = 'Bearer'
        self.api_instance = smooch.ConversationApi()
        self.menu_api_instance = smooch.MenuApi()

    def get_api_instance(self):
        return self.api_instance

    def wake_up_tracker(self, app_id, app_user_id, context, topic):
        print("Inside the wake up tracker")
        tracker_enabled = Scheduler.check_if_tracker_enabled(app_id, app_user_id, context, topic)
        if tracker_enabled:
            print(tracker_enabled)
            self.trigger_start_typing_activity(app_id=app_id, app_user_id=app_user_id)

            reminder_menu = SmoochRenderer.render_tracker_reminder_menu(app_id=app_id,
                                                                        app_user_id=app_user_id,
                                                                        context=context,
                                                                        topic=topic)
            # print(reminder_menu)
            api_response = self.api_instance.post_message(
                app_id, app_user_id, reminder_menu)
            self.trigger_stop_typing_activity(app_id=app_id, app_user_id=app_user_id)

    def popup_daily_tips(self, app_id, app_user_id, context, topic):
        pass