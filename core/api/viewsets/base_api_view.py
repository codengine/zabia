from collections import OrderedDict
from django.conf import settings
from rest_framework.response import Response
from rest_framework.views import APIView


class BaseAPIView(APIView):

    def get_queryset(self, request):
        return None

    def get(self, request, **kwargs):
        page = self.request.GET.get('page', 1)
        page_size = self.request.GET.get('page_size', settings.REST_FRAMEWORK.get("PAGE_SIZE", 1))
        objects = self.get_queryset(request)
        return self.get_paginated_response(objects, page, page_size)

    def is_valid(self, request):
        return True

    def handle_post(self, request):
        pass

    def create_post_response(self, request):
        pass

    def post(self, request):
        valid = self.is_valid(request)
        if valid:
            post_processed = self.handle_post(request)
            if post_processed:
                post_response = self.create_post_response(request, post_processed)
                return Response(post_response)
            else:
                return Response({})
        return Response({ 'status': 'Failure', 'message': 'Validation Failed' })

    def get_paginated_response(self, data, page, page_num):
        return Response(OrderedDict([
            ('count', self.page.paginator.count),
            ('current', page),
            ('next', None),
            ('previous', None),
            ('page_size', page_num),
            ('results', data)
        ]))