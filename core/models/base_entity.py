from django.contrib.auth.models import User
from django.db import models
from engine.clock.Clock import Clock


class BaseEntity(models.Model):
    date_created = models.DateTimeField()
    last_updated = models.DateTimeField()
    is_active = models.BooleanField(default=False)
    is_deleted = models.BooleanField(default=False)
    created_by = models.ForeignKey(User, related_name='+', null=True, on_delete=models.CASCADE)
    last_updated_by = models.ForeignKey(User, related_name='+', null=True, on_delete=models.CASCADE)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):

        if not self.pk:
            self.date_created = Clock.timezone_utc_now()

        self.last_updated = Clock.timezone_utc_now()
        super(BaseEntity, self).save(force_insert, force_update, using, update_fields)

    class Meta:
        abstract = True
