"""zabia URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.urls import include
from django.conf import settings
from django.conf.urls.static import static

from zabia.models.QStack import QStack
from zabia.views.home_view import HomeView
from zabia.views.message_view import MessageView
from zabia.views.zabia_user_view import ZabiaUserListView, ZabiaUserDetailsView, ZabiaUserListAjaxView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name="zabia_home_view"),
    url(r'^messages$', MessageView.as_view(), name="zabia_message_view"),
    url(r'^zabia$', MessageView.as_view(), name="zabia_view"),
    url(r'^admin/', include("zadmin.urls")),
    url(r'^zabiatest/users/$', ZabiaUserListView.as_view(), name="zabia_user_list"),
    url(r'^zabiatest/user/(?P<pk>[0-9]+)/details/$', ZabiaUserDetailsView.as_view(), name="zabia_user_details"),
    url(r'^ajax/fetch-users/$', ZabiaUserListAjaxView.as_view(), name="zabia_user_list_ajax"),
]

if settings.DEBUG is True:
    urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

