from django.views.generic import ListView

from zabia.models.zabia_user import ZUser


class AdminHomeView(ListView):
    queryset = ZUser.objects.all()
    template_name = "zadmin/admin_home.html"
