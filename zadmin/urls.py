from django.conf.urls import url
from zadmin.views.admin_home_view import AdminHomeView


urlpatterns = [
    url(r'^$', AdminHomeView.as_view(), name="zabia_admin_home_view"),
]

